const express = require('express');

const app = express();

app.use(express.static('dist/HouseWise'));


app.listen(3001, () => {
    console.log('App started');
});