// pune
$('span.pune').hover(
  function () {
    $('span.pune').css('color', '#8dc63f');
    $('div.address').append($('<span><center><b>91 Springboard Sky Loft, Creaticity Mall,Opposite Golf Course, Off, Airport Rd, Shastrinagar, Yerawada, Pune, Maharashtra 411006</b> </center></span>'));
    $('div.punediv').hide();
  }, function () {
    $('span.pune').css('color', '');
    $('div.address').find('span:last').remove();
    $('div.punediv').show();
  }
);

// Bengaluru
$('span.Bengaluru').hover(
  function () {
    $('span.Bengaluru').css('color', '#8dc63f');
    $('div.address').append($('<span><center><b>Spacelance Office Solutions Pvt.Ltd. #39,2nd Floor,NGEF Lane, Indiranagar, Stage1 Bangalore-560038</b> </center></span>'));
    $('div.punediv').hide();
  }, function () {
    $('span.Bengaluru').css('color', '');
    $('div.address').find('span:last').remove();
    $('div.punediv').show();
  }
);

// Hyderabad
$('span.Hyderabad').hover(
  function () {
    $('span.Hyderabad').css('color', '#8dc63f');
    $('div.address').append($('<span><center><b>Unit -D, T Square Building, 4th floor,Kavuri Hills, Madhapur,Hyderabad - 500033</b></center></span>'));
    $('div.punediv').hide();
  }, function () {
    $('span.Hyderabad').css('color', '');
    $('div.address').find('span:last').remove();
    $('div.punediv').show();
  }
);

// Gurugram
$('span.Gurugram').hover(
  function () {
    $('span.Gurugram').css('color', '#8dc63f');
    $('div.address').append($('<center><span><b>Unit No. 258, Spaze iTech Park, Sector 49, Sohna Road,Gurugram 122001</b></center></span>'));
    $('div.punediv').hide();
  }, function () {
    $('span.Gurugram').css('color', '');
    $('div.address').find('span:last').remove();
    $('div.punediv').show();
  }
);


// Noida
$('span.Noida').hover(
  function () {
    $('span.Noida').css('color', '#8dc63f');
    $('div.address').append($('<span><center><b>Unit No. 258, Spaze iTech Park, Sector 49, Sohna Road,Gurugram 122001</b></center></span>'));
    $('div.punediv').hide();
  }, function () {
    $('span.Noida').css('color', '');
    $('div.address').find('span:last').remove();
    $('div.punediv').show();
  }
);

// Chennai
$('span.Chennai').hover(
  function () {
    $('span.Chennai').css('color', '#8dc63f');
    $('div.address').append($('<span><center><b>	715-A, 7th Floor, Spencer Plaza, Suite No.499 Mount Road, Anna Salai, Chennai - 600 002</b></center></span>'));
    $('div.punediv').hide();
  }, function () {
    $('span.Chennai').css('color', '');
    $('div.address').find('span:last').remove();
    $('div.punediv').show();
  }
);

// Delhi
$('span.Delhi').hover(
  function () {
    $('span.Delhi').css('color', '#8dc63f');
    $('div.address').append($('<span><center><b>91 Springboard Sky Loft, Creaticity Mall, Opposite Golf Course, Off, Airport Rd, Shastrinagar, Yerawada, Pune,Maharashtra 411006</b></center></span>'));
    $('div.punediv').hide();
  }, function () {
    $('span.Delhi').css('color', '');
    $('div.address').find('span:last').remove();
    $('div.punediv').show();
  }
);

// Mumbai
$('span.Mumbai').hover(
  function () {
    $('span.Mumbai').css('color', '#8dc63f');
    $('div.address').append($('<span><center><b>Orbit Plaza, c-154 Mascots,103/104 1st Floor,New Prabhadevi Marg, Mumbai, Maharashtra - 400025</b></center></span>'));
    $('div.punediv').hide();
  }, function () {
    $('span.Mumbai').css('color', '');
    $('div.address').find('span:last').remove();
    $('div.punediv').show();
  }
);


$(document).ready(function () {


  $.fn.isInViewport = function () {
    try {
      const elementTop = $(this).offset().top;
      const elementBottom = elementTop + $(this).outerHeight();
      const viewportTop = $(window).scrollTop();
      const viewportBottom = viewportTop + $(window).height();
      return elementBottom > viewportTop && elementTop < viewportBottom;
    } catch (e) { }
  };

  // $(window).on('resize scroll', function () {
  //   if ($('#counters').isInViewport()) {
  //     $('.counter').each(function () {
  //       $(this).removeClass('counter-in');
  //       $(this).addClass('counter-in');
  //       $('.counter-in').each(function () {
  //         const val = $(this).text();
  //         $(this).prop('Counter', 0).animate({
  //           Counter: $(this).text()
  //         }, {
  //             duration: 4000,
  //             easing: 'swing',
  //             step(now) {
  //               $(this).text(Math.ceil(now));
  //             }
  //           });
  //       });
  //     });
  //   } else {
  //     $(this).removeClass('counter-in');
  //   }
  // });


  let myIndex = 0;
  carousel();

  function carousel() {
    try {
      let i;
      // var x = document.getElementsByClassName("mySlides");
      const x = $('.mySlides');
      for (i = 0; i < x.length; i++) {

        x[i].style.display = 'none';
      }
      myIndex++;
      if (myIndex > x.length) { myIndex = 1; }
      x[myIndex - 1].style.display = 'block';
      setTimeout(carousel, 4000);
    } catch (e) {

    }

  }

  AOS.init();
  //   $('.counter').counterUp({
  //     delay: 10,
  //     time: 1000
  // });
  //$(document).on('scroll', onScroll);

  //onScroll();
  // smoothscroll

  $('a.parallax').on('click', function (e) {

    $(document).off('scroll');

    $('a').each(function () {
      $(this).removeClass('current');
    });
    $(this).addClass('current');

    let target = this.hash,
      menu = target;
    target = $(target);
    $('html, body').stop().animate({
      scrollTop: target.offset().top + 2
    }, 500, 'swing', function () {
      window.location.hash = target;
      $(document).on('scroll', onScroll);
    });
  });

  $('a#services').on('click', function (e) {

    $(document).off('scroll');

    $('nav a').each(function () {
      $(this).removeClass('current');
    });
    $(this).addClass('current');

    let target = this.hash,
      menu = target;
    target = $(target);
    $('html, body').stop().animate({
      scrollTop: target.offset().top + 2
    }, 500, 'swing', function () {
      window.location.hash = target;
      $(document).on('scroll', onScroll);
    });
  });

  $('a#how').on('click', function (e) {

    $(document).off('scroll');

    $('nav a').each(function () {
      $(this).removeClass('current');
    });
    $(this).addClass('current');

    let target = this.hash,
      menu = target;
    target = $(target);
    $('html, body').stop().animate({
      scrollTop: target.offset().top + 2
    }, 500, 'swing', function () {
      window.location.hash = target;
      $(document).on('scroll', onScroll);

    });
  });


  $('a#contact').on('click', function (e) {

    $(document).off('scroll');

    $('nav a').each(function () {
      $(this).removeClass('current');
    });
    $(this).addClass('current');

    let target = this.hash,
      menu = target;
    target = $(target);
    $('html, body').stop().animate({
      scrollTop: target.offset().top + 2
    }, 500, 'swing', function () {
      window.location.hash = target;
      $(document).on('scroll', onScroll);
    });
  });
});

// news feed carousel
$('.carousel[data-type="multi"] .item').each(function () {
  let next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  for (let i = 0; i < 2; i++) {
    next = next.next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }

    next.children(':first-child').clone().appendTo($(this));
  }
});


function onScroll(event) {
  const scrollPosition = $(document).scrollTop();

  // (function () {
  //   let words = [
  //     'HESITANT',
  //     'WORRIED'
  //   ], i = 0;
  //   setInterval(function () {
  //     $('#changingword').fadeIn(function () {
  //       $(this).html(words[i = (i + 1) % words.length]).fadeIn();
  //     });
  //   }, 3500);

  // })();

  // (function () {
  //   let words = [
  //     'RENTING',
  //     ' MANAGING'
  //   ], i = 0;
  //   setInterval(function () {
  //     $('#change').fadeOut(function () {
  //       $(this).html(words[i = (i + 1) % words.length]).fadeIn();
  //     });
  //   }, 3500);

  // })();



}

