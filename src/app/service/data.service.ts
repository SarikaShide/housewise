import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

    constructor() { }



    private loginDataSource = new BehaviorSubject('guest');
    logInChange = this.loginDataSource.asObservable();


    private lastPageSource = new BehaviorSubject(1);
    lastPageSourceChange = this.lastPageSource.asObservable();

    private changeAuthTag = new BehaviorSubject(1);
    changeAuthTagChange = this.changeAuthTag.asObservable();


    logInSuccess(message: string) {
        this.loginDataSource.next(message)
    }

    lastPageNumber(pageNumber: number) {
        this.lastPageSource.next(pageNumber);
    }

    authTabChange(tabNumber: number) {
        this.changeAuthTag.next(tabNumber);
    }

    private changeCurrentHomeMenu = new BehaviorSubject('Home');
    changeCuurentHomeMenuObservable = this.changeCurrentHomeMenu.asObservable();

    currentMenuChanged(currentMenu) {
        this.changeCurrentHomeMenu.next(currentMenu);
    }


    private currentCitySelectedBs = new BehaviorSubject('/');
    changeCurrentCityObservable = this.currentCitySelectedBs.asObservable();

    currentCityChange(currentMenu) {
        this.currentCitySelectedBs.next(currentMenu);
    }

}