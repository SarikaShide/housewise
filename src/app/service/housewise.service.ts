import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { constants } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class HousewiseService {


  // Test enviornment
  url = 'http://13.126.105.113/Housewise_admin/index.php/services';
  resourceBasePath = 'http://13.126.105.113/Housewise_admin/';




  loginUrl = '/login';
  registerUrl = '/registration';
  contactUrl = '/contact_request';
  socialLoginUrl = "/social_login";
  addPropertyUrl = '/property';
  tokenUrl = '/authorization_token';
  propertyListUrl = '/property_list';
  uploadPropertyImagesUrl = '/upload_property_image'
  uploadPropertyDocsUrl = '/upload_property_documents'
  uploadUserDocumentsUrl = '/upload_user_documents'
  comboListUrl = '/combo_list';
  documentTypesUrl = '/get_document_types';
  documentImagesUrl = '/get_documents_images';
  deletePropertyUrl = '/delete_property';
  propertyDetailsUrl = '/property_details';
  deletePropertyImageUrl = '/delete_property_image';
  deletePropertyDocumentUrl = '/delete_property_document';
  profileUrl = '/user_profile';
  cityListUrl = '/city_list';
  addShortListUrl = '/shorlist_property';
  addInterestedUrl = '/interested_property';
  shortListUrl = '/get_shortlist_property';
  interestedListUrl = '/get_interested_property';
  blogListUrl = '/blog_list';
  blogDetailUrl = "/blog_details"
  updateAgreementStatusUrl = '/update_agreement_status';
  propertyCountUrl = '/property_count';
  getPropertyInterestedTenantUrl = '/get_interested_tenant';
  getTenantListUrl = '/tenant_list';
  getUserKYCDocumentsUrl = '/get_user_doc'
  getPropertyReportsUrl = '/get_property_reports'
  getPropertyAgreementUrl = '/get_tenant_agreements'
  shorlistInterestedPropertyUrl = '/shorlist_interested_property'
  deleteUserDocumentUrl = '/delete_user_document'
  updatePropertyStatusUrl = '/update_property_status';
  changePasswordUrl = '/change_password';
  forgotPasswordUrl = '/forgot_password';
  userDetailURl = '/user_detail';
  setForgotPasswordUrl = '/set_password';
  getTenantPropertyCountUrl = '/tenant_count';
  updateConfirmedTenantApproveStatusUrl = '/update_confirmed_tenent_approve_status';
  counterDetailsUrl = '/counter_details';
  newsFeedListUrl = '/news_feed_list';


  userData: any;

  constructor(private http: HttpClient) {
  }


  //put
  //post
  //delete
  //get

  getCounterDetails(): Observable<any> {
    return this.http.get(this.url + this.counterDetailsUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      })
      // params: data
    });
  }


  forgotPassword(param): Observable<any> {
    return this.http.post<any>(this.url + this.forgotPasswordUrl, param, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      })
    }).pipe(
      tap((response) => {
        // console.log(`in service ${response}`);
      })
    );
  }

  getUserDetails(param): Observable<any> {
    return this.http.post<any>(this.url + this.userDetailURl, param, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      })
    }).pipe(
      tap((response) => {
        // console.log(`in service ${response}`);
      })
    );
  }

  changePassword(param, token): Observable<any> {
    return this.http.put(this.url + this.changePasswordUrl, param, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    });
  }

  setForgotPassword(param): Observable<any> {
    return this.http.put(this.url + this.setForgotPasswordUrl, param, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      })
    });
  }

  getNewsFeedsList(): Observable<any> {
    return this.http.get(this.url + this.newsFeedListUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      })
      // params: data
    });
  }

  getBlogs(data): Observable<any> {
    return this.http.get(this.url + this.blogListUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      }),
      params: data
    });
  }


  getBlogsDetails(data): Observable<any> {
    return this.http.get(this.url + this.blogDetailUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      }),
      params: data
    });
  }

  getSearchBlogs(data): Observable<any> {
    return this.http.get(this.url + this.blogListUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      }),
      params: data
    });
  }

  contactSubmit(formVal): Observable<any> {

    const url = this.url + this.contactUrl;
    return this.http.put(this.url + this.contactUrl, formVal, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      }),
    });
  }

  getPropertyDetails(data, token): Observable<any> {
    return this.http.get(this.url + this.propertyDetailsUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }

  deleteProperty(data, token): Observable<any> {

    return this.http.delete(this.url + this.deletePropertyUrl, {
      headers: new HttpHeaders({
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
    return;
  }

  onLogin(param: any): Observable<any> {

    return this.http.post<any>(this.url + this.loginUrl, param, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      })
    }).pipe(
      tap((response) => {
        // console.log(`in service ${response}`);
      })
    );
  }






  onRegister(param: any): Observable<any> {
    //console.log("in register service");
    return this.http.put<any>(this.url + this.registerUrl, param, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      })
    }).pipe(
      tap((response) => {
        //  console.log(`in service ${response}`);
      })
    );
  }


  facebook_social_login(user: any): Observable<any> {

    // const user={ "user_email_id": "sulabha.naware@iarianatech.com"};
    return this.http.post<any>(this.url + this.socialLoginUrl, user, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      })
    }).pipe(
      tap((response) => {
        // console.log(`User social login responce... ${response}`);
      })
    );
  }



  updateAuthToken(userId): Observable<any> {

    const userData = JSON.parse(constants.getUserData());
    const data = {
      'email': userData.user_email_id,
      'password': userData.password,
      'user_id': userData.user_id
    };

    return this.http.put<any>(this.url + this.tokenUrl, data, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12'
      })
    }).pipe(
      tap((response) => {
        //  console.log(response);

      }),
      catchError(this.handleError<any>('/login'))
    );
  }


  getPropertyList(data, token): Observable<any> {

    return this.http.get(this.url + this.propertyListUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }
  getShortPropertyList(data, token): Observable<any> {

    return this.http.get(this.url + this.shortListUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }
  getInterestedProperty(data, token): Observable<any> {

    return this.http.get(this.url + this.interestedListUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }

  getTenantPropertyCount(data, token): Observable<any> {

    return this.http.get(this.url + this.getTenantPropertyCountUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }



  getCityList(data, token): Observable<any> {
    return this.http.get(this.url + this.cityListUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }

  addProperty(param, token): Observable<any> {
    //console.log(param);
    // console.log(token);
    return this.http.put(this.url + this.addPropertyUrl, param, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    })
  }


  uploadPropertyDocs(data, token): Observable<any> {

    return this.http.post<any>(this.url + this.uploadPropertyDocsUrl, data, {
      headers: new HttpHeaders({
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    }).pipe(
      tap((response) => {

        return 'token_error';
      }),
      catchError(this.handleError<any>('sendinvite'))
    );
  }


  uploadPropertyImages(data, token): Observable<any> {

    return this.http.post<any>(this.url + this.uploadPropertyImagesUrl, data, {
      headers: new HttpHeaders({
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    }).pipe(
      tap((response) => {

        return 'token_error';
      }),
      catchError(this.handleError<any>('sendinvite'))
    );
  }

  getComboList(token): Observable<any> {
    return this.http.get(this.url + this.comboListUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    });
  }


  getDocumentType(data, token): Observable<any> {
    return this.http.get(this.url + this.documentTypesUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }

  getDocumentImages(data, token): Observable<any> {
    return this.http.get(this.url + this.documentImagesUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }

  getUserKYCDocuments(data, token): Observable<any> {
    return this.http.get(this.url + this.getUserKYCDocumentsUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }

  getPropertyReports(data, token): Observable<any> {
    return this.http.get(this.url + this.getPropertyReportsUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }

  getPropertyAgreement(data, token): Observable<any> {
    return this.http.get(this.url + this.getPropertyAgreementUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }

  deletePropertyImage(data, token): Observable<any> {
    return this.http.delete(this.url + this.deletePropertyImageUrl, {
      headers: new HttpHeaders({
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }


  deletePropertyDocument(data, token): Observable<any> {
    return this.http.delete(this.url + this.deletePropertyDocumentUrl, {
      headers: new HttpHeaders({
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }

  deleteUserDocument(data, token): Observable<any> {
    return this.http.delete(this.url + this.deleteUserDocumentUrl, {
      headers: new HttpHeaders({
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }


  update_UserProfileData(data, token): Observable<any> {


    return this.http.put<any>(this.url + this.registerUrl, data, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    }).pipe(
      tap((response) => {
        //  console.log(response);

      }),
      catchError(this.handleError<any>('/login'))
    );
  }

  shortListProperty(param: any, token): Observable<any> {
    return this.http.post<any>(this.url + this.addShortListUrl, param, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    }).pipe(
      tap((response) => {
        //  console.log(`in service ${response}`);
      })
    );
  }
  interestedProperty(param: any, token): Observable<any> {
    return this.http.post<any>(this.url + this.addInterestedUrl, param, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    }).pipe(
      tap((response) => {
        //  console.log(`in service ${response}`);
      })
    );
  }
  getUserProfileData(data, token): Observable<any> {

    return this.http.get(this.url + this.profileUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }

  updateConfirmedTenantApproveStatus(data, token): Observable<any> {
    return this.http.put<any>(this.url + this.updateConfirmedTenantApproveStatusUrl, data, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    }).pipe(
      tap((response) => {
        //  console.log(response);

      }),
      catchError(this.handleError<any>('/login'))
    );
  }


  updateAgreementStatus(data, token): Observable<any> {
    return this.http.put<any>(this.url + this.updateAgreementStatusUrl, data, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    }).pipe(
      tap((response) => {
        //  console.log(response);

      }),
      catchError(this.handleError<any>('/login'))
    );
  }


  updatePropetyStatus(data, token): Observable<any> {
    return this.http.put<any>(this.url + this.updatePropertyStatusUrl, data, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    }).pipe(
      tap((response) => {
        //  console.log(response);

      }),
      catchError(this.handleError<any>('/login'))
    );
  }

  getPropertyCount(data, token): Observable<any> {

    return this.http.get(this.url + this.propertyCountUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }


  uploadUserDocuments(data, token): Observable<any> {

    return this.http.post<any>(this.url + this.uploadUserDocumentsUrl, data, {
      headers: new HttpHeaders({
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    }).pipe(
      tap((response) => {

        return 'token_error';
      }),
      catchError(this.handleError<any>('sendinvite'))
    );
  }

  getTenantList(data, token): Observable<any> {

    return this.http.get(this.url + this.getTenantListUrl, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': 'Sagar@12',
        'Authorization': token
      }),
      params: data
    });
  }

  shorlistInterestedProperty(data, token): Observable<any> {

    return this.http.post<any>(this.url + this.shorlistInterestedPropertyUrl, data, {
      headers: new HttpHeaders({
        'x-api-key': 'Sagar@12',
        'Authorization': token
      })
    }).pipe(
      tap((response) => {

        return 'token_error';
      }),
      catchError(this.handleError<any>('sendinvite'))
    );
  }

  getAgreementDocument(data, token): Observable<any> {
    return 
  }


  setUserData(val) {
    this.userData = val;
  }
  getUserData() {
    return this.userData;
  }




  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      // console.error('Error' + error); // log to console instead

      // TODO: better job of transforming error for user consumption
      // console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

















// New Live Site Endpoints (housewise.in)
// url = 'https://www.housewise.in/Housewise_admin/index.php/services';
// resourceBasePath = 'https://www.housewise.in/Housewise_admin/';