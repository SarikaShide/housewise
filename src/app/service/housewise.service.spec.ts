import { TestBed } from '@angular/core/testing';

import { HousewiseService } from './housewise.service';

describe('HousewiseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HousewiseService = TestBed.get(HousewiseService);
    expect(service).toBeTruthy();
  });
});
