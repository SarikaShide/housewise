import { HomePuneComponent } from './home/home-pune/home-pune.component';
import { HomeNoidaComponent } from './home/home-noida/home-noida.component';
import { HomeMumbaiComponent } from './home/home-mumbai/home-mumbai.component';
import { HomeHydrabadComponent } from './home/home-hydrabad/home-hydrabad.component';
import { HomeGurugramComponent } from './home/home-gurugram/home-gurugram.component';
import { HomeDelhiComponent } from './home/home-delhi/home-delhi.component';
import { HomeBengaluruComponent } from './home/home-bengaluru/home-bengaluru.component';
import { AuthGuard } from './core/guards/auth.guard';
import { HomeComponent } from './home/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { ReadmoreComponent } from './home/readmore/readmore.component';
import { BlogsComponent } from './blogs/blogs.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { FaqComponent } from './faq/faq.component';
import { BlogDetailsComponent } from './blogs/blog-details/blog-details.component';
import { NewsFeedDetailsComponent } from './home/news-feed-details/news-feed-details.component';
import { GuestGuard } from './core/guards/guest.guard';
import { HomeChennaiComponent } from './home/home-chennai/home-chennai.component';
import { RentalPropertyComponent } from './home/readmore/rental-property/rental-property.component';
import { EndToEndServicesComponent } from './home/readmore/end-to-end-services/end-to-end-services.component';
import { PropertyManagementComponent } from './home/readmore/property-management/property-management.component';

const routes: Routes = [
  {
    path: 'faq',
    component: FaqComponent
  },
  {
    path: 'blog',
    component: BlogsComponent
  },
  {
    path: 'blog-details/:name',
    component: BlogDetailsComponent
  },
  {
    path: 'terms-condition',
    component: TermsConditionComponent
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent
  },
  {
    path: 'rental-property-management',
    component: RentalPropertyComponent
  },
  {
    path: 'property-maintenance-services',
    component: EndToEndServicesComponent
  },
  {
    path: 'property-management-services',
    component: PropertyManagementComponent
  },
  {
    path: 'services/:name',
    component: ReadmoreComponent
  },
  {
    path: 'home', component: HomeComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'property-management-services-in-Bangalore', component: HomeBengaluruComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'property-management-services-in-Chennai', component: HomeChennaiComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'property-management-services-in-Delhi', component: HomeDelhiComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'property-management-services-in-Gurugram', component: HomeGurugramComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'property-management-services-in-Hyderabad', component: HomeHydrabadComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'property-management-services-in-Mumbai', component: HomeMumbaiComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'property-management-services-in-Noida', component: HomeNoidaComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'property-management-services-in-Pune', component: HomePuneComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'home/:id', component: HomeComponent
  },
  {
    path: 'about-us', component: AboutUsComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'auth', loadChildren: '../app/authentication/authentication.module#AuthenticationModule',
    canActivate: [GuestGuard]
  },
  {
    path: 'user',
    loadChildren: '../app/authentication/user/user.module#UserModule'
  },
  {
    path: 'news-feed/housewise-property-management-startup-in-pune',
    component: NewsFeedDetailsComponent
  },
  {
    path: 'news-feed/housewise-title-sponsor-in-quiz-at-symbiosis-centre',
    component: NewsFeedDetailsComponent
  },
  {
    path: 'news-feed/housewise-offers-owners-to-check-the-tenants-credit-history',
    component: NewsFeedDetailsComponent
  },
  {
    path: 'news-feed/:name',
    component: NewsFeedDetailsComponent
  },
  {
    path: '', component: HomeComponent,
    pathMatch: 'full',
    canActivate: [GuestGuard]
  },
  {
    path: '',
    redirectTo: 'user',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: "**",
    redirectTo: "/"
  }
];

@NgModule({

  // imports: [RouterModule.forRoot(routes, {useHash: true, scrollPositionRestoration: 'enabled' })],
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
