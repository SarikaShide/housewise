import { constants } from './../../constants';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, FacebookLoginProvider, SocialLoginModule, GoogleLoginProvider } from 'angular-6-social-login';
import { HousewiseService } from 'src/app/service/housewise.service';
import { DataService } from 'src/app/service/data.service';


@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  active = false;
  User: { "user_email_id": any; };
  UserData: any;
  username: any;
  forgotTabActive = false;


  constructor(private router: Router, private socialAuthService: AuthService, private housewiseService: HousewiseService,
    private dataService: DataService) {
    //console.log(this.router.url);
    this.dataService.changeAuthTagChange.subscribe((message) => {
      this.active = this.router.url == '/auth/login' ? true : false;
      if (message === 3) {
        this.forgotTabActive = true
      } else {
        this.forgotTabActive = false;
      }

    });
  }

  ngOnInit() {
    this.active = this.router.url == '/auth/login' ? true : false;


  }

  toggle(val) {
    this.active = val;

  }
  //google Sign In

  onSignIn(googleUser) {
    // Useful data for your client-side scripts:
    var profile = googleUser.getBasicProfile();
    // console.log("ID: " + profile.getId()); // Don't send this directly to your server!
    //  console.log('Full Name: ' + profile.getName());
    // console.log('Given Name: ' + profile.getGivenName());
    //  console.log('Family Name: ' + profile.getFamilyName());
    //  console.log("Image URL: " + profile.getImageUrl());
    //  console.log("Email: " + profile.getEmail());

    // The ID token you need to pass to your backend:
    var id_token = googleUser.getAuthResponse().id_token;
    //  console.log("ID Token: " + id_token);
  }


  public socialSignIn(socialPlatform: string) {


    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      const appid = 2910902555618886
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;

    }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {

        this.UserData = userData.email;

        if (userData.email) {
          this.SocialLoginCall(userData);
        }
        this.socialAuthService.signOut();
      }
    );


  }

  SocialLoginCall(UserData: any) {
    if (UserData) {

      this.User = { 'user_email_id': UserData.email };
      this.username = UserData.name;

      this.housewiseService.facebook_social_login(JSON.stringify(this.User)).subscribe((res: any) => {
        for (const key in res) {
          if (key === "Success") {
            if (res.Success.user_type.toUpperCase() == constants.tenant.toUpperCase()) {
              constants.setUserData(JSON.stringify(res.Success));
              this.router.navigate(['/user/tenant-dashboard']);
            } else if (res.Success.user_type.toUpperCase() == constants.owner.toUpperCase()) {
              constants.setUserData(JSON.stringify(res.Success));
              this.router.navigate(['/user/owner-dashboard']);
            }

          } else if (key === 'Error') {
            this.housewiseService.setUserData(UserData);
            this.router.navigate(['/auth/signup']);
            this.active = false;
          }
        }
      }, (e) => {

      });
    }
  }

  // TODO: Cross browsing
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
}
