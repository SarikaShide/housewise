import { constants } from './../../constants';
import { ToastrService } from 'ngx-toastr';
import { HeaderService } from './../../shared/services/header.service';
import { HousewiseService } from '../../service/housewise.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { from } from 'rxjs';
import { DataService } from 'src/app/service/data.service';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  regForm: FormGroup;
  isSubmitted = false;
  invalid = false;
  saving = false;
  passwordMismatch = false;
  param;
  emailid: any;
  UserData: any;
  email: any;
  fname: any;
  lastname: any;
  otherSpecifyVisible = false;
  isLoading = false;
  country_code = '+91';

  default: string = "Select";

  isRecaptchaValid = false;

  captcha;

  constructor(
    private header: HeaderService,
    private acRt: ActivatedRoute,
    private formBuilder: FormBuilder, private housewiseService: HousewiseService, private router: Router,
    private toaster: ToastrService, private dataService: DataService,
    private titleSerive: Title) {

    this.UserData = this.housewiseService.getUserData();
    if (this.UserData) {
      let strarr = [];
      strarr = this.UserData.name.split(' ');
      this.email = this.UserData['email'];
      this.fname = strarr[0];
      this.lastname = strarr[1];
    }

  }

  ngOnInit() {

    this.UserData = this.housewiseService.getUserData();
    if (this.UserData) {
      let strarr = [];
      strarr = this.UserData.name.split(' ');
      this.email = this.UserData['email'];
      this.fname = strarr[0];
      this.lastname = strarr[1];
      this.dataService.authTabChange(1);
    }

    this.regForm = this.formBuilder.group({
      type: ['', Validators.required],
      firstName: ['', [Validators.required, Validators.pattern(/^[a-zA-Z]+(\s)*$/)]],
      lastName: ['', [Validators.required, Validators.pattern(/^[a-zA-Z]+(\s)*$/)]],
      email: ['', [Validators.required, Validators.pattern(/^[_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/)]],
      contact: ['', [Validators.required, Validators.pattern("[0-9]{10}")]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required]],
      platform: ['', [Validators.required]],
      platform_description: [''],
      ref: [''],
      terms_and_condition: [''],
      country_code: [''],
      captcha: ['']
    });


  }

  get control(): any {
    return this.regForm.controls;
  }

  onSubmit(form: any) {

    this.isSubmitted = true;    
    
    if (form.invalid) {
      this.invalid = true;
      return;
    }
    this.invalid = false;

    if ((form.value.password !== form.value.confirmPassword)) {
      this.passwordMismatch = true;
      return;
    }

    if (form.value.platform === 'Select') {
      return;
    }

    if (form.value.terms_and_condition !== true) {
      this.toaster.error('Please select Terms and Conditions & Privacy Policy ', 'Error:');
      return;
    }

    if (this.isRecaptchaValid === false) {
      this.toaster.error("Please verify captcha", "Error:");
      return;
    }

    if(form.value.country_code.charAt(0) !== '+') {
      form.value.country_code = '+' + form.value.country_code;
    }

    form.value.firstName = form.value.firstName.replace(/\s/g, "");
    form.value.lastName = form.value.lastName.replace(/\s/g, "");

    form.value.email = form.value.email.toLowerCase();

    this.param = {
      user_type: form.value.type,
      first_name: form.value.firstName,
      last_name: form.value.lastName,
      user_email_id: form.value.email,
      contact_no: form.value.country_code + '-' + form.value.contact,
      password: form.value.password,
      platform: form.value.platform,
      platform_description: form.value.platform_description,
    }

    this.param = JSON.stringify(this.param);

    this.saving = true;
    this.isLoading = true;
    this.housewiseService.onRegister(this.param).subscribe((res: any) => {
      this.isLoading = false;
      for (const key in res) {
        if (key === "Success") {
          this.toaster.success('Welcome, ' + res.Success.first_name, 'Registration Success:');
          constants.setUserData(JSON.stringify(res.Success));
          if (res.Success.user_type.toUpperCase() == constants.tenant.toUpperCase()) {
            this.router.navigate(['/user/tenant-dashboard']);
          } else if (res.Success.user_type.toUpperCase() == constants.owner.toUpperCase()) {
            this.router.navigate(['/user/owner-dashboard']);
          }
          this.dataService.logInSuccess("Login Success");
        } else if (key === 'Error') {
          this.toaster.error(res.Error, 'Error:');
        }
      }
    }, (e) => {
      this.isLoading = false;
    });
  }


  selectionChangeAboutHouseWise(event) {
    // console.log(event);
    if (event.target.value == 'Other') {
      this.otherSpecifyVisible = true;
    } else {

      this.otherSpecifyVisible = false;
    }
  }

  resolved(captchaResponse: string) {
    if (captchaResponse === null || captchaResponse === undefined || captchaResponse === '') {
      this.toaster.error("Captcha verification failed", "Error:")
      return;
    } else {
      this.isRecaptchaValid = true;
    }
    //console.log(`Resolved captcha with response: ${captchaResponse}`);
    this.captcha = captchaResponse;
  }
}
