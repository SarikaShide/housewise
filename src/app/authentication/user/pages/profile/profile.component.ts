import { ToastrService } from 'ngx-toastr';
import { HeaderService } from './../../../../shared/services/header.service';
import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { HousewiseService } from 'src/app/service/housewise.service';
import { HttpParams } from '@angular/common/http';
import { FormsModule, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { DataService } from 'src/app/service/data.service';
import { Title } from "@angular/platform-browser";
import { constants } from 'src/app/constants';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  sizeErrorString: string = 'size must not be greater than 5MB';

  profileForm: FormGroup;
  userData: any;

  userRole = '';
  authToken: any;

  isLoading = false;
  isSubmitted = false;
  invalid = false;
  docsArray;

  selectedDocIndex = 0;
  selectedDocTypeIndex = 0;
  documentFile;
  documentsArray;
  documentsArrayMatrix = [];

  docsDataFiles: [];
  uploadDocsFormData: FormData = new FormData();

  //default: string = "Savings Resident Indian";
  country_code;
  countryName;

  comboList;

  owner = constants.owner.toUpperCase();


  constructor(
    private headerServ: HeaderService,
    private housewiseService: HousewiseService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private dataService: DataService,
    private titleService: Title
  ) {
    this.userData = JSON.parse(constants.getUserData());

    this.authToken = this.userData.Authorization;
    this.userRole = this.userData.user_type.toUpperCase();
    // if (this.userData.user_city !== '') {
    //   this.countryName = this.userData.user_country;
    // }

  }

  ngOnInit() {
    this.createForm();
    //this.profileForm.controls['account_type'].setValue(this.default, { onlySelf: true });
    this.getComboList();
    this.getUserProfileData();
    this.getDocumentTypes();
    this.getDocumentImages();

    let goto = localStorage.getItem('goto');
    if(goto === 'true') {
      this.gotoDocumentsTab('property_images_tab');
    }

  }

  gotoDocumentsTab(id) {
    var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("nav-item");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById('property_documents').style.display = "block";
			document.getElementById('property_images_tab').className += " active";
			//evt.currentTarget.className += " active";

  }
  

  createForm() {
    if (this.userData.user_type.toUpperCase() === constants.owner.toUpperCase()) {
      this.profileForm = this.formBuilder.group({
        first_name: [null, Validators.required],
        last_name: [null, Validators.required],
        user_email_id: [null, [Validators.required, Validators.pattern(/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/)]],
        contact_no: [null, [Validators.required, Validators.pattern("[0-9]{10}")]],
        user_address: [null, Validators.required],
        landmark: [null, Validators.required],
        area: [null, Validators.required],
        user_city: [null, Validators.required],
        pincode: [null, [Validators.required, Validators.pattern('[0-9]{6}')]],
        country: [null, Validators.required],
        bank_name: [null, [Validators.required, Validators.pattern(/^[a-zA-Z\s]+$/)]],
        ifsc_code: [null, [Validators.required, Validators.pattern(/^[A-Za-z]{4}[0]{1}[a-zA-Z0-9]{6}$/)]],
        account_no: [null, [Validators.required, Validators.pattern('[0-9]{9,18}')]],
        account_type: [null, Validators.required],
        account_name: [null, Validators.required],
        country_code: ['']
      });
    } else {
      this.profileForm = this.formBuilder.group({
        first_name: [null, Validators.required],
        last_name: [null, Validators.required],
        user_email_id: [null, [Validators.required, Validators.pattern(/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/)]],
        contact_no: [null, [Validators.required, Validators.pattern("[0-9]{10}")]],
        user_address: [null, Validators.required],
        landmark: [null, Validators.required],
        area: [null, Validators.required],
        user_city: [null, Validators.required],
        pincode: [null, [Validators.required, Validators.pattern('[0-9]{6}')]],
        country: [null, Validators.required],
        country_code: ['']
      });
    }

  }


  get formControl(): any {
    return this.profileForm.controls;
  }


  docsChanged(event) {
    this.selectedDocIndex = event.target.selectedIndex;
    this.selectedDocTypeIndex = 0;
  }


  docsTypeChanged(event) {
    this.selectedDocTypeIndex = event.target.selectedIndex;
  }


  getDocumentFileImage(item) {

    if (item.path.indexOf(".pdf") > -1) {
      return './assets/images/pdf_placeholder.png';
    } else if (item.path.indexOf(".docx") > -1) {
      return './assets/images/docx_placeholder.png';
    } else {
      return item.path;
    }
  }

  getDocumentFileImageUrl(item) {
    return item.path;
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      this.documentFile = event.target.files[0];
      if ((this.documentFile.size / 1000) > 5000) {
        this.toastr.error('File size to large', 'Error:');
        (<HTMLInputElement>document.getElementById('upload_documents')).value = "";
      } else {
        this.uploadDocs();
      }
    }
  }

  // onSelect($event) {
  //   if ($event.target.value !== '') {
  //     this.countryName = "India";
  //   }
  // }


  uploadDocs() {
    if (this.documentFile) {
      this.uploadDocsFormData = new FormData();
      this.uploadDocsFormData.append('file', this.documentFile);
      this.uploadDocsFormData.append('user_id', this.userData.user_id);
      this.uploadDocsFormData.append('document_proof_id', this.docsArray[this.selectedDocIndex].document_proof_id);
      if (this.docsArray[this.selectedDocIndex].docType[this.selectedDocTypeIndex] && this.docsArray[this.selectedDocIndex].docType[this.selectedDocTypeIndex].document_proof_type_id) {
        this.uploadDocsFormData.append('document_proof_type_id', this.docsArray[this.selectedDocIndex].docType[this.selectedDocTypeIndex].document_proof_type_id);
      }

      this.isLoading = true;
      this.housewiseService.uploadUserDocuments(this.uploadDocsFormData, this.authToken).subscribe((result) => {
        this.isLoading = false;
        for (const key in result) {
          if (key === 'Success') {
            this.documentFile = undefined;
            (<HTMLInputElement>document.getElementById('upload_documents')).value = "";
            this.documentsArray = result.Success;
            // console.log(result.Success);
            this.getDocumentImages();
            this.documentFile = undefined;
            this.toastr.success('Document Saved', 'Success:');
          } else if (key === 'Error') {
            (<HTMLInputElement>document.getElementById('upload_documents')).value = "";
            // console.log("error in ");
            if (result.Error === 'Authorization token not valid') {
              this.refreshAuth('4');
            } else if (result.Error.includes(this.sizeErrorString)) {
              this.toastr.error(result.Error, "Error:");
            }
            return;
          }
        }
        // console.log(JSON.stringify(result));
      }, (err) => {
        this.isLoading = false;
        console.log(err);
      });
    } else {
      console.log('Select doc file');
      this.toastr.error('Select file', "Error:");
    }
  }


  //Get User Documents


  getDocumentTypes() {
    const param = new HttpParams().set('user_type', this.userData.user_type + '');
    this.housewiseService.getDocumentType(param, this.authToken).subscribe((result) => {
      // console.log(result);
      for (const key in result) {
        if (key === 'Success') {
          this.docsArray = result.Success;
        } else if (key === 'Error') {
          //  console.log("error in ");
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(6);
          }
          return;
        }
      }
    });
  }


  getDocumentImages() {
    const params = new HttpParams().set('user_id', this.userData.user_id + '');
    //	const params = '';
    this.housewiseService.getUserKYCDocuments(params, this.authToken).subscribe((result) => {
      //  console.log("response " + result);
      for (const key in result) {
        if (key === "Success") {
          //  console.log("success " + result.Success);
          this.documentsArray = result.Success;
          this.createDocumentImagesMatrix();
        } else if (key === 'Error') {
          this.documentsArrayMatrix = [];
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(7);
          }
          return;
        }
      }
    }, (err) => {
      console.log(err);
    });
  }

  createDocumentImagesMatrix() {
    this.documentsArrayMatrix = [];
    let k = 0;
    for (let i = 0; i < this.documentsArray.length; i++) {
      if (i % 4 === 0) {
        k++;
        this.documentsArrayMatrix[k] = [];
      }
      this.documentsArrayMatrix[k].push(this.documentsArray[i]);
    }
    // console.log(this.documentsArrayMatrix);
  }

  getComboList() {
    this.housewiseService.getComboList(this.authToken).subscribe((result) => {
      //	console.log(result);
      for (const key in result) {
        if (key === 'Success') {
          this.comboList = result.Success;
        } else if (key === 'Error') {
          //	console.log("error in ");
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(3);
          }
          return;
        }
      }
    });
  }


  refreshAuth(type) {
    const userId = { 'id': this.userData.user_id };

    this.housewiseService.updateAuthToken(JSON.stringify(userId)).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          constants.setUserData(JSON.stringify(result.Success));
          this.userData = result.Success;
          this.authToken = this.userData.Authorization;
          // console.log('Success' + result.Success);
          if (type === 1) {
            this.getUserProfileData();
          } else if (type === 2) {
            this.updateProfile();
          } else if (type === 3) {
            this.getComboList();
          }
        } else if (key === 'Error') {
          //  console.log(result.Error);
        }
        return;
      }
      //  console.log(JSON.stringify(result));
    }, (err) => {
      console.log(err);
    });
  }


  //Get User Profile Details...
  getUserProfileData() {
    const params = new HttpParams().set('user_id', this.userData.user_id + '');
    this.housewiseService.getUserProfileData(params, this.authToken).subscribe((result) => {

      for (const key in result) {
        if (key === "Success") {

          this.userData = result.Success;
          // console.log("First:",this.userData.contact_no);
          if(this.userData.contact_no.charAt(0)!="+") {
            this.userData.contact_no = "+"+this.userData.contact_no;
          }
          this.userData.contact_no = this.userData.contact_no.split("-");
          
          // console.log("Second:",this.userData.contact_no);
          this.countryName = this.userData.user_country;

          if (this.userData.account_type === "") {
            this.userData.account_type = 'Select';
          }
         

        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
             this.refreshAuth(1);
          }
          return;
        }
      }
    }, (err) => {
      console.log(err);
    });


  }



  updateProfile() {
    this.isSubmitted = true;
    //console.log("pavan", this.profileForm);
    
    if (this.profileForm.invalid) {
      this.invalid = true;
      return;
    }
    
    if (this.profileForm.value.account_type === 'Select') {
      this.toastr.error("Select account type", "Error:");
      return;
    }

    if(this.profileForm.value.country_code.charAt(0) !== '+') {
      this.profileForm.value.country_code = '+' + this.profileForm.value.country_code;
    }

    let data = {
      user_id: this.userData.user_id,
      first_name: this.profileForm.value.first_name,
      last_name: this.profileForm.value.last_name,
      user_email_id: this.profileForm.value.user_email_id,
      contact_no: this.profileForm.value.country_code + '-' + this.profileForm.value.contact_no,
      user_address: this.profileForm.value.user_address,
      user_landmark: this.profileForm.value.landmark,
      user_area: this.profileForm.value.area,
      user_city: this.profileForm.value.user_city,
      user_pin_code: this.profileForm.value.pincode,
      user_country: this.profileForm.value.country,
      bank_details: {
        bank_name: this.profileForm.value.bank_name,
        ifsc_code: this.profileForm.value.ifsc_code,
        account_no: this.profileForm.value.account_no,
        account_type: this.profileForm.value.account_type,
        account_name: this.profileForm.value.account_name
      }
    }

    // console.log(data);

    this.isLoading = true;
    this.housewiseService.onRegister(JSON.stringify(data)).subscribe((result) => {
      this.isLoading = false;
      for (const key in result) {
        if (key === "Success") {
          //  console.log("success " + result.Success);
          constants.setUserData(JSON.stringify(result.Success));
          this.toastr.success('Profile updated successfully', 'Success:');
          this.dataService.logInSuccess("profile updated");
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(2);
          }
          return;
        }
      }
    }, (err) => {
      this.isLoading = false;
      console.log(err);
    });
  }

  deleteUserDocument(item, row, column) {
    const params = new HttpParams().set('document_id', item.document_id + '').set('user_id', this.userData.user_id);
    this.housewiseService.deleteUserDocument(params, this.authToken).subscribe((result) => {
      // console.log("response: ", result);
      for (const key in result) {
        if (key === "Success") {
          //  console.log("success: ", result.Success);
          //this.documentsArrayMatrix[row].splice(column, 1);
          this.getDocumentImages();
          this.toastr.success('Delete document success', 'Success:');
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(8);
          } else {
            this.toastr.error('Error while document delete', 'Error:');
          }
          return;
        }
      }
    }, (err) => {
      console.log(err);
      this.toastr.error('Error while document delete', 'Error:');
    });
  }


  changeTab(evt, tabName) {
    var i, tabcontent, tablinks;
    if (evt.currentTarget.id === 'property_images_tab'){
      this.userData = JSON.parse(constants.getUserData());
    }
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("nav-item");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";

    if (this.userData.account_type === "") {
      this.userData.account_type = 'Select';
    }

    this.userData.contact_no = this.userData.contact_no.split("-");
   
  }

  // TODO: Cross browsing
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  ngOnDestroy() {
    localStorage.removeItem('goto');
  }

}
