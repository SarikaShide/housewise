import { DataService } from 'src/app/service/data.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { HousewiseService } from 'src/app/service/housewise.service';
import { HttpParams } from '@angular/common/http';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { Title } from '@angular/platform-browser';
import { constants } from 'src/app/constants';


@Component({
  selector: 'app-listing-page',
  templateUrl: './listing-page.component.html',
  styleUrls: ['./listing-page.component.css']
})
export class ListingPageComponent implements OnInit {

  userRole = '';
  userData: any;
  authToken: any;
  pageData: any;
  cityid: any;
  userId: any;
  currentPage = 1;
  pageCount = 1;
  paginationArray = [];
  cityList: any;
  loadingList = false;
  dataError = false;
  address;
  subModule: any;
  propertyId: any;


  constructor(private formBuilder: FormBuilder, private housewiseService: HousewiseService,
    private cd: ChangeDetectorRef, private router: Router, private acRt: ActivatedRoute,
    private dataService: DataService,
    private titleService: Title) {
    this.userData = JSON.parse(constants.getUserData());
    this.authToken = this.userData.Authorization;
  }

  ngOnInit() {

    this.dataService.lastPageSourceChange.subscribe((pageNumber) => {
      this.currentPage = pageNumber;
    });

    this.userData = JSON.parse(constants.getUserData());
    this.userRole = this.userData.user_type;
    this.userId = this.userData.user_id;
    this.acRt.params.subscribe(params => { this.cityid = params['id']; });
    this.getCities();
    this.getProperties();

  }
  /** Function Search property with city */
  searchProperty() {
    this.getProperties();
  }
  /** FUnction to get City List */
  getCities() {
    const params = new HttpParams();
    this.housewiseService.getCityList(params, this.authToken).subscribe((result) => {
      for (const key in result) {
        if (key === "Success") {
          this.cityList = result.Success;
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(3);
          }
          return;
        }
      }
    }, (err) => {
      console.log(err);
    });

  }
  /** Function to get property list */
  getProperties() {
    this.loadingList = true;
    let params = new HttpParams();
    if (this.userData.user_type.toUpperCase() ===constants.owner.toUpperCase()) {
      params = new HttpParams().set('owner_id', this.userId + '').set('page', this.currentPage + '');
    } else {
      params = new HttpParams().set('city_id', this.cityid + '').set('user_id', this.userId + '').set('page', this.currentPage + '');
    }

    this.housewiseService.getPropertyList(params, this.authToken).subscribe((result) => {
      this.loadingList = false;
      // console.log("response " + result);
      for (const key in result) {
        if (key === "Success") {
          this.pageData = result.Success.data;
          //  console.log("pageData " + JSON.stringify(this.pageData));
          this.pageCount = Number(result.Success.pageCount);
          this.createPaginationArray();
          //this.getAddress();
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(2);
          } else if (this.currentPage < 2) {
            this.dataError = true;
          }
          return;
        }
      }
    }, (err) => {
      this.loadingList = false;
      console.log(err);
      this.dataError = true;
    });

  }

  // getAddress() {
  //   this.address = '';
  //   if(this.pageData) {

  //   }
  // }


  //Change Page Number
  changePage(pageNumber) {
    this.currentPage = pageNumber;
    this.getProperties();
  }

  //Go to previous page
  previousPage() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getProperties();
    }
  }

  //Go to next page
  nextPage() {
    if (this.currentPage < this.pageCount) {
      this.currentPage++;
      this.getProperties();
    }
  }


  /** Create Paginator */
  createPaginationArray() {
    this.paginationArray = [];
    let firstIndex = Math.floor(this.currentPage / 10) + 1;
    if (firstIndex > 1) {
      firstIndex = firstIndex * 10 - 10;
    }
    for (let index = firstIndex, k = 0; index <= this.pageCount && k < 10; index++ , k++) {
      this.paginationArray.push(index);
    }
    // console.log('paginationArray' + this.paginationArray);
  }

  /*** Funtion to refresh token after Expiration */
  refreshAuth(type) {
    const userId = { 'id': this.userData.user_id };
    this.housewiseService.updateAuthToken(JSON.stringify(userId)).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          constants.setUserData(JSON.stringify(result.Success));
          this.userData = result.Success;
          this.authToken = this.userData.Authorization;
          if (type === 2) {
            this.getProperties();
          } else if (type === 3) {
            this.getCities();
          } else if (type === 4) {
            this.shortListProperty(this.subModule, this.propertyId);
          } else if (type === 5) {
            this.interestedProperty(this.subModule, this.propertyId);
          }
        } else if (key === 'Error') {
          //error logic here
        }
      }
    }, (err) => {
      console.log(err);
    });
  }



  /** FUnction to Short List Property */
  shortListProperty(subModule, propertyId) {
    this.subModule = subModule;
    this.propertyId = propertyId;
    subModule.sactive = !subModule.sactive;
    const data = { 'user_id': this.userData.user_id, 'property_id': propertyId };
    this.housewiseService.shortListProperty(JSON.stringify(data), this.authToken).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          //success
        } else if (key === 'Error') {
          //error logic here
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(4);
          }
        }
      }
    }, (err) => {
      console.log(err);
    });

  }


  viewDetails(propertyId) {
    // console.log(propertyId);
    this.dataService.lastPageNumber(this.currentPage);
    this.router.navigate(['/user/detail-page', propertyId]);
  }

  /** FUnction to Interested Property */
  interestedProperty(subModule, propertyId) {
    this.subModule = subModule;
    this.propertyId = propertyId;
    subModule.iactive = !subModule.iactive;
    const data = { 'user_id': this.userData.user_id, 'property_id': propertyId };
    this.housewiseService.interestedProperty(JSON.stringify(data), this.authToken).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          //success
        } else if (key === 'Error') {
          //error logic here
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(5);
          }
        }
      }
    }, (err) => {
      console.log(err);
    });
  }

  editProperty(propertyObject) {
    localStorage.setItem('editPropertyData', JSON.stringify(propertyObject));
    this.router.navigate(['user/submit-property', propertyObject.id]);
  }

  getPropertyImage(item) {
    if (item.images.length > 0) {
      return item.images[0].image;
    } else {
      return './assets/images/no_photo_available.png';
    }
  }

  // TODO: Cross browsing
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  getAddress(item) {
    let addr = '';
    if (item.addr1) {
      addr += item.addr1;
    }

    if (item.addr2 && item.addr2 != '') {
      addr += ", " + item.addr2;
    }

    if (item.area && item.area != '') {
      addr += ", " + item.area;
    }

    if (item.cityName && item.cityName != '') {
      addr += ", " + item.cityName;
    }

    if (item.countryName && item.countryName != '') {
      addr += ", " + item.countryName;
    }
    return addr;
  }

}
