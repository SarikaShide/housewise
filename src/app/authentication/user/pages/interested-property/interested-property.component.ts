import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { HousewiseService } from 'src/app/service/housewise.service';
import { HttpParams } from '@angular/common/http';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { ToastrService } from 'ngx-toastr';
import { Title } from '@angular/platform-browser';
import { constants } from 'src/app/constants';

@Component({
  selector: 'app-interested-property',
  templateUrl: './interested-property.component.html',
  styleUrls: ['./interested-property.component.css']
})
export class InterestedPropertyComponent implements OnInit {

  userRole = '';
  userData: any;
  authToken: any;
  pageData: any;
  cityid: any;
  userId: any;
  listGrid: any;
  propertyId: any;

  isLoading = false;
  isRemoving = false;
  loadingList = false;
  dataError = false;

  subModule;
  type;

  constructor(private formBuilder: FormBuilder, private housewiseService: HousewiseService, private toastr: ToastrService,
    private cd: ChangeDetectorRef, private router: Router, private acRt: ActivatedRoute,
    private titleService: Title) {
    this.userData = JSON.parse(constants.getUserData());
    this.authToken = this.userData.Authorization;
  }

  ngOnInit() {
    this.userData = JSON.parse(constants.getUserData());
    this.userRole = this.userData.user_type;
    this.userId = this.userData.user_id;
    this.acRt.params.subscribe(params => { this.cityid = params['id']; });
    this.getInterestedProperties();
  }
  /** Function to get property list */
  getInterestedProperties() {
    this.loadingList = true;
    const params = new HttpParams().set('user_id', this.userId + '');
    this.housewiseService.getInterestedProperty(params, this.authToken).subscribe((result) => {
      this.loadingList = false;
      for (const key in result) {
        if (key === "Success") {
          this.pageData = result.Success.data;
          this.listGrid = [];
          let k = -1;
          this.listGrid[0] = [];
          if (this.pageData.length > 1) {
            this.listGrid[1] = [];
          }
          for (let i = 0; i < this.pageData.length; i++) {
            let key = i % 2;
            //   console.log(i + '%' + '2' + '--' + key)
            this.listGrid[key].push(this.pageData[i]);
          }
          //console.log('property Grid:' + JSON.stringify(this.listGrid));
        } else if (key === 'Error') {
          this.listGrid = [];
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(2);
          } else {
            this.dataError = true;
          }
          return;
        }
      }
    }, (err) => {
      this.loadingList = false;
      console.log(err);
    });
  }

  shorlistInterestedProperty(subModule, propertyId, type) {
    this.subModule = subModule;
    this.propertyId = propertyId;
    this.type = type;

    const data = {
      'user_id': this.userData.user_id,
      'property_id': propertyId,
      'type': type
    };

    subModule.iactive = !subModule.iactive;
    subModule.sactive = false;
    this.isRemoving = true;
    this.isLoading = true;
    this.housewiseService.shorlistInterestedProperty(JSON.stringify(data), this.authToken).subscribe((result) => {
      this.isRemoving = false;
      this.isLoading = false;
      for (const key in result) {
        if (key === 'Success') {
          this.toastr.success('Property Removed from Interested ', 'Success:');
          this.getInterestedProperties();
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(5);
          } else {
            this.toastr.error(result.Error, 'Error:');
          }
        }
      }
    }, (err) => {
      this.isRemoving = false;
      this.isLoading = false;
      console.log(err);
    });
  }

  /*** Funtion to refresh token after Expiration */
  refreshAuth(type) {
    const userId = { 'id': this.userData.user_id };
    this.housewiseService.updateAuthToken(JSON.stringify(userId)).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          constants.setUserData(JSON.stringify(result.Success));
          this.userData = result.Success;
          this.authToken = this.userData.Authorization;
          if (type === 2) {
            this.getInterestedProperties();
          }
          if (type === 3) {
            this.interestedProperty(this.propertyId);
          }
          if (type === 5) {
            this.shorlistInterestedProperty(this.subModule, this.propertyId, this.type);
          }

        } else if (key === 'error') {
          //error logic here
        }
      }
    }, (err) => {
      console.log(err);
    });
  }
  /** FUnction to Interested Property */
  interestedProperty(propertyId) {
    this.propertyId = propertyId;
    const data = { 'user_id': this.userData.user_id, 'property_id': propertyId };
    this.housewiseService.interestedProperty(JSON.stringify(data), this.authToken).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          this.toastr.success('Property Removed from Interested ', 'Success:');
          this.getInterestedProperties();
        } else if (key === 'Error') {
          this.refreshAuth(3);
        }
      }
    }, (err) => {
      console.log(err);
    });
  }

  viewDetails(propertyId) {
    //  console.log(propertyId);
    //this.dataService.lastPageNumber(this.currentPage);
    this.router.navigate(['/user/detail-page', propertyId]);
  }

  getImageUrl(row) {
    if (row.images && row.images.length > 0) {
      return row.images[0].image;
    } else {
      return './assets/images/no_photo_available.png';
    }
  }

  getAddress(item) {
    let addr = '';
    if (item.addr1 != null) {
      addr += item.addr1;
    }

    if (item.addr2 && item.addr2 != '') {
      addr += ", " + item.addr2;
    }

    if (item.area && item.area != '') {
      addr += ", " + item.area;
    }

    if (item.cityName && item.cityName != '') {
      addr += ", " + item.cityName;
    }

    if (item.countryName && item.countryName != '') {
      addr += ", " + item.countryName;
    }
    return addr;
  }
}