export interface PropertyModel {
    id: string;
    userId: string;
    propertyType: string;
    city: string;
    typeOfAServices: string;
    details: Details;
    features: Features;
    preference: Preference;
    images: any[];
}

export interface Details {
    furnishing: string;
    availFrom: string;
    propertyAge: string;
    addr1: string;
    addr2: string;
    area: string;
    city: string;
    pincode: string;
    country: string;
}

export interface Features {
    occupancy: string;
    bedrooms: string;
    bathrooms: string;
    balconies: string;
    builtup: string;
    units: string;
    floorlvl: string;
    totalFloor: string;
}

export interface Preference {
    exptRent: string;
    security: string;
    rentType: string;
    amenities: string[];
}



