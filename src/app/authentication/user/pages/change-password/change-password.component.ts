import { constants } from './../../../../constants';
import { Component, OnInit } from '@angular/core';
import { HousewiseService } from 'src/app/service/housewise.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  isSubmitted = false;
  // oldPassword: string;
  // newPassword: string;
  // confirmPassword: string;
  userData: any;
  authToken: any;
  param: any;
  userId: any;
  userEmail: any;
  passwordMismatch: boolean = false;
  changePasswordForm: FormGroup;
  invalid: boolean = false;
  tempForm: any;

  constructor(private formBuilder: FormBuilder, private housewiseService: HousewiseService,
    private toastr: ToastrService, private router: Router,
    private titleSerive: Title) {
    this.userData = JSON.parse(constants.getUserData());
    this.authToken = this.userData.Authorization;
    this.userId = this.userData.user_id;
    this.userEmail = this.userData.user_email_id;
  }

  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group(
      {
        oldPassword: ['', [Validators.required]],
        // password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]]
        newPassword: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', Validators.required]
      }
    );
  }

  get control(): any {
    return this.changePasswordForm.controls;
  }

  changePassword(form) {
    // console.log(form);
    // console.log(form.value);
    this.isSubmitted = true;

    if (form.invalid) {
      //  console.log("in if");
      this.invalid = true;
      this.toastr.error("Please fill valid details", "Error:");
      //  console.log("in submit");
      return;
    }

    if ((form.value.newPassword !== form.value.confirmPassword)) {
      this.passwordMismatch = true;
      return;
    }
    // console.log("invalid: ", this.invalid);

    this.invalid = false;


    this.param = {
      id: this.userId,
      email: this.userEmail,
      old_password: form.value.oldPassword,
      new_password: form.value.newPassword
    }

    this.tempForm = form;

    this.housewiseService.changePassword(this.param, this.authToken).subscribe((result: any) => {
      // console.log("result: ", result);
      for (const key in result) {
        if (key === "Success") {
          //  console.log("success " + result.Success);
          this.toastr.success('Password Changed', 'Success:');
          if (this.userData.user_type.toUpperCase() == constants.tenant.toUpperCase()) {
            this.router.navigate(['/user/tenant-dashboard']);
          } else if (this.userData.user_type.toUpperCase() == constants.owner.toUpperCase()) {
            this.router.navigate(['/user/owner-dashboard']);
          }
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            //  console.log("Authorization token not valid", result.Error);
            this.refreshAuth(2);
          }
        }
      }
    }, (e) => {
      console.log(e);
      // console.log(e.error.Error);
      if (e.error.Error === 'Old password and new password can not be same') {
        // console.log("Old password and new password can not be same", e.error.Error);
        this.toastr.error("Current password and new password can not be same", "Error:");
      } else if (e.error.Error === 'old Password is Incorrect') {
        // console.log("old Password is Incorrect", e.error.Error);
        this.toastr.error("Current Password is Incorrect", "Error:");
      }
    });

  }


  refreshAuth(type) {
    const userId = { 'id': this.userData.user_id };
    // console.log('Id:' + JSON.stringify(userId));
    this.housewiseService.updateAuthToken(JSON.stringify(userId)).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          constants.setUserData(JSON.stringify(result.Success));
          this.userData = result.Success;
          this.authToken = result.Success.Authorization;
          //  console.log('Success' + result.Success);
          if (type === 2) {
            this.changePassword(this.tempForm);
          }
        } else if (key === 'Error') {
          // console.log(result.Error)
        }
      }
      // console.log(JSON.stringify(result));
    }, (err) => {
      console.log(err);
    });
  }
}