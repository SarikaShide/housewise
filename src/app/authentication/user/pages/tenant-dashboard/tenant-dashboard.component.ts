import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HousewiseService } from 'src/app/service/housewise.service';
import { ToastrService } from 'ngx-toastr';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { constants } from 'src/app/constants';

@Component({
  selector: 'app-tenant-dashboard',
  templateUrl: './tenant-dashboard.component.html',
  styleUrls: ['./tenant-dashboard.component.css']
})
export class TenantDashboardComponent implements OnInit {
  userRole = '';
  selectedCityId;
  cityList: any;
  authToken: any;
  userData: any;
  userName: any;

  loadingList = false;
  dataError = false;
  tenantPropertyCount;

  // date = constants.newUserDate;
  // userDateError = false;

  agreement: any;


  constructor(private router: Router, private housewiseService: HousewiseService, private toastr: ToastrService) {
    this.userData = JSON.parse(constants.getUserData());
    this.authToken = this.userData.Authorization;
    localStorage.removeItem('cityId');
  }

  ngOnInit() {

    //this.checkUserDate();
    this.userRole = localStorage.getItem('userRole');
    this.userData = JSON.parse(constants.getUserData());
    this.userName = this.userData['first_name'];
    this.getCityList();

    this.selectedCityId = "Select";

    this.getTenantPropertyCount();
    // this.getTenantAgreement();
  }

  // checkUserDate() {    
  //   var nowDate = moment(this.date, "YYYY-MM-DD");
  //   if (this.userData.date) {
  //     var userDate = moment(this.userData.date, "YYYY-MM-DD")
  //     if (userDate<nowDate) {
  //      // console.log("pavan", userDate, nowDate);
  //       this.userDateError = true;        
  //     }
  //   }
  // }

  getTenantPropertyCount() {
    this.loadingList = true;
    const params = new HttpParams().set('user_id', this.userData.user_id + '');
    this.housewiseService.getTenantPropertyCount(params, this.authToken).subscribe((res) => {
      this.loadingList = false;
      for (let key in res) {
        if (key === 'Success') {
          this.tenantPropertyCount = res.Success;
        } else if (key === 'Error') {
          //  console.log(res.Error);
          if (res.Error === 'Authorization token not valid') {
            this.refreshAuth('2');
          }
          return;
        }
      }
    }, (e) => {
      this.loadingList = false;
      console.log(e);
    });
  }

  getCityList() {
    this.loadingList = true;
    this.housewiseService.getComboList(this.authToken).subscribe((result) => {
      this.loadingList = false;
      //  console.log("response " + result);
      for (const key in result) {
        if (key === "Success") {
          this.loadingList = false;
          //  console.log("success " + result.Success);
          this.cityList = result.Success.city;
          //this.selectedCityId = this.cityList[0].city_id;
          //  console.log("City List is..." + JSON.stringify(this.cityList));
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth('1');
          }
          return;
        }
      }
    }, (err) => {
      this.loadingList = false;
      console.log(err);
    });
  }

  onSelect($event) {
    this.selectedCityId = $event.target.value;
    //  console.log(this.selectedCityId);
  }

  onFindHomes() {
    if (this.selectedCityId === 'Select') {
      this.toastr.error("Please select valid city", "Error:");
      return;
    }
    let id = this.selectedCityId;
    localStorage.setItem('cityId', this.selectedCityId);
    this.router.navigate(['/user/tenant-property-list', this.selectedCityId]);
  } 

  getTenantAgreement() {
    this.housewiseService.getAgreementDocument(this.userData.user_id, this.authToken).subscribe((result) => {
      for(const key in result) {
        if (key == 'Success') {
          this.agreement = result.Success;
        } else if (key === 'Error') {
          if(result.Error === 'Authorization token not valid') {
            this.refreshAuth('3')
          }
        }
      }
    }, (err) => {
      console.log(err);
    });
  }

  refreshAuth(type) {
    const userId = { 'id': this.userData.user_id };
    //  console.log('Id:' + JSON.stringify(userId));
    this.housewiseService.updateAuthToken(JSON.stringify(userId)).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          constants.setUserData(JSON.stringify(result.Success));
          this.userData = result.Success;
          this.authToken = result.Success.Authorization;
          //  console.log('Success' + result.Success);
          if (type === '1') {
            this.getCityList();
          } else if (type === '2') {
            this.getTenantPropertyCount();
          } else if (type === '3') {
            this.getTenantAgreement();
          }
        } else if (key === 'Error') {
          console.log(result.Error);
        }
      }
      //  console.log(JSON.stringify(result));
    }, (err) => {
      console.log(err);
    });
  }

  gotoUserDocuments() {    
    localStorage.setItem('goto', 'true');
    this.router.navigate(['/user/profile']);
  }

  // TODO: Cross browsing
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }




}
