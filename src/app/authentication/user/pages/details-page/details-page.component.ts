import { constants } from './../../../../constants';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HousewiseService } from 'src/app/service/housewise.service';
import { HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';
import * as $ from 'jquery';
import { ToastrService } from 'ngx-toastr';
import { Title } from '@angular/platform-browser';



@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.css']
})
export class DetailsPageComponent implements OnInit, OnDestroy {

  // slideConfig = { slidesToShow: 1, slidesToScroll: 1, asNavFor: '.child' };
  // slideConfig2 = {
  //   slidesToShow: 4, slidesToScroll: 1, asNavFor: '.main', dots: false,
  //   centerMode: false, arrows: false, focusOnSelect: true
  // };

  slideConfig = {
    "slidesToShow": 5,
    "slidesToScroll": 1,
    "dots": true,
    "infinite": true,
    "autoplay": false,
    "autoplaySpeed": 1500,
    "mobileFirst": true,
    "responsive": [
      {
        "breakpoint": 300,
        "settings": {
          "slidesToShow": 1
        }
      },
      {
        "breakpoint": 1000,
        "settings": {
          "slidesToShow": 7
        }
      }
    ]
  };

  // slideConfig = {
  //   "slidesToShow": 4,
  //   "slidesToScroll": 1,
  //   "nextArrow": "<div class='nav-btn next-slide'></div>",
  //   "prevArrow": "<div class='nav-btn prev-slide'></div>",
  //   "dots": true,
  //   "infinite": false
  // };



  userData: any;
  propertyData: any;
  propertyImages;
  noImage = false;
  noImageUrl = constants.getNoImage();
  similarPropertyData = [];


  interestedTenants: any;

  property_id: any;
  previousList_page: any;

  authToken: any;

  comboList: any;
  amenitiesMatrix: any;
  amenities = [];
  //rentTo = [];
  rentTo: any;
  currentShortInterestype;

  rentToArray = [
    { id: 1, name: 'Anyone', checked: false },
    { id: 2, name: 'Family', checked: false },
    { id: 3, name: 'Single Men', checked: false },
    { id: 4, name: 'Single Women', checked: false },
  ];

  services: any[];
  propertyAge: any;
  propertyType: any;
  city: any;
  cityId;
  country: any;
  isTenant: boolean = false;
  typeOfServicesString;

  property;
  status;

  isLoading = false;
  interestedLoading = false;
  shortlistedLoading = false;
  approvedBtnPressed = false;
  rejectedBtnPressed = false;

  userTypeOwner = constants.owner.toUpperCase();
  userTypeTenant = constants.tenant.toUpperCase();
  userRole;



  constructor(private activatedRoute: ActivatedRoute, private housewiseService: HousewiseService,
    private router: Router, private location: Location, private toaster: ToastrService,
    private titleService: Title) {
    this.userData = JSON.parse(constants.getUserData());
    this.authToken = this.userData.Authorization;
    this.userRole = this.userData.user_type.toUpperCase();
    //console.log(this.authToken);    
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      this.property_id = params['propertyId'];
      this.previousList_page = params['pageNumber'];
    });

    this.getProperty();
    this.createRentToArray();
    this.getTenantList();

  }

  getPropertyDetails(item) {

    this.property_id = item.id;
    this.propertyData = undefined;
    this.propertyImages = undefined;
    this.getProperty();

    // let element = document.getElementById('top').offsetTop;
    // let time = 2000;

    // $("body, html").animate({
    //   scrollTop: $("#" + element).offset().top
    // }, time);


    window.scrollTo(0, 0);

    // window.scroll({ 
    //   top: 0,
    //   left: 0,
    //   behavior: 'smooth'
    // });
  }

  getComboList() {
    this.housewiseService.getComboList(this.authToken).subscribe((result) => {
      //console.log(result);
      for (const key in result) {
        if (key === 'Success') {
          this.comboList = result.Success;
          // console.log("Combo List", this.comboList);
          this.amenitiesMatrix = [];
          this.createAmenitiesArray();
          // console.log("Amenities Matrix", this.amenitiesMatrix);
          this.getTypeOfServicesString();
          this.getPropertyType();
          this.getCity();
          this.getCountry();
        } else if (key === 'Error') {
          //console.log("in error");
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(2);
          }
        }
      }
    });

  }

  getTypeOfServices() {
    this.services = [];
    // console.log(this.comboList.services);
    for (let i = 0; i < this.comboList.services.length; i++) {
      if (this.propertyData && this.propertyData != null) {
        if (this.propertyData.typeOfAServices === this.comboList.services[i].services_master_id) {
          this.services.push(this.comboList.services[i]);
        }
      }
    }
  }

  getPropertyType() {
    this.propertyType;
    for (let i = 0; i < this.comboList.propertyTypes.length; i++) {
      if (this.propertyData && this.propertyData != null) {
        if (this.propertyData.propertyType === this.comboList.propertyTypes[i].ptype_id) {
          this.propertyType = this.comboList.propertyTypes[i].ptype_name;
        }
      }
    }
  }

  getCity() {
    this.city;
    for (let i = 0; i < this.comboList.city.length; i++) {
      if (this.propertyData && this.propertyData != null) {
        if (this.propertyData.city === this.comboList.city[i].city_id) {
          this.city = this.comboList.city[i].city_name;
        }
      }
    }
    // console.log(this.city);
  }

  getCountry() {
    this.country;
    for (let i = 0; i < this.comboList.country.length; i++) {
      if (this.propertyData && this.propertyData != null) {
        if (this.propertyData.details.country === this.comboList.country[i].country_id) {
          this.country = this.comboList.country[i].country_name;
        }
      }
    }
    // console.log(this.country);
  }


  createAmenitiesArray() {
    this.amenities = [];
    for (let i = 0; i < this.comboList.amenities.length; i++) {
      if (this.propertyData && this.propertyData != null) {
        this.propertyData.preference.amenities.forEach(element => {
          if (element === this.comboList.amenities[i].ami_id) {
            this.amenities.push(this.comboList.amenities[i]);
          }
        });
      }
    }
    // console.log(this.amenities);
  }


  createRentToArray() {
    // this.rentTo = [];
    // for(let i=0; i<this.rentToArray)

    // console.log(this.rentToArray);    
  }

  getRentToString() {
    let str = '';
    this.propertyData.preference.rentTO.forEach(element => {
      str += element + ', ';
    });
    // console.log(str);
    this.rentTo = str.replace(/,\s*$/, "");
    // console.log(this.rentTo);

  }

  getTypeOfServicesString() {
    let str = '';
    this.propertyData.typeOfAServices.forEach(element => {

      str += element.services_name + ', ';
    });
    //  console.log(str);
    this.typeOfServicesString = str.replace(/,\s*$/, "");
    // console.log(this.typeOfServicesString);
  }


  getProperty() {
    this.isLoading = true;
    const params = new HttpParams().set('property_id', this.property_id + '').set('user_id', this.userData.user_id + '');
    this.housewiseService.getPropertyDetails(params, this.authToken).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          this.propertyData = result.Success;
          this.propertyImages = this.propertyData.images;
          this.isLoading = false;
          if (this.propertyImages.length == 0) {
            this.noImage = true;
          } else {
            this.noImage = false;
          }
          this.getComboList();
          this.getSimilarProperties();
          this.createRentToArray();
          this.getRentToString();

          return;
        } else if (key === 'Error') {
          this.isLoading = false;
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(1);
          }
          return;
        }
      }
    }, (err) => {
      this.isLoading = false;
      console.log(err);
    });
    return;
  }

  getTenantList() {
    const params = new HttpParams().set('property_id', this.property_id + '');
    this.housewiseService.getTenantList(params, this.authToken).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          this.interestedTenants = result.Success;
          this.isTenant = true;
          // console.log("tenant list: ", this.interestedTenants);
          return;
        } else if (key === 'Error') {
          // console.log("Error: ", result.Error);
          // console.log("tenant list: ", this.interestedTenants);
          this.isTenant = false;
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(2);
          }
          return;
        }
      }
    }, (err) => {
      this.isTenant = false;
      console.log(err);
    });
    return;
  }

  checkStatus(item) {
    try {
      if ((item.status == 'Verification Complete' || item.status == 'Agreement Complete' || item.status == 'Fees Transferred' || 
           item.status == 'Confirmed') && item.tenant_detail_status == '') {
        return 1;
      } else if ((item.status == 'Verification Complete' || item.status == 'Agreement Complete' || item.status == 'Fees Transferred' || 
                  item.status == 'Confirmed' || item.status == 'Moved-In') && item.tenant_detail_status == 'Rejected') {
        return 2;
      } else if ((item.status == 'Verification Complete' || item.status == 'Agreement Complete' || item.status == 'Fees Transferred' || 
                  item.status == 'Confirmed' || item.status == 'Moved-In') && item.tenant_detail_status == 'Approved') {
        return 3;
      } else {
        return 4;
      }
    } catch (e) {

    }
  }

  updateAgreementStatus(property, status) {
    // console.log(property);
    // console.log(status);
    this.isLoading = true;

    if (status === 'Approved') {
      this.approvedBtnPressed = true;
    } else if (status === 'Rejected') {
      this.rejectedBtnPressed = true;
    }

    this.property = property;
    this.status = status;

    let params = {
      property_id: property.property_id,
      user_id: property.tenant_id,
      approve_status: status
    };

    // Accepted/Rejected
    this.housewiseService.updateConfirmedTenantApproveStatus(JSON.stringify(params), this.authToken).subscribe((result) => {
      this.approvedBtnPressed = false;
      this.rejectedBtnPressed = false;
      this.isLoading = false;
      for (const key in result) {
        if (key === "Success") {
          this.toaster.success('Tenant ' + status, 'Success:');
          this.getProperty();
          this.getTenantList();
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(6);
          } else {
            //this.dataError = true;
          }
          return;
        }
      }
    }, (err) => {
      this.isLoading = false;
      this.approvedBtnPressed = false;
      this.rejectedBtnPressed = false;
      //this.loadingList = false;
      console.log(err);
    });
  }

  shorlistInterestedProperty(type) {
    this.currentShortInterestype = type;
    const data = {
      'user_id': this.userData.user_id,
      'property_id': this.property_id,
      'type': type
    };

    if(type ==='interested'){
      this.interestedLoading = true;
    } else {
      this.shortlistedLoading = true;
    }
    this.isLoading = true;
    this.housewiseService.shorlistInterestedProperty(JSON.stringify(data), this.authToken).subscribe((result) => {
      this.isLoading = false;
      this.interestedLoading = false;
      this.shortlistedLoading = false;
      for (const key in result) {
        if (key === 'Success') {
          if (type === 'interested') {
            this.propertyData.iactive = !this.propertyData.iactive;
            this.propertyData.sactive = false;
            if (this.propertyData.iactive == true) {
              this.toaster.success("Property added to interested", "Success:");
            } else {
              this.toaster.success("Property removed from interested", "Success:");
            }
          } else {
            this.propertyData.sactive = !this.propertyData.sactive;
            this.propertyData.iactive = false;
            if (this.propertyData.sactive == true) {
              this.toaster.success("Property added to shortlisted", "Success:");
            } else {
              this.toaster.success("Property removed from shortlisted", "Success:");
            }
          }
        } else if (key === 'Error') {

          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(4);
          } else {
            this.toaster.error(result.Error, 'Error:');
          }
        }
      }
    }, (err) => {
      this.isLoading = false;
      this.interestedLoading = false;
      this.shortlistedLoading = false;
      console.log(err);
    });
  }

  getSimilarProperties() {
    this.similarPropertyData = [];
    const params = new HttpParams().set('city_id', this.propertyData.city + '').set('no_of_bedrooms', this.propertyData.features.bedrooms + '');
    this.housewiseService.getPropertyList(params, this.authToken).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          let data = result.Success.data;
          data.forEach(element => {
            if (element.id !== this.propertyData.id) {
              this.similarPropertyData.push(element);
            }
            //  console.log("similar properties: ",this.similarPropertyData);

          });

          return;
        } else if (key === 'Error') {
          // console.log("Error: ", result.Error);
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(5);
          }
          return;
        }
      }
    }, (err) => {
      // console.log(err);
    });
    return;
  }


  refreshAuth(type) {
    const userId = { 'id': this.userData.user_id };
    //console.log('Id:' + JSON.stringify(userId));
    this.housewiseService.updateAuthToken(JSON.stringify(userId)).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          constants.setUserData(JSON.stringify(result.Success));
          this.userData = result.Success;
          this.authToken = this.userData.Authorization;
          // console.log('Success' + this.userData);
          if (type === 1) {
            this.getProperty();
          } else if (type === 2) {
            this.getComboList();
          } else if (type === 3) {
            this.getTenantList();
          } else if (type === 4) {
            this.shorlistInterestedProperty(this.currentShortInterestype);
          } else if (type === 5) {
            this.getSimilarProperties();
          } else if (type == 6) {
            this.updateAgreementStatus(this.property, this.status);
          }
        } else if (key === 'error') {

        }
      }
      // console.log(JSON.stringify(result));
    }, (err) => {
      // console.log(err);
    });
  }

  goToListingPage() {
    this.location.back();
  }

  // TODO: Cross browsing
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  ngOnDestroy(): void {
    localStorage.removeItem('propertyData');
  }

  getPropertyImage(item) {
    if (item.images.length > 0) {
      return item.images[0].image;
    } else {
      return './assets/images/no_photo_available.png';
    }
  }

  getAddress(item) {
    // console.log(item);
    let addr = '';

    if (item.addr1 && item.addr1 !== undefined && item.addr1 !== '') {
      addr += item.addr1;
    }

    if (item.addr2 && item.addr2 != '') {
      addr += ", " + item.addr2;
    }

    if (item.area && item.area != '') {
      addr += ", " + item.area;
    }

    if (item.cityName && item.cityName != '') {
      addr += ", " + item.cityName;
    }

    if (item.countryName && item.countryName != '') {
      addr += ", " + item.countryName;
    }
    return addr;
  }

  editProperty(propertyObject) {
    localStorage.setItem('editPropertyData', JSON.stringify(propertyObject));
    this.router.navigate(['user/submit-property', propertyObject.id]);
  }

}
