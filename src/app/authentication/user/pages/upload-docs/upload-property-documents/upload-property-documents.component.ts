import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';
import { HousewiseService } from 'src/app/service/housewise.service';
import { constants } from 'src/app/constants';
import { Route } from '@angular/compiler/src/core';

@Component({
	selector: 'app-upload-property-documents',
	templateUrl: './upload-property-documents.component.html',
	styleUrls: ['./upload-property-documents.component.css']
})
export class UploadPropertyDocumentsComponent implements OnInit {

	propertyId: any;
	userData: any;
	authToken: any;
	docsArray: any;
	documentsArray: any;
	documentsArrayMatrix: any = [];
	selectedDocIndex = 0;
	selectedDocTypeIndex = 0;
	documentFile: any;
	uploadDocsFormData: FormData = new FormData();
	isLoading = false;
	sizeErrorString: string = 'size must not be greater than 5MB';
	deleteDoc: any;
	deleteDocRow: any;
	deleteDocColumn: any;

	constructor(private router: Router, private activatedRoute: ActivatedRoute, private location: Location,
		private housewiseService: HousewiseService, private toastr: ToastrService) {
		this.userData = JSON.parse(constants.getUserData());
		this.authToken = this.userData.Authorization;
	}

	ngOnInit() {
		this.activatedRoute.params.subscribe(params => {
			this.propertyId = params['propertyId'];
		});

		this.getDocumentTypes();
		this.getDocumentImages();

	}

	getDocumentTypes() {
		const param = new HttpParams().set('user_type', this.userData.user_type + '');
		this.housewiseService.getDocumentType(param, this.authToken).subscribe((result) => {
			//console.log(result);
			for (const key in result) {
				if (key === 'Success') {
					this.docsArray = result.Success;
				} else if (key === 'Error') {
					//	console.log("error in ");
					if (result.Error === 'Authorization token not valid') {
						this.refreshAuth('1');
					}
					return;
				}
			}
		});
	}

	getDocumentImages() {
		const params = new HttpParams().set('property_id', this.propertyId + '');
		//	const params = '';
		this.housewiseService.getDocumentImages(params, this.authToken).subscribe((result) => {
			//console.log("response " + result);
			for (const key in result) {
				if (key === "Success") {
					//	console.log("success " + result.Success);
					this.documentsArray = result.Success;
					this.createDocumentImagesMatrix();
				} else if (key === 'Error') {
					if (result.Error === 'Authorization token not valid') {
						this.refreshAuth('2');
					} else {
						this.documentsArrayMatrix = [];
					}
					return;
				}
			}
		}, (err) => {
			console.log(err);
		});
	}

	createDocumentImagesMatrix() {
		this.documentsArrayMatrix = [];
		let k = 0;
		for (let i = 0; i < this.documentsArray.length; i++) {
			if (i % 4 === 0) {
				k++;
				this.documentsArrayMatrix[k] = [];
			}
			this.documentsArrayMatrix[k].push(this.documentsArray[i]);
		}
		//	console.log(this.documentsArrayMatrix);
	}

	getDocumentFileImage(item) {
		if (item.document_file.indexOf(".pdf") > -1) {
			return './assets/images/pdf_placeholder.png';
		} else if (item.document_file.indexOf(".docx") > -1) {
			return './assets/images/docx_placeholder.png';
		} else {
			return item.document_file;
		}
	}

	getDocumentFileImageUrl(item) {
		return item.document_file;
	}

	docsChanged(event) {
		this.selectedDocIndex = event.target.selectedIndex;
		this.selectedDocTypeIndex = 0;
	}

	docsTypeChanged(event) {
		this.selectedDocTypeIndex = event.target.selectedIndex;
	}

	onFileChange(event) {
		let text = " file selected";
		this.documentFile = '';
		if (event.target.files && event.target.files.length) {
			this.documentFile = event.target.files[0];
			//	console.log(event.target.files[0]);
			if ((this.documentFile.size / 1000) > 5000) {
				this.toastr.error('File size to large', 'Error:');
				this.documentFile = undefined;
				(<HTMLInputElement>document.getElementById('upload_documents')).value = "";
			} else {
				this.uploadDocs();
			}
		}
	}

	uploadDocs() {

		if (this.documentFile) {
			this.uploadDocsFormData = new FormData();
			this.uploadDocsFormData.append('file', this.documentFile);
			this.uploadDocsFormData.append('property_id', this.propertyId);
			this.uploadDocsFormData.append('user_id', this.userData.user_id);
			this.uploadDocsFormData.append('document_proof_id', this.docsArray[this.selectedDocIndex].document_proof_id);
			//this.uploadDocsFormData.append('document_proof_type_id', ((((this.docsArray || {})[this.selectedDocIndex].docType || [])[this.selectedDocTypeIndex] || {}).document_proof_type_id));
			if (this.docsArray[this.selectedDocIndex].docType[this.selectedDocTypeIndex] && this.docsArray[this.selectedDocIndex].docType[this.selectedDocTypeIndex].document_proof_type_id) {
				this.uploadDocsFormData.append('document_proof_type_id', this.docsArray[this.selectedDocIndex].docType[this.selectedDocTypeIndex].document_proof_type_id);
			}
			this.isLoading = true;
			this.housewiseService.uploadPropertyDocs(this.uploadDocsFormData, this.authToken).subscribe((result) => {
				this.isLoading = false;
				//	console.log(result);
				for (const key in result) {
					if (key === 'Success') {
						this.documentFile = undefined;
						(<HTMLInputElement>document.getElementById('upload_documents')).value = "";
						this.documentsArray = result.Success;
						this.createDocumentImagesMatrix();
						this.toastr.success('Property documents added', 'Success:');
						//	this.router.navigate(['/listing-page']);
					} else if (key === 'Error') {
						this.documentFile = undefined;
						(<HTMLInputElement>document.getElementById('upload_documents')).value = "";
						//	console.log("error in ");
						if (result.Error === 'Authorization token not valid') {
							this.refreshAuth('3');
						} else if (result.Error.includes(this.sizeErrorString)) {
							this.toastr.error(result.Error, "Error:");
						}
						return;
					}
				}
				//	console.log(JSON.stringify(result));
			}, (err) => {
				this.isLoading = false;
				console.log(err);
			});
		} else {
			this.toastr.error('Select file', 'Error:');
		}
	}

	deletePropertyDocument(item, row, column) {
		this.deleteDoc = item;
		this.deleteDocRow = row;
		this.deleteDocColumn = column;
		const params = new HttpParams().set('document_id', item.document_id + '').set('property_id', this.propertyId).set('user_id', this.userData.user_id);
		this.housewiseService.deletePropertyDocument(params, this.authToken).subscribe((result) => {
			//	console.log("response: ", result);
			for (const key in result) {
				if (key === "Success") {
					//	console.log("success: ", result.Success);
					this.toastr.success('Property document deleted', 'Success:');
					//delete (this.documentsArrayMatrix[row][column]);
					//	console.log(this.documentsArrayMatrix);
					//this.documentsArrayMatrix[row].splice(column, 1);
					this.getDocumentImages();
					//	console.log(this.documentsArrayMatrix);

					//this.createDocsMatrix();
				} else if (key === 'Error') {
					if (result.Error === 'Authorization token not valid') {
						this.refreshAuth('4');
					} else {
						//  this.toastrService.error('Failed to Delete Property', '', { timeOut: 5000 });
					}
					return;
				}
			}
		}, (err) => {
			console.log(err);
			// this.toastrService.error('Failed to Delete Property', '', { timeOut: 5000 });
		});
	}

	refreshAuth(type) {
		const userId = { 'id': this.userData.user_id };
		//	console.log('Id:' + JSON.stringify(userId));
		this.housewiseService.updateAuthToken(JSON.stringify(userId)).subscribe((result) => {
			for (const key in result) {
				if (key === 'Success') {
					constants.setUserData(JSON.stringify(result.Success));
					this.userData = result.Success;
					this.authToken = result.Success.Authorization;
					//	console.log('Success' + result.Success);
					if (type === '1') {
						this.getDocumentTypes();
					} else if (type === '2') {
						this.getDocumentImages();
					} else if (type === '3') {
						this.uploadDocs();
					} else if (type === '4') {
						this.deletePropertyDocument(this.deleteDoc, this.deleteDocRow, this.deleteDocColumn);
					}
				} else if (key === 'Error') {
					console.log(result.Error);
				}
			}
			//	console.log(JSON.stringify(result));
		}, (err) => {
			console.log(err);
		});
	}

	goToListingPage() {
		this.location.back();
	}


}
