import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';
import { HousewiseService } from 'src/app/service/housewise.service';
import { constants } from 'src/app/constants';

@Component({
  selector: 'app-upload-docs',
  templateUrl: './upload-docs.component.html',
  styleUrls: ['./upload-docs.component.css']
})
export class UploadDocsComponent implements OnInit {



  propertyId;
  tenantId;
  userData;
  authToken;

  documentsArray;
  documentsArrayMatrix = [];

  docsArray;
  selectedDocIndex = 0;
  selectedDocTypeIndex = 0;
  documentFile;
  uploadDocsFormData: FormData = new FormData();

  loadingList = false;
  dataError = false;
  isLoading = false;

  type;
  title = 'My Docs';
  currentUserId;
  currentPropertyId;

  deleteItem;
  deleteItemRow;
  deleteItemColumn;
  isTenant: boolean = false;
  interestedTenants: any;
  property: any;
  status: any;
  approvedBtnPressed: boolean = false;
  rejectedBtnPressed: boolean = false;


  constructor(private activatedRoute: ActivatedRoute, private housewiseService: HousewiseService,
    private toastr: ToastrService, private router: Router, private location: Location) { }

  ngOnInit() {

    this.userData = JSON.parse(constants.getUserData());
    this.authToken = this.userData.Authorization;
    this.getRouteParams();
    // this.getDocumentTypes();
  }

  getRouteParams() {
    this.activatedRoute.params.subscribe(params => {
      this.propertyId = params['propertyId'];
      this.tenantId = params['tenantId'];
      this.type = params['type'];
    });
    if (this.type === 'my_docs') {
      this.title = 'My Documents';
      this.getDocumentImages();
    } else if (this.type === 'agreement') {
      this.title = 'Agreement Documents';
      this.getPropertyAgreement();
    } else if (this.type === 'tenant_uploaded_docs') {
      this.title = 'Tenant Details';
      // this.getUserKYCDocuments(this.propertyId, this.tenantId);
      this.getTenantList();
    } else if (this.type === 'reports') {
      this.title = 'HW Uploaded Documents';
      this.getPropertyReports();
    } else if (this.type === 'tenant_docs') {
      this.title = 'Tenant Uploaded Documents';
      this.getUserKYCDocuments(this.propertyId,this.tenantId);
    }

		this.getDocumentTypes();

  }

  getTenantList() {
    const params = new HttpParams().set('property_id', this.propertyId + '');
    this.housewiseService.getTenantList(params, this.authToken).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          this.interestedTenants = result.Success;
          this.isTenant = true;
          // console.log("tenant list: ", this.interestedTenants);
          return;
        } else if (key === 'Error') {
          // console.log("Error: ", result.Error);
          // console.log("tenant list: ", this.interestedTenants);
          this.isTenant = false;
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(8);
          }
          return;
        }
      }
    }, (err) => {
      this.isTenant = false;
      console.log(err);
    });
    return;
  }

  checkStatus(item) {
    try {
      if ((item.status == 'Verification Complete' || item.status == 'Agreement Complete' || item.status == 'Fees Transferred' || 
           item.status == 'Confirmed') && item.tenant_detail_status == '') {
        return 1;
      } else if ((item.status == 'Verification Complete' || item.status == 'Agreement Complete' || item.status == 'Fees Transferred' || 
                  item.status == 'Confirmed' || item.status == 'Moved-In') && item.tenant_detail_status == 'Rejected') {
        return 2;
      } else if ((item.status == 'Verification Complete' || item.status == 'Agreement Complete' || item.status == 'Fees Transferred' || 
                  item.status == 'Confirmed' || item.status == 'Moved-In') && item.tenant_detail_status == 'Approved') {
        return 3;
      } else {
        return 4;
      }
    } catch (e) {

    }
  }

  updateAgreementStatus(property, status) {
    // console.log(property);
    // console.log(status);
    this.isLoading = true;

    if (status === 'Approved') {
      this.approvedBtnPressed = true;
    } else if (status === 'Rejected') {
      this.rejectedBtnPressed = true;
    }

    this.property = property;
    this.status = status;

    let params = {
      property_id: property.property_id,
      user_id: property.tenant_id,
      approve_status: status
    };

    // Accepted/Rejected
    this.housewiseService.updateConfirmedTenantApproveStatus(JSON.stringify(params), this.authToken).subscribe((result) => {
      this.approvedBtnPressed = false;
      this.rejectedBtnPressed = false;
      this.isLoading = false;
      for (const key in result) {
        if (key === "Success") {
          this.toastr.success('Tenant ' + status, 'Success:');
          // this.getProperty();
          this.getTenantList();
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(9);
          } else {
            //this.dataError = true;
          }
          return;
        }
      }
    }, (err) => {
      this.isLoading = false;
      this.approvedBtnPressed = false;
      this.rejectedBtnPressed = false;
      //this.loadingList = false;
      console.log(err);
    });
  }

  getDocumentImages() {
    const params = new HttpParams().set('property_id', this.propertyId + '');
    //	const params = '';
    this.loadingList = true;
    this.housewiseService.getDocumentImages(params, this.authToken).subscribe((result) => {
      this.loadingList = false;
      this.handleResponse(result, 1);
    }, (err) => {
      this.loadingList = false;
      console.log(err);
    });
  }

  getUserKYCDocuments(propertyId, userId) {
    this.currentUserId = userId;
    this.currentPropertyId = propertyId;
    const params = new HttpParams().set('user_id', userId).set('property_id', propertyId);
    //	const params = '';
    this.loadingList = true;
    this.housewiseService.getUserKYCDocuments(params, this.authToken).subscribe((result) => {
      this.loadingList = false;
      this.handleResponse(result, 2);
    }, (err) => {
      this.loadingList = false;
      console.log(err);
    });
  }

  getPropertyReports() {
    const params = new HttpParams().set('property_id', this.propertyId + '');
    //	const params = '';
    this.loadingList = true;
    this.housewiseService.getPropertyReports(params, this.authToken).subscribe((result) => {
      this.loadingList = false;
      this.handleResponse(result, 3);
    }, (err) => {
      this.loadingList = false;
      console.log(err);
    });
  }

  getPropertyAgreement() {
    const params = new HttpParams().set('property_id', this.propertyId + '');
    //	const params = '';
    this.loadingList = true;
    this.housewiseService.getPropertyAgreement(params, this.authToken).subscribe((result) => {
      this.loadingList = false;
      this.handleResponse(result, 4);
    }, (err) => {
      this.loadingList = false;
      console.log(err);
    });
  }

  handleResponse(result, type) {
    for (const key in result) {
      if (key === "Success") {
        this.documentsArray = result.Success;
        this.createDocumentImagesMatrix();
      } else if (key === 'Error') {
        if (result.Error === 'Authorization token not valid') {
          this.refreshAuth(type);
        } else {
          this.dataError = true;
        }
      }
    }
  }


  deletePropertyDocument(item, row, column) {
    this.deleteItem = item;
    this.deleteItemRow = row;
    this.deleteItemColumn = column;
    const params = new HttpParams().set('document_id', item.document_id + '').set('property_id', this.propertyId).set('user_id', this.userData.user_id);
    this.housewiseService.deletePropertyDocument(params, this.authToken).subscribe((result) => {
    //  console.log("response: ", result);
      for (const key in result) {
        if (key === "Success") {
        //  console.log("success: ", result.Success);
          this.toastr.success('Document deleted', 'Success:');
          this.getDocumentImages();
          //this.documentsArrayMatrix[row].splice(column, 1);
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(6);
          } else {
            //  this.toastrService.error('Failed to Delete Property', '', { timeOut: 5000 });
          }
          return;
        }
      }
    }, (err) => {
      console.log(err);
      // this.toastrService.error('Failed to Delete Property', '', { timeOut: 5000 });
    });
  }

  getDocumentTypes() {
    const param = new HttpParams().set('user_type', this.userData.user_type + '');
    this.housewiseService.getDocumentType(param,this.authToken).subscribe((result) => {
    //  console.log(result);
      for (const key in result) {
        if (key === 'Success') {
          this.docsArray = result.Success;
        } else if (key === 'Error') {
        //  console.log("error in ");
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(5);
          }
          return;
        }
      }
    });
  }

  uploadDocs() {

    if (this.documentFile) {
      this.uploadDocsFormData = new FormData();
      this.uploadDocsFormData.append('file', this.documentFile);
      this.uploadDocsFormData.append('property_id', this.propertyId);
			this.uploadDocsFormData.append('user_id', this.userData.user_id);
      this.uploadDocsFormData.append('document_proof_id', this.docsArray[this.selectedDocIndex].document_proof_id);
      //this.uploadDocsFormData.append('document_proof_type_id', ((((this.docsArray || {})[this.selectedDocIndex].docType || [])[this.selectedDocTypeIndex] || {}).document_proof_type_id));
      if (this.docsArray[this.selectedDocIndex].docType[this.selectedDocTypeIndex] && this.docsArray[this.selectedDocIndex].docType[this.selectedDocTypeIndex].document_proof_type_id) {
        this.uploadDocsFormData.append('document_proof_type_id', this.docsArray[this.selectedDocIndex].docType[this.selectedDocTypeIndex].document_proof_type_id);
      }
      this.isLoading = true;
      this.housewiseService.uploadPropertyDocs(this.uploadDocsFormData, this.authToken).subscribe((result) => {
        this.isLoading = false;
      //  console.log(result);
        for (const key in result) {
          if (key === 'Success') {
            document.getElementById('modelClose').click();
            this.documentFile = undefined;
            (<HTMLInputElement>document.getElementById('upload_documents')).value = "";
            this.documentsArray = result.Success;
            this.createDocumentImagesMatrix();
            this.toastr.success('Document uploaded successfully', 'Success:');
          } else if (key === 'Error') {
          //  console.log("error in ");
            if (result.Error === 'Authorization token not valid') {
              this.refreshAuth(7);
            }
            return;
          }
        }
      //  console.log(JSON.stringify(result));
      }, (err) => {
        this.isLoading = false;
        console.log(err);
      });
    } else {
      this.toastr.error('Select file', 'Error:');
    }
  }

  refreshAuth(type) {
    const userId = { 'id': this.userData.user_id };
    this.housewiseService.updateAuthToken(JSON.stringify(userId)).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          constants.setUserData(JSON.stringify(result.Success));
          this.userData = result.Success;
          this.authToken = result.Success.Authorization;
        //  console.log('Success' + result.Success);
          if (type === 1) {
            this.getDocumentImages();
          } else if (type === 2) {
            this.getUserKYCDocuments(this.currentPropertyId,this.currentUserId);
          } else if (type === 3) {
            this.getPropertyReports();
          } else if (type === 4) {
            this.getPropertyAgreement();
          } else if (type === 5) {
            this.getDocumentTypes();
          } else if (type === 6) {
            this.deletePropertyDocument(this.deleteItem, this.deleteItemRow, this.deleteItemRow);
          } else if (type === 7) {
            this.uploadDocs();
          } else if (type === 8) {
            this.getTenantList();
          } else if (type === 9) {
            this.updateAgreementStatus(this.property, this.status);
          }

        } else if (key === 'Error') {

        }
      }
    //  console.log(JSON.stringify(result));
    }, (err) => {
      console.log(err);
    });
  }


  createDocumentImagesMatrix() {
    this.documentsArrayMatrix = [];
    let k = 0;
    for (let i = 0; i < this.documentsArray.length; i++) {
      if (i % 3 === 0) {
        k++;
        this.documentsArrayMatrix[k] = [];
      }
      this.documentsArrayMatrix[k].push(this.documentsArray[i]);
    }
  //  console.log(this.documentsArrayMatrix);
  }

  getDocumentFileImage(item) {
    if (item.document_file.indexOf(".pdf") > -1) {
      return './assets/images/pdf_placeholder.png';
    } else if (item.document_file.indexOf(".docx") > -1) {
      return './assets/images/docx_placeholder.png';
    } else {
      return item.document_file;
    }
  }

  getImagePath(item) {
    if (item.path.indexOf(".pdf") > -1) {
      return './assets/images/pdf_placeholder.png';
    } else if (item.path.indexOf(".docx") > -1) {
      return './assets/images/docx_placeholder.png';
    } else {
      return item.path;
    }
  }

  getDocumentFileImageUrl(item) {
    return item.document_file;
  }

  docsChanged(event) {
    this.selectedDocIndex = event.target.selectedIndex;
    this.selectedDocTypeIndex = 0;
  }

  docsTypeChanged(event) {
    this.selectedDocTypeIndex = event.target.selectedIndex;
  }

  onFileChange(event) {
    let text = " file selected";
		this.documentFile = '';
		if (event.target.files && event.target.files.length) {
			this.documentFile = event.target.files[0];
			//	console.log(event.target.files[0]);
			if ((this.documentFile.size / 1000) > 5000) {
				this.toastr.error('File size to large', 'Error:');
				this.documentFile = undefined;
				(<HTMLInputElement>document.getElementById('upload_documents')).value = "";
			} else {
				this.uploadDocs();
			}
		}
  }

  uploadPropertyDocs() {
    this.router.navigate(['user/upload-property-documents', this.propertyId]);
  }

  goToPreviousPage() {
    this.location.back();
  }

  goBack() {
    this.router.dispose()
  }



}
