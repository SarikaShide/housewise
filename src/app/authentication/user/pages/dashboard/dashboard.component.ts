import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { HousewiseService } from 'src/app/service/housewise.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { DataService } from 'src/app/service/data.service';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer, Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { constants } from 'src/app/constants';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

  userData;
  authToken;
  peopertyListData;
  propertyCountData;
  loadingList = false;
  dataError = false;
  propertyCount;

  propertyId;
  AgreementStatus;
  agreementText = '';

  isLoading = false;
  approvedBtnPressed = false;
  rejectedBtnPressed = false;

  // date = constants.newUserDate;
  // userDateError = false;



  constructor(private formBuilder: FormBuilder, private housewiseService: HousewiseService,
    private cd: ChangeDetectorRef, private router: Router, private acRt: ActivatedRoute,
    private dataService: DataService, private toaster: ToastrService,
    private sanitized: DomSanitizer,
    private titleService: Title) {
    this.userData = JSON.parse(constants.getUserData());
    this.authToken = this.userData.Authorization;
  }

  ngOnInit() {
    //this.checkUserDate();
    this.getProperties();
    this.getPropertiesCount();
  }

  // checkUserDate() {
  //   var nowDate = moment(this.date, "YYYY-MM-DD");
  //   if (this.userData.date) {
  //     var userDate = moment(this.userData.date, "YYYY-MM-DD")
  //     if (userDate < nowDate) {
  //       this.userDateError = true;
  //     }
  //   }
  //   //console.log('userData',this.userDateError);
  // }

  checkStatus(item) {
    try {
      if (item.aggrement_status == "Pending Approval" && item.HW_status === 'Adminapprove') {
        return 1;
      } else if (item.aggrement_status == "Rejected") {
        return 2;
      } else if (item.aggrement_status == "Approved") {
        return 3;
      } else {
        return 4;
      }
    } catch (e) {
    
    }
  }

  // checkDocViewStatus(item) {
  //   try {
  //     if (item.HW_status === 'Adminapprove' || item.HW_status === 'Agreement' || item.HW_status === 'Confirmed') {
  //       return 1;
  //     }
  //     if (item.tenantId == "Rejected" && item.HW_status === 'Adminapprove') {
  //       return 2;
  //     }
  //     if (item.HW_status == "Agreement") {
  //       return 3;
  //     } 
  //   } catch (e) {

  //   }
  // }

  showAgreementText(item) {
    // console.log(item.text);
    this.agreementText = item.text;
  }

  /** Function to get property list */
  getProperties() {
    this.loadingList = true;
    let params = new HttpParams();

    params = new HttpParams().set('owner_id', this.userData.user_id + '');

    this.housewiseService.getPropertyList(params, this.authToken).subscribe((result) => {
      this.loadingList = false;
      // console.log("response " + result);
      for (const key in result) {
        if (key === "Success") {
          this.peopertyListData = result.Success.data;

        } else if (key === 'Error') {
          this.peopertyListData = undefined
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(2);
          } else if (result.Error === 'No records available') {
            this.propertyCount = 0;
          } else {
            this.dataError = true;
          }
          return;
        }
      }
    }, (err) => {
      this.dataError = true;
      this.loadingList = false;
      console.log(err);
    });
  }

  updateAgreementStatus(propertyId, status) {
    this.isLoading = true;

    if (status === 'Approved') {
      this.approvedBtnPressed = true;
    } else if (status === 'Rejected') {
      this.rejectedBtnPressed = true;
    }

    this.propertyId = propertyId;
    this.AgreementStatus = status;

    let params = {
      property_id: propertyId,
      agreement_status: status
    };

    // Accepted/Rejected
    this.housewiseService.updateAgreementStatus(JSON.stringify(params), this.authToken).subscribe((result) => {
      this.approvedBtnPressed = false;
      this.rejectedBtnPressed = false;
      this.isLoading = false;
      for (const key in result) {
        if (key === "Success") {
          this.toaster.success('Property Agreement ' + this.AgreementStatus, 'Success:', { timeOut: 4000 });
          this.getProperties();
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(3);
          } else {
            this.dataError = true;
          }
          return;
        }
      }
    }, (err) => {
      this.isLoading = false;
      this.loadingList = false;
      this.approvedBtnPressed = false;
      this.rejectedBtnPressed = false;
      console.log(err);
    });
  }



  getPropertiesCount() {
    this.loadingList = true;
    let params = new HttpParams();

    params = new HttpParams().set('user_id', this.userData.user_id + '');

    this.housewiseService.getPropertyCount(params, this.authToken).subscribe((result) => {
      this.loadingList = false;
      // console.log("response " + result);
      for (const key in result) {
        if (key === "Success") {
          this.propertyCountData = result.Success;
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(4);
          }
          return;
        }
      }
    }, (err) => {
      this.loadingList = false;
      console.log(err);
    });
  }



  /*** Funtion to refresh token after Expiration */
  refreshAuth(type) {
    const userId = { 'id': this.userData.user_id };
    this.isLoading = true;
    this.housewiseService.updateAuthToken(JSON.stringify(userId)).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          this.isLoading = false;
          constants.setUserData(JSON.stringify(result.Success));
          this.userData = result.Success;
          this.authToken = this.userData.Authorization;
          if (type === 2) {
            this.getProperties();
          } else if (type === 3) {
            this.updateAgreementStatus(this.propertyId, this.AgreementStatus);
          } else if (type === 4) {
            this.getPropertiesCount();
          }
        } else if (key === 'error') {
          this.dataError = true;
          //error logic here
        }
      }
    }, (err) => {
      this.isLoading = false;
      this.dataError = true;
      console.log(err);
    });
  }


  // TODO: Cross browsing
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }


}
