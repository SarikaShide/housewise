import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortlistPropertyComponent } from './shortlist-property.component';

describe('ShortlistPropertyComponent', () => {
  let component: ShortlistPropertyComponent;
  let fixture: ComponentFixture<ShortlistPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortlistPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortlistPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
