import { DataService } from 'src/app/service/data.service';
import { Component, OnInit, ChangeDetectorRef, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { HousewiseService } from 'src/app/service/housewise.service';
import { HttpParams } from '@angular/common/http';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { ToastrService } from 'ngx-toastr';
import { IfStmt } from '@angular/compiler';
import { constants } from 'src/app/constants';

@Component({
  selector: 'app-tenant-property-list',
  templateUrl: './tenant-property-list.component.html',
  styleUrls: ['./tenant-property-list.component.css']
})
export class TenantPropertyListComponent implements OnInit {

  // isShow: boolean;
  // topPosToStartShowing = 50;

  userRole = '';
  userData: any;
  authToken: any;
  pageData: any;
  cityid;
  userId: any;
  currentPage = 1;
  pageCount = 1;
  paginationArray = [];
  cityList: any;

  isLoading = false;
  interestedLoading = false;
  shortlistedLoading = false;
  loadingList = false;
  dataError = false;
  cityId;

  subModule;
  propertyId;
  type;

  constructor(private formBuilder: FormBuilder, private housewiseService: HousewiseService,
    private cd: ChangeDetectorRef, private router: Router, private acRt: ActivatedRoute,
    private dataService: DataService, private toaster: ToastrService) {
    this.userData = JSON.parse(constants.getUserData());
    this.authToken = this.userData.Authorization;
    this.cityid = localStorage.getItem('cityId');
    // console.log(this.cityId);

  }


  ngOnInit() {

    this.dataService.lastPageSourceChange.subscribe((pageNumber) => {
      this.currentPage = pageNumber;
    });

    this.userData = JSON.parse(constants.getUserData());
    this.userRole = this.userData.user_type;
    this.userId = this.userData.user_id;
    // this.acRt.params.subscribe(params => { this.cityid = params['cityId']; });

    this.getCities();


    if (this.cityid) {
      this.getProperties();
    } else {
      this.cityid = 'Select'
    }
  }

  /** Function Search property with city */
  searchProperty() {
    //  console.log(this.cityid);

    if (this.cityid === 'Select') {
      this.toaster.error("Please select valid city", "Error:");
      return;
    }
    this.currentPage = 1;
    this.getProperties();
  }

  onSelect($event) {
    this.cityid = $event.target.value;
    //  console.log(this.cityid);
  }

  /** FUnction to get City List */
  getCities() {
    this.loadingList = true;
    const params = new HttpParams();
    this.housewiseService.getCityList(params, this.authToken).subscribe((result) => {
      this.loadingList = false;
      for (const key in result) {
        if (key === "Success") {
          this.cityList = result.Success;
          if (!this.cityid) {
            //this.cityid = this.cityList[0].city_id;
            this.getProperties();
          }
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(3);
          } else {
            this.dataError = true;
          }
          return;
        }
      }
    }, (err) => {
      this.loadingList = false;
      console.log(err);
    });
  }


  /** Function to get property list */
  getProperties() {
    this.loadingList = true;
    this.pageData = undefined;
    this.dataError = false;
    const params = new HttpParams().set('city_id', this.cityid + '').set('page', this.currentPage + '').set('user_id', this.userData.user_id + '');
    this.housewiseService.getPropertyList(params, this.authToken).subscribe((result) => {
      this.loadingList = false;
      //  console.log("response " + result);
      for (const key in result) {
        if (key === "Success") {
          this.pageData = result.Success.data;
          //  console.log("pageData: " + JSON.stringify(this.pageData));
          this.pageCount = Number(result.Success.pageCount);
          //  console.log("pageCount: " + JSON.stringify(this.pageCount));
          localStorage.setItem("cityId", this.cityid);
          this.createPaginationArray();
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(2);
          } else if (this.currentPage < 2) {
            this.dataError = true;
          }
          return;
        }
      }
    }, (err) => {
      this.loadingList = false;
      console.log(err);
      this.dataError = true;
    });

  }

  shorlistInterestedProperty(subModule, propertyId, type) {
    this.subModule = subModule;
    this.propertyId = propertyId;
    this.type = type;

    const data = {
      'user_id': this.userData.user_id,
      'property_id': propertyId,
      'type': type
    };

    if(type ==='interested'){
      this.interestedLoading = true;
    } else {
      this.shortlistedLoading = true;
    }
    this.isLoading = true;
    this.housewiseService.shorlistInterestedProperty(JSON.stringify(data), this.authToken).subscribe((result) => {
      this.isLoading = false;
      this.interestedLoading = false;
      this.shortlistedLoading = false;
      for (const key in result) {
        if (key === 'Success') {
          if (type === 'interested') {
            subModule.iactive = !subModule.iactive;
            subModule.sactive = false;
            if (subModule.iactive == true) {
              this.toaster.success("Property added to interested", "Success:");
            } else {
              this.toaster.success("Property removed from interested", "Success:");
            }
          } else {
            subModule.sactive = !subModule.sactive;
            subModule.iactive = false;
            if (subModule.sactive == true) {
              this.toaster.success("Property added to shortlisted", "Success:");
            } else {
              this.toaster.success("Property removed from shortlisted", "Success:");
            }
          }
        } else if (key === 'Error') {
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(4);
          } else {
            this.toaster.error(result.Error, 'Error:');
          }
        }
      }
    }, (err) => {
      this.isLoading = false;
      this.interestedLoading = false;
      this.shortlistedLoading = false;
      console.log(err);
    });
  }

  //Change Page Number
  changePage(pageNumber) {
    this.currentPage = pageNumber;
    this.getProperties();
  }

  //Go to previous page
  previousPage() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getProperties();
    }
  }

  //Go to next page
  nextPage() {
    if (this.currentPage < this.pageCount) {
      this.currentPage++;
      this.getProperties();
    }
  }


  /** Create Paginator */
  createPaginationArray() {
    this.paginationArray = [];
    let firstIndex = Math.floor(this.currentPage / 10) + 1;
    if (firstIndex > 1) {
      firstIndex = firstIndex * 10 - 10;
    }
    for (let index = firstIndex, k = 0; index <= this.pageCount && k < 10; index++ , k++) {
      this.paginationArray.push(index);
    }
    //  console.log('paginationArray' + this.paginationArray);
  }



  /*** Funtion to refresh token after Expiration */
  refreshAuth(type) {
    const userId = { 'id': this.userData.user_id };
    this.housewiseService.updateAuthToken(JSON.stringify(userId)).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          constants.setUserData(JSON.stringify(result.Success));
          this.userData = result.Success;
          this.authToken = this.userData.Authorization;
          if (type === 2) {
            this.getProperties();
          } else if (type === 3) {
            this.getCities();
          } else if (type === 4) {
            this.shorlistInterestedProperty(this.subModule, this.propertyId, this.type);
          } else if (type === 5) {
            this.shortListProperty(this.subModule, this.propertyId);
          } else if (type === 6) {
            this.interestedProperty(this.subModule, this.propertyId);
          }
        } else if (key === 'error') {
          //error logic here
        }
      }
    }, (err) => {
      console.log(err);
    });
  }



  /** FUnction to Short List Property */
  shortListProperty(subModule, propertyId) {
    this.subModule = subModule;
    this.propertyId = propertyId;
    subModule.sactive = !subModule.sactive;

    //  console.log(subModule);

    const data = { 'user_id': this.userData.user_id, 'property_id': propertyId };
    this.housewiseService.shortListProperty(JSON.stringify(data), this.authToken).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          //success
          this.toaster.success('Added to Shortlisted Properties', 'Success:');

        } else if (key === 'Error') {
          //error logic here
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(5);
          }
        }
      }
    }, (err) => {
      console.log(err);
    });
  }




  viewDetails(propertyId) {
    //  console.log(propertyId);
    this.dataService.lastPageNumber(this.currentPage);
    this.router.navigate(['/user/detail-page', propertyId]);
  }

  /** FUnction to Interested Property */
  interestedProperty(subModule, propertyId) {
    this.subModule = subModule;
    this.propertyId = propertyId
    subModule.iactive = !subModule.iactive;

    const data = { 'user_id': this.userData.user_id, 'property_id': propertyId };
    this.housewiseService.interestedProperty(JSON.stringify(data), this.authToken).subscribe((result) => {
      for (const key in result) {
        if (key === 'Success') {
          //success
          this.toaster.success('Added to Interested Properties', 'Success:');

        } else if (key === 'Error') {
          //error logic here
          if (result.Error === 'Authorization token not valid') {
            this.refreshAuth(6);
          }
        }
      }
    }, (err) => {
      console.log(err);
    });
  }

  getPropertyImage(item) {
    if (item.images.length > 0) {
      return item.images[0].image;
    } else {
      return './assets/images/no_photo_available.png';
    }
  }

  getAddress(item) {
    let addr = '';
    if (item.addr1) {
      addr += item.addr1;
    }

    if (item.addr2 && item.addr2 != '') {
      addr += ", " + item.addr2;
    }

    if (item.area && item.area != '') {
      addr += ", " + item.area;
    }

    if (item.cityName && item.cityName != '') {
      addr += ", " + item.cityName;
    }

    if (item.countryName && item.countryName != '') {
      addr += ", " + item.countryName;
    }
    return addr;
  }


  // TODO: Cross browsing
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  // @HostListener('window:scroll')
  // checkScroll() {

  //   // window의 scroll top
  //   // Both window.pageYOffset and document.documentElement.scrollTop returns the same result in all the cases. window.pageYOffset is not supported below IE 9.

  //   const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

  //   //console.log('[scroll]', scrollPosition);

  //   if (scrollPosition >= this.topPosToStartShowing) {
  //     this.isShow = true;
  //   } else {
  //     this.isShow = false;
  //   }
  // }


}
