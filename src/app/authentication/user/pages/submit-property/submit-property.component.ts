import { Details } from './../PropertyModel';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Component, OnInit, ChangeDetectorRef, AfterContentChecked, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { HousewiseService } from 'src/app/service/housewise.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import * as _moment from 'moment';
import { HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { OwlDateTime } from 'ng-pick-datetime/date-time/date-time.class';
import { DateTimeAdapter, OWL_DATE_TIME_LOCALE, OWL_DATE_TIME_FORMATS, OwlDateTimeIntl } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { element } from '@angular/core/src/render3';
import { Observable } from 'rxjs';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { startWith, map } from 'rxjs/operators';
import { constants } from 'src/app/constants';

// here is the default text string
export class DefaultIntl extends OwlDateTimeIntl {

	/** A label for the cancel button */
	cancelBtnLabel = 'Cancel';

	/** A label for the set button */
	setBtnLabel = 'Select';

};



const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_CUSTOM_FORMATS = {
	parseInput: 'LL LT',
	fullPickerInput: 'LL LT',
	datePickerInput: 'MM/DD/YYYY',
	timePickerInput: 'LT',
	monthYearLabel: 'MMMM YYYY',
	dateA11yLabel: 'LL',
	monthYearA11yLabel: 'MMMM YYYY'
};

@Component({
	selector: 'app-submit-property',
	templateUrl: './submit-property.component.html',
	styleUrls: ['./submit-property.component.css'],
	providers: [
		// `MomentDateTimeAdapter` can be automatically provided by importing
		// `OwlMomentDateTimeModule` in your applications root module. We provide it at the component level
		// here, due to limitations of our example generation script.
		{ provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },

		{ provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
	]
})
export class SubmitPropertyComponent implements OnInit, AfterContentChecked, OnDestroy {

	propertyForm: FormGroup;
	selectedFile;
	imageUrls = [];
	imageGrid = [];
	imageDataFiles = [];
	docsDataFiles: [];
	files = [];
	comboList;
	amenitiesMatrix = [];
	rentToArray = [];
	docsArray;
	selectedDocIndex = 0;
	selectedDocTypeIndex = 0;
	documentFile;
	documentsArray;
	documentsArrayMatrix = [];

	invalid = false;
	saving: boolean;
	isSubmitted = false;
	param;
	updatePropertyStatusData;
	authToken;
	userData: any;
	tempForm: any;

	dataError: boolean;


	public imagePath;
	imgURL: any;
	public message: string;
	submitForm;
	propertyData;
	isEdit = false;
	isAddProperty = false;
	isImageAdded = false;
	propertyAddedComplete = false;

	uploadImageFormData: FormData = new FormData();
	uploadDocsFormData: FormData = new FormData();
	docsGrid: any[];
	docsUrls: any;
	docArray: any;

	isLoading = false;
	isImageUploading = false
	currentDate: Date = new Date();
	emptyDate = false;
	countryId;
	availableFromDate;

	sizeErrorString: string = 'size must not be greater than 5MB';

	selectedTypeOfServices = [];

	dropdownList = [];

	dropdownSettings = {};
	delPropImgItem: any;
	delPropDocItem: any;
	delPropDocItemRow: any;
	delPropDocItemColumn: any;




	constructor(private formBuilder: FormBuilder, private housewiseService: HousewiseService,
		private cd: ChangeDetectorRef, private router: Router, private domSanitizer: DomSanitizer,
		private toastr: ToastrService,
		private activatedRoute: ActivatedRoute) {
		this.userData = JSON.parse(constants.getUserData());
		this.authToken = this.userData.Authorization;
		//console.log('authorisationToken' + this.authToken);
	}

	ngOnInit() {

		this.activatedRoute.params.subscribe(params => {
			this.isAddProperty = false;
		});

		if (this.isAddProperty === false) {
			this.checkEditProperty();
		} else {
			this.isAddProperty = true
			localStorage.removeItem('editPropertyData');
		}


		this.getComboList();
		this.getDocumentTypes();
		this.createForm();
		this.currentDate.setDate((new Date()).getDate() - 1);
		this.setDropDown();

	}


	setDropDown() {
		this.dropdownSettings = {
			singleSelection: true,
			idField: 'services_master_id',
			textField: 'services_name',
			selectAllText: 'Select All',
			unSelectAllText: 'UnSelect All',
			itemsShowLimit: 1,
			allowSearchFilter: false,
			enableCheckAll: false,
			maxHeight: '49px'

		};
	}

	ngAfterContentChecked(): void {
		//	throw new Error("Method not implemented.");
		this.cd.detectChanges();

		//this.createAmenitiesMatrix();
	}

	getComboList() {
		this.housewiseService.getComboList(this.authToken).subscribe((result) => {
			//console.log(result);
			for (const key in result) {
				if (key === 'Success') {
					this.comboList = result.Success;
					this.amenitiesMatrix = [];
					this.createAmenitiesMatrix();
					this.createRentToArray();

					if (this.isEdit === true) {
						this.selectedTypeOfServices = [];
						this.propertyData.typeOfAServices.forEach(element => {
							this.comboList.services.forEach(serviceItem => {
								if (element.services_master_id === serviceItem.services_master_id) {
									this.selectedTypeOfServices.push(serviceItem);
								}
							});
						});
					}
					//	console.log(this.selectedTypeOfServices);
				} else if (key === 'Error') {
					//	console.log("error in ");
					if (result.Error === 'Authorization token not valid') {
						this.refreshAuth('2');
					}
					return;
				}
			}
		});
	}

	getDocumentTypes() {
		const param = new HttpParams().set('user_type', this.userData.user_type + '');
		this.housewiseService.getDocumentType(param, this.authToken).subscribe((result) => {
			//console.log(result);
			for (const key in result) {
				if (key === 'Success') {
					this.docsArray = result.Success;
				} else if (key === 'Error') {
					//	console.log("error in ");
					if (result.Error === 'Authorization token not valid') {
						this.refreshAuth(6);
					}
					return;
				}
			}
		});
	}

	getDocumentImages() {
		const params = new HttpParams().set('property_id', this.propertyData.id + '');
		//	const params = '';
		this.housewiseService.getDocumentImages(params, this.authToken).subscribe((result) => {
			//console.log("response " + result);
			for (const key in result) {
				if (key === "Success") {
					//	console.log("success " + result.Success);
					this.documentsArray = result.Success;
					this.createDocumentImagesMatrix();
				} else if (key === 'Error') {
					if (result.Error === 'Authorization token not valid') {
						this.refreshAuth(7);
					} else {
						this.documentsArrayMatrix = [];
					}
					return;
				}
			}
		}, (err) => {
			console.log(err);
		});
	}

	createAmenitiesMatrix() {
		let k = 0;
		//console.log(this.comboList.amenities);
		for (let i = 0; i < this.comboList.amenities.length; i++) {
			this.comboList.amenities[i]['checked'] = false;
			if (i % 4 === 0) {
				k++;
				this.amenitiesMatrix[k] = [];
			}
			if (this.propertyData && this.propertyData != null) {
				this.propertyData.preference.amenities.forEach(element => {
					if (element === this.comboList.amenities[i].ami_id) {
						this.comboList.amenities[i]['checked'] = true;
					}
				});
			}
			this.amenitiesMatrix[k].push(this.comboList.amenities[i]);
		}
	}

	createRentToArray() {
		this.rentToArray = [
			{ id: 1, name: 'Anyone', checked: false },
			{ id: 2, name: 'Family', checked: false },
			{ id: 3, name: 'Single Men', checked: false },
			{ id: 4, name: 'Single Women', checked: false },
		];

		for (let i = 0; i < this.rentToArray.length; i++) {
			if (this.propertyData && this.propertyData != null) {
				this.propertyData.preference.rentTO.forEach(element => {
					if (element === this.rentToArray[i].name) {
						this.rentToArray[i]['checked'] = true;
					}
				});
			}
		}

	}


	createDocumentImagesMatrix() {
		this.documentsArrayMatrix = [];
		let k = 0;
		for (let i = 0; i < this.documentsArray.length; i++) {
			if (i % 4 === 0) {
				k++;
				this.documentsArrayMatrix[k] = [];
			}
			this.documentsArrayMatrix[k].push(this.documentsArray[i]);
		}
		//	console.log(this.documentsArrayMatrix);
	}



	getDocumentFileImage(item) {

		if (item.document_file.indexOf(".pdf") > -1) {
			return './assets/images/pdf_placeholder.png';
		} else if (item.document_file.indexOf(".docx") > -1) {
			return './assets/images/docx_placeholder.png';
		} else {
			return item.document_file;
		}

	}

	getDocumentFileImageUrl(item) {
		return item.document_file;
	}

	checkEditProperty() {
		this.selectedTypeOfServices = [];
		this.propertyData = JSON.parse(localStorage.getItem('editPropertyData'));
		if (this.propertyData && this.propertyData !== '') {
			//	this.propertyData.details.availFrom = moment(this.propertyData.details.availFrom, "YYYY-MM-DD").format("DD/MM/YYYY");
			this.createImageMatrix();
			this.getDocumentImages();
			this.isEdit = true;
			this.countryId = this.propertyData.details.country;
			this.selectedTypeOfServices = this.propertyData.typeOfAServices;
			if (this.propertyData.details && this.propertyData.details.availFrom) {
				this.availableFromDate = this.propertyData.details.availFrom
			}


		} else {
			this.isAddProperty = true;
		}
	}


	createForm() {
		this.propertyForm = this.formBuilder.group({
			propertyType: [null, Validators.required],
			city: [null, Validators.required],
			typeOfAServices: [null, Validators.required],
			details: this.formBuilder.group({
				furnishing: [null, Validators.required],
				propertyAge: [null, Validators.required],
				addr1: [null, Validators.required],
				addr2: [null],
				area: [null, Validators.required],
				city: [null, Validators.required],
				pincode: [null, [Validators.required, Validators.pattern('[0-9]{6}')]],
				country: [null, Validators.required]
			}),
			features: this.formBuilder.group({
				occupancy: [null],
				bedrooms: [null, Validators.required],
				bathrooms: [null, Validators.required],
				balconies: [null, Validators.required],
				builtup: [null, [Validators.required, Validators.pattern(/^[0-9]\d{1,5}(\.\d{1,2})?%?$/)]],
				units: [null, Validators.required],
				floorlvl: [null, Validators.required],
				totalFloor: [null, Validators.required]
			}),
			preference: this.formBuilder.group({
				rentTO: [null],
				exptRent: [null, [Validators.required, Validators.pattern(/^[0-9]\d{1,6}$/)]],
				security: [null, [Validators.required, Validators.pattern(/^[0-9]\d{1,6}$/)]],
				rentType: [null],
				amenities: [null]

			})
		});
	}


	amenitiesCheckeBoxChanged(row, column, event) {
		if (event.target.checked) {
			this.amenitiesMatrix[row][column].checked = true;
		} else {
			this.amenitiesMatrix[row][column].checked = false;
		}
	}

	rentToChecked(index, event) {
		if (event.target.checked) {
			this.rentToArray[index].checked = true;
		} else {
			this.rentToArray[index].checked = false;
		}

	}

	docsChanged(event) {
		this.selectedDocIndex = event.target.selectedIndex;
		this.selectedDocTypeIndex = 0;
	}

	docsTypeChanged(event) {
		this.selectedDocTypeIndex = event.target.selectedIndex;
	}

	getSelectedAmeneties() {
		let amenities = [];
		this.amenitiesMatrix.forEach(row => {
			row.forEach(element => {
				if (element.checked === true) {
					amenities.push(element.ami_id);
				}
			});
		});
		return amenities;
	}

	getSelectedRentTo() {
		let rentTo = [];
		this.rentToArray.forEach(element => {
			if (element.checked === true) {
				rentTo.push(element.name);
			}
		});
		return rentTo;
	}

	get formControl(): any {
		return this.propertyForm.controls;
	}




	onSubmit(form: any) {

		//	console.log(form);
		this.isSubmitted = true;



		if (form.invalid) {
			this.invalid = true;
			this.toastr.error('Fill all the data', 'Error:');
			return;
		}

		this.emptyDate = false;
		if (form.value.details.availFrom === '') {
			this.emptyDate = true;
			return;
		}

		if (parseInt(form.value.features.floorlvl) > parseInt(form.value.features.totalFloor)) {
			this.invalid = true;
			this.toastr.error('Floor level can not be greater than total floor', 'Error:');
			return;
		}

		//console.log(this.rentToArray);
		if (this.checkRentToArray() === false) {
			this.toastr.error("At least 1 whom to rent option should be selected", "Error:");
			return;
		}

		if (!this.availableFromDate) {
			this.toastr.error('Select available from date', 'Error:');
			return true;
		}

		//console.log("selected type of services 2: ", this.selectedTypeOfServices);
		// if (this.selectedTypeOfServices.length === 0) {
		// 	this.invalid = true;
		// 	this.toastr.error("Select type of service", "Error:");
		// 	return;
		// }



		this.invalid = false;

		//let dateString = moment(form.value.details.availFrom, "DD/MM/YYYY").format("YYYY-MM-DD");
		let amenities = this.getSelectedAmeneties();
		let rentTo = this.getSelectedRentTo();

		var availDate = moment(this.availableFromDate).format('YYYY-MM-DD');

		// create array of selected service array and send in typeOfAService
		//	console.log("form services selected: ", form.value.typeOfAServices);

		this.selectedTypeOfServices = [];
		this.comboList.services.forEach(serviceItem => {
			if (form.value.typeOfAServices === serviceItem.services_master_id) {
				this.selectedTypeOfServices.push(serviceItem);
			}
		});



		this.submitForm = {
			propertyType: form.value.propertyType,
			city: form.value.city,
			typeOfAServices: this.selectedTypeOfServices,
			details: {
				furnishing: form.value.details.furnishing,
				availFrom: availDate,
				propertyAge: form.value.details.propertyAge,
				addr1: form.value.details.addr1,
				addr2: form.value.details.addr2,
				area: form.value.details.area,
				city: form.value.details.city,
				pincode: form.value.details.pincode,
				country: form.value.details.country
			},
			features: {
				occupancy: form.value.features.occupancy,
				bedrooms: form.value.features.bedrooms,
				bathrooms: form.value.features.bathrooms,
				balconies: form.value.features.balconies,
				builtup: form.value.features.builtup,
				units: form.value.features.units,
				floorlvl: form.value.features.floorlvl,
				totalFloor: form.value.features.totalFloor
			},
			preference: {
				rentTO: rentTo,
				exptRent: form.value.preference.exptRent,
				security: form.value.preference.security,
				rentType: form.value.preference.rentType,
				amenities: amenities

			}
		}

		if (this.isEdit === true) {
			this.submitForm['id'] = this.propertyData.id;
		}
		this.submitForm['userId'] = this.userData.user_id;


		this.tempForm = this.submitForm;
		//	console.log("submit form: ", this.tempForm);

		this.isLoading = true;
		this.housewiseService.addProperty(JSON.stringify(this.submitForm), this.authToken).subscribe((result) => {
			this.isLoading = false;
			//console.log(result);
			for (const key in result) {
				if (key === 'Success') {
					//	console.log("on success: ", result.Success);
					this.propertyData = result.Success;
					//	console.log("property Data: ", this.propertyData)
					if (this.isEdit) {
						this.toastr.success('Property updated', 'Success:');
					} else {
						this.toastr.success('Property added', 'Success:');
					}
					this.isEdit = true;
					localStorage.setItem('editPropertyData', JSON.stringify(this.propertyData));
					document.getElementById('property_images_tab').click();
				} else if (key === 'Error') {
					//	console.log("error in ", result.Error);
					if (result.Error === 'Authorization token not valid') {
						this.refreshAuth('1');
					}
					return;
				}
			}
		}, (err) => {
			this.isLoading = false;
			this.toastr.error("Failed to save property", "Error:");
			console.log(err);
		});

	}

	checkRentToArray() {
		let flag = false;
		let test = this.rentToArray.forEach(element => {
			if (element.checked === true) {
				flag = true;

			}
		});
		return flag;
	}



	refreshAuth(type) {
		const userId = { 'id': this.userData.user_id };
		//	console.log('Id:' + JSON.stringify(userId));
		this.housewiseService.updateAuthToken(JSON.stringify(userId)).subscribe((result) => {
			for (const key in result) {
				if (key === 'Success') {
					constants.setUserData(JSON.stringify(result.Success));
					this.userData = result.Success;
					this.authToken = result.Success.Authorization;
					//	console.log('Success' + result.Success);
					if (type === '1') {
						this.onSubmit(this.tempForm);
					} else if (type === '2') {
						this.getComboList();
					} else if (type === '3') {
						this.uploadImages();
					} else if (type === '4') {
						this.uploadDocs();
					} else if (type === '5') {
						this.deletePropertyImage(this.delPropImgItem);
					} else if (type === 6) {
						this.getDocumentTypes();
					} else if (type === 7) {
						this.getDocumentImages();
					} else if (type === 8) {
						this.updatePropertyCompleteStatus();
					} else if (type === '9') {
						this.deletePropertyDocument(this.delPropDocItem, this.delPropDocItemRow, this.delPropDocItemColumn);
					}

				} else if (key === 'error') {

				}
			}
			//	console.log(JSON.stringify(result));
		}, (err) => {
			console.log(err);
		});
	}

	getImageUrl(url) {
		if (url && url.substring(0, 5) === 'data:') {
			return url;
		} else {
			return url;
		}

	}

	onFileChange(event) {
		let text = " file selected";
		this.documentFile = '';
		if (event.target.files && event.target.files.length) {
			this.documentFile = event.target.files[0];
			//	console.log(event.target.files[0]);
			if ((this.documentFile.size / 1000) > 5000) {
				this.toastr.error('File size to large', 'Error:');
				this.documentFile = undefined;
				(<HTMLInputElement>document.getElementById('upload_documents')).value = "";
			} else {
				this.uploadDocs();
			}
		}
	}

	onSelect($event) {
		if ($event.target.value !== '') {
			this.countryId = 1;
		}
	}

	createDocsMatrix() {
		this.docsGrid = [];
		let k = 0;
		for (let i = 0; i < this.docsGrid.length; i++) {
			if (i % 3 === 0) {
				k++;
				this.docsGrid[k] = [];
			}
			this.docsGrid[k].push(this.docsUrls[i]);
		}
	}



	onImageChange(event) {
		this.imageDataFiles = [];
		if (event.target.files && event.target.files.length) {
			this.imageDataFiles = [];
			for (let index = 0; index < event.target.files.length; index++) {
				const element = event.target.files[index];
				//	console.log('type:', element.type);
				let isImage = this.checkFileType(element);
				if (isImage === false) {
					this.toastr.error('Please select valid image ', 'Error:');
					(<HTMLInputElement>document.getElementById('image_upload')).value = "";
				} else if ((element.size / 1000) > 5000) {
					this.toastr.error(element.name + ' File size too large ', 'Error:');
					(<HTMLInputElement>document.getElementById('image_upload')).value = "";
				} else {
					this.imageDataFiles.push(element);
				}
			}
			if (this.imageDataFiles.length > 0) {
				this.uploadImages();
			}
		}

	}

	checkFileType(element) {
		let flag = false;
		if ((element.type === 'image/png' || element.type === 'image/jpeg' || element.type === 'image/jpg')) {
			flag = true;
		}
		return flag;
	}


	createImageMatrix() {
		this.imageGrid = []
		let k = 0;
		for (let i = 0; i < this.propertyData.images.length; i++) {
			if (i % 4 === 0) {
				k++;
				this.imageGrid[k] = [];
			}
			this.imageGrid[k].push(this.propertyData.images[i]);
		}
	}


	uploadDocs() {

		if (this.documentFile) {
			this.uploadDocsFormData = new FormData();
			this.uploadDocsFormData.append('file', this.documentFile);
			this.uploadDocsFormData.append('property_id', this.propertyData.id);
			this.uploadDocsFormData.append('user_id', this.userData.user_id);
			this.uploadDocsFormData.append('document_proof_id', this.docsArray[this.selectedDocIndex].document_proof_id);
			//this.uploadDocsFormData.append('document_proof_type_id', ((((this.docsArray || {})[this.selectedDocIndex].docType || [])[this.selectedDocTypeIndex] || {}).document_proof_type_id));
			if (this.docsArray[this.selectedDocIndex].docType[this.selectedDocTypeIndex] && this.docsArray[this.selectedDocIndex].docType[this.selectedDocTypeIndex].document_proof_type_id) {
				this.uploadDocsFormData.append('document_proof_type_id', this.docsArray[this.selectedDocIndex].docType[this.selectedDocTypeIndex].document_proof_type_id);
			}
			this.isLoading = true;
			this.housewiseService.uploadPropertyDocs(this.uploadDocsFormData, this.authToken).subscribe((result) => {
				this.isLoading = false;
				//	console.log(result);
				for (const key in result) {
					if (key === 'Success') {
						this.documentFile = undefined;
						(<HTMLInputElement>document.getElementById('upload_documents')).value = "";
						this.documentsArray = result.Success;
						this.createDocumentImagesMatrix();
						this.toastr.success('Property documents added', 'Success:');
						//	this.router.navigate(['/listing-page']);
					} else if (key === 'Error') {
						this.documentFile = undefined;
						(<HTMLInputElement>document.getElementById('upload_documents')).value = "";
						//	console.log("error in ");
						if (result.Error === 'Authorization token not valid') {
							this.refreshAuth('4');
						} else if (result.Error.includes(this.sizeErrorString)) {
							this.toastr.error(result.Error, "Error:");
						}
						return;
					}
				}
				//	console.log(JSON.stringify(result));
			}, (err) => {
				this.isLoading = false;
				console.log(err);
			});
		} else {
			this.toastr.error('Select file', 'Error:');
		}
	}


	uploadImages() {
		if (this.imageDataFiles && this.imageDataFiles.length > 0) {
			this.uploadImageFormData = new FormData();
			for (var x = 0; x < this.imageDataFiles.length; x++) {
				this.uploadImageFormData.append('property_image[]', this.imageDataFiles[x]);
			}
			this.uploadImageFormData.append('property_id', this.propertyData.id);
			this.uploadImageFormData.append('user_id', this.userData.user_id);
			this.isImageUploading = true;
			this.housewiseService.uploadPropertyImages(this.uploadImageFormData, this.authToken).subscribe((result) => {
				this.isImageUploading = false;
				//	console.log(result);
				for (const key in result) {
					if (key === 'Success') {
						this.imageDataFiles = undefined;
						(<HTMLInputElement>document.getElementById('image_upload')).value = "";
						this.isImageAdded = true;
						this.propertyData.images = result.Success;
						this.createImageMatrix();
						this.toastr.success('Property images added', 'Success:');

						//document.getElementById('property_docs_tab').click();
					} else if (key === 'Error') {
						//	console.log("error in ");
						(<HTMLInputElement>document.getElementById('image_upload')).value = "";
						if (result.Error.includes(this.sizeErrorString)) {
							//this.errorMessages.push(element);/*to show on html page */
							this.toastr.error(result.Error, "Error:", {
								timeOut: 6000
							});
						}
						if (result.Error === 'Authorization token not valid') {
							this.refreshAuth('3');
						}
						// else if (result.Error.includes(this.sizeErrorString)) {
						// 	this.toastr.error(result.Error, "Error:");
						// }
						return;
					}
				}
				//	console.log(JSON.stringify(result));
			}, (err) => {
				this.isImageUploading = false;
				console.log(err);
			});
		} else {
			this.toastr.error("Select file ", "Error:")
			console.log('select file error');
		}



	}


	deletePropertyImage(item) {
		this.delPropImgItem = item; 
		const params = new HttpParams().set('property_id', item.property_id + '').set('image_id', item.image_id).set('user_id', this.userData.user_id);
		this.housewiseService.deletePropertyImage(params, this.authToken).subscribe((result) => {
			//	console.log("response: ", result);
			for (const key in result) {
				if (key === "Success") {
					this.toastr.success('Property image deleted', 'Success:');
					//	console.log("success: ", result.Success);
					this.propertyData.images = result.Success;
					localStorage.setItem('editPropertyData', JSON.stringify(this.propertyData));
					this.createImageMatrix();
				} else if (key === 'Error') {
					if (result.Error === 'Authorization token not valid') {
						this.refreshAuth('5');
					} else if (result.Error === 'No records available') {
						this.propertyData.images = '';
						localStorage.setItem('editPropertyData', JSON.stringify(this.propertyData));
						this.createImageMatrix();
					}
					return;
				}
			}
		}, (err) => {
			console.log(err);
			// this.toastrService.error('Failed to Delete Property', '', { timeOut: 5000 });
		});
	}


	deletePropertyDocument(item, row, column) {
		this.delPropDocItem = item;
		this.delPropDocItemRow = row;
		this.delPropDocItemColumn = column;
		const params = new HttpParams().set('document_id', item.document_id + '').set('property_id', this.propertyData.id).set('user_id', this.userData.user_id);
		this.housewiseService.deletePropertyDocument(params, this.authToken).subscribe((result) => {
			//	console.log("response: ", result);
			for (const key in result) {
				if (key === "Success") {
					//	console.log("success: ", result.Success);
					this.toastr.success('Property document deleted', 'Success:');
					//delete (this.documentsArrayMatrix[row][column]);
					//	console.log(this.documentsArrayMatrix);
					//this.documentsArrayMatrix[row].splice(column, 1);
					this.getDocumentImages();
					//	console.log(this.documentsArrayMatrix);

					//this.createDocsMatrix();
				} else if (key === 'Error') {
					if (result.Error === 'Authorization token not valid') {
						this.refreshAuth('9');
					} else {
						//  this.toastrService.error('Failed to Delete Property', '', { timeOut: 5000 });
					}
					return;
				}
			}
		}, (err) => {
			console.log(err);
			// this.toastrService.error('Failed to Delete Property', '', { timeOut: 5000 });
		});
	}

	gotoUploadDocs() {
		document.getElementById('property_docs_tab').click();
	}

	changeTab(evt, tabName) {

		if (this.isAddProperty === true && this.isImageAdded === false && tabName === 'property_documents') {
			this.toastr.error('Add property images first', 'Error:');
			return;
		}

		if (this.propertyData && this.isEdit === true) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("nav-item");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById(tabName).style.display = "block";
			evt.currentTarget.className += " active";
		} else if (evt.currentTarget.id === 'property_images_tab' || evt.currentTarget.id === 'property_docs_tab') {
			this.toastr.error('Add property details first', 'Error:');
		}
	}

	skipDocTab() {
		this.router.navigate(['/user/owner-dashboard']);
	}

	skipImageTab() {
		//this.gotoUploadDocs();

		if (this.propertyData) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("nav-item");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById('property_documents').style.display = "block";
			document.getElementById('property_docs_tab').className += " active";
			//evt.currentTarget.className += " active";
		} else {
			this.toastr.error('Add property details first', 'Error:');
		}
	}

	//cancel button clicked
	cancelAction() {
		if(this.isEdit) {
			this.router.navigate(['/user/detail-page/', this.propertyData.id]);
		} else {
			this.router.navigate(['/user/owner-dashboard']);
		}
	}


	// TODO: Cross browsing
	gotoTop() {
		window.scroll({
			top: 0,
			left: 0,
			behavior: 'smooth'
		});
	}

	ngOnDestroy(): void {
		localStorage.removeItem('editPropertyData');
	}


	updatePropertyCompleteStatus() {

		if (this.isEdit) {
			this.updatePropertyStatusData = {
				property_id: this.propertyData.id,
				status: this.propertyData.HW_status
			};
		} else {
			this.updatePropertyStatusData = {
				property_id: this.propertyData.id,
				status: 'Added'
			};
		}
		this.housewiseService.updatePropetyStatus(this.updatePropertyStatusData, this.authToken).subscribe((result) => {
			//	console.log("response: ", result);
			for (const key in result) {
				if (key === "Success") {
					if (this.propertyAddedComplete === false && this.isEdit === false) {
						this.toastr.success('Property added successfully', 'Success:');
					} else if (this.isEdit === true) {
						this.toastr.success('Property updated successfully', 'Success:');
					}

					this.propertyAddedComplete = true;
					this.router.navigate(['/user/owner-dashboard']);


				} else if (key === 'Error') {
					if (result.Error === 'Authorization token not valid') {
						this.refreshAuth(8);
					} else {
						//  this.toastrService.error('Failed to Delete Property', '', { timeOut: 5000 });
					}
					return;
				}
			}
		}, (err) => {
			console.log(err);
			// this.toastrService.error('Failed to Delete Property', '', { timeOut: 5000 });
		});
	}


}

