import { AuthGuard } from './../../core/guards/auth.guard';
import { TenantGuard } from './../../core/guards/tenant.guard';
import { OwnerGuard } from './../../core/guards/owner.guard';
import { ProfileComponent } from './pages/profile/profile.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component'
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ListingPageComponent } from './pages/listing-page/listing-page.component';
import { DetailsPageComponent } from './pages/details-page/details-page.component';
import { SubmitPropertyComponent } from './pages/submit-property/submit-property.component';
import { TenantDashboardComponent } from './pages/tenant-dashboard/tenant-dashboard.component';
import { ShortlistPropertyComponent } from './pages/shortlist-property/shortlist-property.component';
import { InterestedPropertyComponent } from './pages/interested-property/interested-property.component';
import { TenantPropertyListComponent } from './pages/tenant-property-list/tenant-property-list.component';
import { UploadDocsComponent } from './pages/upload-docs/upload-docs.component';
import { from } from 'rxjs';
import { UploadPropertyDocumentsComponent } from './pages/upload-docs/upload-property-documents/upload-property-documents.component';

const routes: Routes = [
  {
    path: 'change-password',
    component: ChangePasswordComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'owner-dashboard',
    component: DashboardComponent,
    canActivate: [OwnerGuard]
  },
  {
    path: 'listing-page',
    component: ListingPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'listing-page/:id',
    component: ListingPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'detail-page/:propertyId',
    component: DetailsPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'submit-property',
    component: SubmitPropertyComponent,
    canActivate: [OwnerGuard]
  },
  {
    path: 'submit-property/:propertyId',
    component: SubmitPropertyComponent,
    canActivate: [OwnerGuard]
  },
  {
    path: 'tenant-dashboard',
    component: TenantDashboardComponent,
    canActivate: [TenantGuard]
  },
  {
    path: 'shortlisted-property',
    component: ShortlistPropertyComponent,
    canActivate: [TenantGuard]
  },
  {
    path: 'upload-property-documents/:propertyId',
    component: UploadPropertyDocumentsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'upload-docs/:propertyId/:type',
    component: UploadDocsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'upload-docs/:propertyId/:tenantId/:type',
    component: UploadDocsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: 'owner-dashboard',
    pathMatch: 'full',
    canActivate: [OwnerGuard]
  },
  {
    path: '',
    redirectTo: 'tenant-dashboard',
    pathMatch: 'full',
    canActivate: [TenantGuard]
  },
  {
    path: 'shortlist-property',
    component: ShortlistPropertyComponent,
    canActivate: [TenantGuard]
  },
  {
    path: 'interested-property',
    component: InterestedPropertyComponent,
    canActivate: [TenantGuard]
  }
  ,
  {
    path: 'tenant-property-list',
    component: TenantPropertyListComponent,
    canActivate: [TenantGuard]
  }
  ,
  {
    path: 'tenant-property-list/:cityId',
    component: TenantPropertyListComponent,
    canActivate: [TenantGuard]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
