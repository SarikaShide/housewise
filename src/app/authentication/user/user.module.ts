import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { DetailsPageComponent } from './pages/details-page/details-page.component';
import { ListingPageComponent } from './pages/listing-page/listing-page.component';
import { SubmitPropertyComponent } from './pages/submit-property/submit-property.component';
import { TenantDashboardComponent } from './pages/tenant-dashboard/tenant-dashboard.component';
import { ShortlistPropertyComponent } from './pages/shortlist-property/shortlist-property.component';
import { UploadDocsComponent } from './pages/upload-docs/upload-docs.component';
import { UploadPropertyDocumentsComponent } from './pages/upload-docs/upload-property-documents/upload-property-documents.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { SlickModule } from 'ngx-slick';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule } from 'ngx-toastr';
import { InterestedPropertyComponent } from './pages/interested-property/interested-property.component';
import { TenantPropertyListComponent } from './pages/tenant-property-list/tenant-property-list.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS, OwlDateTimeIntl } from 'ng-pick-datetime';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown-angular7';
import { from } from 'rxjs';




@NgModule({
  declarations: [
    UserComponent,
    DashboardComponent,
    ListingPageComponent,
    DetailsPageComponent,
    SubmitPropertyComponent,
    TenantDashboardComponent,
    ShortlistPropertyComponent,
    UploadDocsComponent,
    UploadPropertyDocumentsComponent,
    ProfileComponent,
    ChangePasswordComponent,
    InterestedPropertyComponent,
    TenantPropertyListComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    SlickModule.forRoot(),
    OwlDateTimeModule,
    OwlNativeDateTimeModule,

  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class UserModule { }


