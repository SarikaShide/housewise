import { constants } from './../../constants';

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HousewiseService } from '../../service/housewise.service'
import { Router } from '@angular/router';
import { HeaderService } from 'src/app/shared/services/header.service';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/service/data.service';
import { Title } from '@angular/platform-browser';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  invalid = false;
  saving: boolean;
  isSubmitted = false;
  param;
  isLoading = false;
  loginData;

  isRecaptchaValid = false;

  captcha;

  constructor(private header: HeaderService,
    private formBuilder: FormBuilder, private housewiseService: HousewiseService, private router: Router,
    private toaster: ToastrService,
    private data: DataService,
    private titleSerive: Title) {
    this.data.authTabChange(2);
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group(
      {
        username: ['', [Validators.required, Validators.pattern(/^[_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/)]],
        // password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]]
        password: ['', [Validators.required]],
        remember_me: [''],
        captcha:[''],
      }
    );

    this.loginData = JSON.parse(localStorage.getItem('rememberMe'));
  }

  // , Validators.minLength(6) /* for password*/

  get control(): any {
    return this.loginForm.controls;
  }

  onSubmit(form: any) {
    // console.log(form.value);
    // console.log(this.isRecaptchaValid);

    this.isSubmitted = true;

    if (form.invalid) {
      this.invalid = true;
      //  console.log(form.controls.password);
      this.toaster.error("Please enter valid details", "Error:");
      return;
    }

    if (form.controls.password.invalid) {
      this.toaster.error("Please enter valid password", "Error:");
      return;
    }
    //console.log("abcd");
    this.invalid = false;

    if (this.isRecaptchaValid === false) {
      this.toaster.error("Please verify captcha", "Error:");
      return;
    }

    form.value.username = form.value.username.toLowerCase();
    
    this.param = {
      user_email_id: form.value.username,
      password: form.value.password,
    };
    
    this.param = JSON.stringify(this.param);

    this.saving = true;

    this.isLoading = true;
    this.housewiseService.onLogin(this.param).subscribe((res: any) => {
      this.isLoading = false;
      //  console.log("result " + res);
      for (const key in res)
        if (key === "Success") {
          this.toaster.success('Welcome, ' + res.Success.first_name, 'Login Success:');
          constants.setUserData(JSON.stringify(res.Success));

          if (form.value.remember_me === true) {
            localStorage.setItem('rememberMe', JSON.stringify(form.value));
          } else {
            localStorage.removeItem('rememberMe');
          }

          if (res.Success.user_type.toUpperCase() == constants.tenant.toUpperCase()) {
            this.router.navigate(['/user/tenant-dashboard']);
          } else if (res.Success.user_type.toUpperCase() == constants.owner.toUpperCase()) {
            this.router.navigate(['/user/owner-dashboard']);
          }
          this.data.logInSuccess("Login Success");

        } else if (key === 'Error') {
          this.toaster.error(res.Error, 'Error:');
        }
    }, (e) => {
      this.isLoading = false;
      console.log(e);
    });

  }

  resolved(captchaResponse: string) {
    if (captchaResponse === null || captchaResponse === undefined || captchaResponse === '') {
      this.toaster.error("Captcha verification failed", "Error:")
      return;
    } else {
      this.isRecaptchaValid = true;
    }
   // console.log(`Resolved captcha with response: ${captchaResponse}`);
    this.captcha = captchaResponse;
  }

}
