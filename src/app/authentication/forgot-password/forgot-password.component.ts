import { HousewiseService } from 'src/app/service/housewise.service';
import { Component, OnInit, AfterViewChecked, AfterContentInit, AfterViewInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, Route } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { HeaderService } from 'src/app/shared/services/header.service';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/service/data.service';
import * as _moment from 'moment';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})



export class ForgotPasswordComponent implements OnInit {

  loginForm: FormGroup;
  invalid = false;
  userData;
  isSubmitted = false;
  param;
  isLoading = false;
  password_match_error = false;
  auth;
  era;
  invalidUrl = false;

  constructor(private header: HeaderService,
    private formBuilder: FormBuilder, private housewiseService: HousewiseService, private router: Router,
    private toastr: ToastrService, private route: ActivatedRoute,
    private data: DataService,
    private titleSerive: Title) {
    this.data.authTabChange(3);
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group(
      {
        password: ['', [Validators.required, Validators.minLength(6)]],
        // password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]]
        confirmPassword: ['', [Validators.required]],
      }
    );
    this.getRouteParams();
  }


  getRouteParams() {
    this.era = this.route.snapshot.queryParamMap.get('era');
    this.auth = this.route.snapshot.queryParamMap.get('auth');

    const parsed = _moment.unix(Number.parseInt(this.era));
    const currentDateTime = _moment(new Date());
    var duration = _moment.duration(currentDateTime.diff(parsed));
    var minutes = duration.asMinutes();

    if (minutes > 10.0) {
      this.invalidUrl = true
    } else {
      this.getUserData();
    }


  }

  checkPasswords() {
    const pass = this.loginForm.controls.password.value;
    const confirmPass = this.loginForm.controls.confirmPassword.value;
    //console.log(pass);
    //console.log(confirmPass);
    if (pass === confirmPass) {
      return true;
    } else {
      return false;
    }
  }


  get control(): any {
    return this.loginForm.controls;
  }

  getUserData() {
    // authgetUsersData

    this.param = {
      auth: this.auth
    }
    this.housewiseService.getUserDetails(this.param).subscribe((result) => {
      // console.log(result);
      for (const key in result) {
        if (key === 'Success') {
          this.userData = result.Success;
          //  console.log(this.userData);
        } else if (key === 'rror') {

        }
      }
      // console.log(JSON.stringify(result));
    }, (err) => {
      console.log(err);
    });
  }


  onSubmit() {
    this.isSubmitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.password_match_error = false;
    // console.log(this.checkPasswords());
    if (!this.checkPasswords()) {
      this.password_match_error = true;
      return;
    }

    this.invalid = false;

    this.param = {
      id: this.userData.user_id,
      new_password: this.loginForm.value.password
    }
    this.isLoading = true;

    this.housewiseService.setForgotPassword(this.param).subscribe((result: any) => {
      ///  console.log("result: ", result);
      this.isLoading = false;
      for (const key in result) {
        if (key === "Success") {
          //    console.log("success " + result.Success);
          this.toastr.success('Password set successfully', 'Success:');
          this.router.navigate(['/auth/login']);
        } else if (key === 'Error') {
          this.toastr.success(result.Error, 'Error:');
        }
      }
    }, (e) => {
      this.isLoading = false;
      console.log(e.error.Error);
    });

  }

}
