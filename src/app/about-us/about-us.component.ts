import { Component, OnInit, AfterViewInit } from '@angular/core';
import { baseComponent } from '../baseComponent';
import { Title } from '@angular/platform-browser';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { constants } from '../constants';


declare var gtag;

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit, AfterViewInit {

  showMore = true;
  constructor(private titleSerive: Title, private router:Router) {
    
  }

  ngOnInit() {
    const navEndEvents =  this.router.events.pipe(filter(event => event instanceof NavigationEnd),);

      navEndEvents.subscribe((event: NavigationEnd) => {
        gtag('config', constants.GAID, { 
          'page_path': event.urlAfterRedirects
        }); 
      });
  }


  // TODO: Cross browsing
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  ngAfterViewInit() {
    const testScript1 = document.getElementById('homeScript');
    if (testScript1) {
      testScript1.remove();
    }
    const testScript = document.createElement('script');
    testScript.setAttribute('id', 'homeScript');
    testScript.setAttribute('src', './assets/js/home-script.js');
    document.body.appendChild(testScript);
  }

  toggleShowMoreClass() {
    this.showMore = !this.showMore;
  }
}
