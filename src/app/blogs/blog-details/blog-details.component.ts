import { HousewiseService } from 'src/app/service/housewise.service';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import * as $ from 'jquery';
import { Router, NavigationEnd } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { data } from 'jquery';
import { SeoService } from 'src/app/service/seo.service';
import { filter } from 'rxjs/operators';
import { constants } from '../../constants';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';


declare var gtag;

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.css'],
  // encapsulation: ViewEncapsulation.None,
})
export class BlogDetailsComponent implements OnInit, OnDestroy {

  blogData;
  loadingList = false;
  dataError = false;
  blogDesc: SafeHtml;

  constructor(private location: Location, private router: Router, private sanitizer:DomSanitizer,
              private housewiseService: HousewiseService, public meta: SeoService) { 
      
  }

  ngOnInit() {
    const navEndEvents =  this.router.events.pipe(filter(event => event instanceof NavigationEnd),);

      navEndEvents.subscribe((event: NavigationEnd) => {
        gtag('config', constants.GAID, { 
          'page_path': event.urlAfterRedirects
        }); 
      });

    let currentUrl = this.router.url;
    let blogHeaderName = currentUrl.split('/').pop();

    $('html, body').animate({ scrollTop: 0 }, 500);
    this.getBlogDetails(blogHeaderName);

  }

  ngOnDestroy() {
    localStorage.removeItem('blogDetailsData');
  }


  getBlogDetails(blogHeaderName) {
    this.loadingList = true;
    const params = new HttpParams().set('blog_url_name', blogHeaderName + '');
    this.housewiseService.getBlogsDetails(params).subscribe((result) => {

      this.loadingList = false;
      for (const key in result) {
        if (key === 'Success') {
          this.blogData = result.Success;
          this.blogDesc = this.sanitizer.bypassSecurityTrustHtml(this.blogData.text);
          this.updateMeta();
        } else if (key === "Error") {
          this.dataError = true;
        }
      }
    }, (err) => {
      this.loadingList = false;
      this.dataError = true;
    });
  }

  updateMeta() {
    this.meta.updateTitle(this.blogData.blog_meta_title);
    this.meta.updateMetaInfo(this.blogData.blog_meta_description, this.blogData.blog_meta_title, this.blogData.blog_meta_keywords);
  }


}
