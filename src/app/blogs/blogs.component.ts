import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as $ from 'jquery';
import { HousewiseService } from '../service/housewise.service';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Router, NavigationEnd } from '@angular/router';
import { SeoService } from '../service/seo.service';
import { metaData } from '../home/meta.data';
import { filter } from 'rxjs/operators';
import { constants } from '../constants';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';


declare var gtag;


@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css'],
  // encapsulation: ViewEncapsulation.None
})
export class BlogsComponent implements OnInit {

  blogCategoryId: number = 1;
  blogData: any;
  popularBlogs: any[];

  currentPage = 1;
  pageCount = 1;
  paginationArray = [];

  timeout = null;
  pageNumber = 1;
  errMessage: string;
  isSearchResult = false;
  searchString = '';
  loadingList = false;
  dataError = false;

  blogDesc: SafeHtml;

  constructor(private housewiseService: HousewiseService,
    private router: Router, private sanitizer:DomSanitizer,
    public meta: SeoService) {
    this.meta.updateTitle(metaData.blogPageMetaData.title);
    this.meta.updateMetaInfo(metaData.blogPageMetaData.description, metaData.blogPageMetaData.title, metaData.blogPageMetaData.keyword);

    
  }

  ngOnInit() {

    const navEndEvents =  this.router.events.pipe(filter(event => event instanceof NavigationEnd),);

      navEndEvents.subscribe((event: NavigationEnd) => {
        gtag('config', constants.GAID, { 
          'page_path': event.urlAfterRedirects
        }); 
      });

    $('html, body').animate({ scrollTop: 0 }, 500);

    this.getBlogList();

  }

  getBlogList() {
    this.loadingList = true;
    //const params = new HttpParams().set('blog_category_id', this.blogCategoryId + '').set('page', this.currentPage + '');
    const params = new HttpParams().set('page', this.currentPage + '');
    this.housewiseService.getBlogs(params).subscribe((result) => {
      // console.log(result);
      for (const key in result) {
        if (key === 'Success') {
          // localStorage.setItem('blogData', result.Success);
          this.blogData = result.Success.data;
          //  console.log("Blog Data: ", this.blogData);
          this.pageCount = Number(result.Success.pageCount);
          this.createPaginationArray();
          this.createPopularBlogArray();
          this.loadingList = false;
        } else if (key === "Error") {
          this.loadingList = false;
          this.dataError = true;
          return;
        }
      }
    }, (err) => {
      this.loadingList = false;
      this.dataError = true;
      console.log(err);
    });
  }

  getSafeHtml(blogText:any) {
    this.blogDesc = this.sanitizer.bypassSecurityTrustHtml(blogText);
    return this.blogDesc;
  }

  createPopularBlogArray() {
    this.popularBlogs = [];
    for (const blog of this.blogData) {
      if (blog.name === 'Popular Posts') {
        this.popularBlogs.push(blog);
      }
    }
    // console.log(this.popularBlogs);
  }

  /** Create Paginator start */
  createPaginationArray() {
    this.paginationArray = [];
    let firstIndex = Math.floor(this.currentPage / 10) + 1;
    if (firstIndex > 1) {
      firstIndex = firstIndex * 10 - 10;
    }
    for (let index = firstIndex, k = 0; index <= this.pageCount && k < 10; index++ , k++) {
      this.paginationArray.push(index);
    }
    // console.log('paginationArray' + this.paginationArray);
  }

  changePage(pageNumber) {
    this.currentPage = pageNumber;
    this.getBlogList();
  }

  previousPage() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getBlogList();
    }
  }

  nextPage() {
    if (this.currentPage < this.pageCount) {
      this.currentPage++;
      this.getBlogList();
    }
  }
  /** Create Paginator End */

  clearBlog() {
    if (this.searchString === '') {
      this.searchBlog();
    }
  }

  searchBlog() {
    const searchEle = document.getElementById('search') as HTMLInputElement;

    clearTimeout(this.timeout);

    if (this.searchString === '') {
      this.clearSearchString();
    } else {
      this.timeout = setTimeout(() => {
        this.pageNumber = 1;
        this.getSearchBlogData(searchEle.value);
        //  console.log(searchEle.value);
        this.searchString = searchEle.value;
        //console.log(this.searchString);
      }, 800);
    }

  }

  getSearchBlogData(searchString) {
    //  console.log(searchString);

    const searchEle = document.getElementById('search') as HTMLInputElement;
    const params = new HttpParams().set('searchString', searchString + '');
    this.housewiseService.getSearchBlogs(params).subscribe((result) => {
      //  console.log('data' + result);
      if (result !== null) {

        for (const key in result) {
          // console.log("in if 1");
          if (key === 'Success') {
            // console.log("in success");
            //console.log('Success' + JSON.stringify(result.Success));
            // this.pageCount = Number(result.Success.pageCount);
            this.pageCount = 0;
            this.blogData = result.Success.data;
            this.createPaginationArray();
            //  console.log("Blog Data: ", this.blogData);
          } else if (key === 'Error') {
            //  console.log("in error1");
            this.isSearchResult = true;
            this.errMessage = 'No Blog available!';
            if (result.error === 'token_expired') {
              //this.refreshAuth(1);
              //  console.log("in token expired");
            } else {
              this.errMessage = 'No Blogs Available!';
              this.blogData = result.Success;
              this.pageCount = 0;
            }
            return;
          }
          this.isSearchResult = false;
        }
      } else {
        this.dataError = true;
        //  console.log("in error 2", result.error);
        //this.refreshAuth(1);
      }
      // console.log(JSON.stringify(result));
    }, (err) => {
      this.dataError = true;
      console.log('err:' + err);
    });
  }


  clearSearchString() {

    this.searchString = '';
    const searchEle = document.getElementById('search') as HTMLInputElement;
    // searchEle.value = this.searchString;    
    this.isSearchResult = false;
    this.getBlogList();
    //  console.log("cleared search string");
  }

  goToBlogDetails(blogObject) {
    //localStorage.setItem('blogDetailsData', JSON.stringify(blogObject));
    var blogUrl = blogObject.blog_url_name;
    //blogUrl = blogUrl.replace(/\s+/g, '-').toLowerCase();
    this.router.navigate(['blog-details', blogUrl]);
  }


  goToContactUs() {
    this.router.navigate(['/home', '1']);
  }

  // TODO: Cross browsing
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

}
