import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { SeoService } from 'src/app/service/seo.service';
import { metaData } from '../../meta.data';

@Component({
    selector: 'app-rental-property',
    templateUrl: '../readmore.component.html',
    styleUrls: ['../readmore.component.css']
})
export class RentalPropertyComponent implements OnInit {

    readMore: Number;

    constructor(private route: ActivatedRoute, public meta: SeoService) {
        this.readMore = 1;
        this.meta.updateTitle(metaData.rentalManagementMetaData.title);
        this.meta.updateMetaInfo(metaData.rentalManagementMetaData.description, metaData.rentalManagementMetaData.title, metaData.rentalManagementMetaData.keyword);
    }

    ngOnInit() {
    }

}
