import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { SeoService } from 'src/app/service/seo.service';
import { metaData } from '../../meta.data';

@Component({
    selector: 'app-property-management',
    templateUrl: '../readmore.component.html',
    styleUrls: ['../readmore.component.css']
})
export class PropertyManagementComponent implements OnInit {

    readMore: Number;

    constructor(private route: ActivatedRoute, public meta: SeoService) {
        this.readMore = 3;
        this.meta.updateTitle(metaData.propertyManagementMetaData.title);
        this.meta.updateMetaInfo(metaData.propertyManagementMetaData.description, metaData.propertyManagementMetaData.title, metaData.propertyManagementMetaData.keyword);
    }

    ngOnInit() {
    }

}
