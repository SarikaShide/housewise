import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { SeoService } from 'src/app/service/seo.service';
import { metaData } from '../../meta.data';

@Component({
    selector: 'app-end-to-end-services',
    templateUrl: '../readmore.component.html',
    styleUrls: ['../readmore.component.css']
})
export class EndToEndServicesComponent implements OnInit {

    readMore: Number;

    constructor(private route: ActivatedRoute, public meta: SeoService) {
        this.readMore = 2;
        this.meta.updateTitle(metaData.endToEndServicesMetaData.title);
        this.meta.updateMetaInfo(metaData.endToEndServicesMetaData.description, metaData.endToEndServicesMetaData.title, metaData.endToEndServicesMetaData.keyword);
    }

    ngOnInit() {
    }

}
