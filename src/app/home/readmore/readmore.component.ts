import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { SeoService } from 'src/app/service/seo.service';
import { metaData } from '../meta.data';
import { filter } from 'rxjs/operators';
import { constants } from '../../constants';


declare var gtag;

@Component({
  selector: 'app-readmore',
  templateUrl: './readmore.component.html',
  styleUrls: ['./readmore.component.css']
})
export class ReadmoreComponent implements OnInit {

  readMore: Number;
  constructor(private route: ActivatedRoute, public meta: SeoService, private router:Router) {

    this.route.params.subscribe(res=> {
     // console.log(res);
      if(res.name=='rental-property-management'){
        this.readMore = 1;
        this.meta.updateTitle(metaData.rentalManagementMetaData.title);
        this.meta.updateMetaInfo(metaData.rentalManagementMetaData.description, metaData.rentalManagementMetaData.title, metaData.rentalManagementMetaData.keyword);
      } else if(res.name=='property-maintenance-services'){
        this.readMore = 2;
        this.meta.updateTitle(metaData.endToEndServicesMetaData.title);
        this.meta.updateMetaInfo(metaData.endToEndServicesMetaData.description, metaData.endToEndServicesMetaData.title, metaData.endToEndServicesMetaData.keyword);
      } else if(res.name=='property-management-services'){
        this.readMore = 3;
        this.meta.updateTitle(metaData.propertyManagementMetaData.title);
        this.meta.updateMetaInfo(metaData.propertyManagementMetaData.description, metaData.propertyManagementMetaData.title, metaData.propertyManagementMetaData.keyword);
      }
      
    });

    
   }

  ngOnInit() {
    const navEndEvents =  this.router.events.pipe(filter(event => event instanceof NavigationEnd),);

      navEndEvents.subscribe((event: NavigationEnd) => {
        gtag('config', constants.GAID, { 
          'page_path': event.urlAfterRedirects
        }); 
      });
  }

}
