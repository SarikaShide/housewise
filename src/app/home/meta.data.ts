export class metaData {
    static homeMetaData = {
        name: "Home Page",
        pageUrl: "https://www.housewise.in/",
        title: "Rental Property Management Services in India| Housewise|+917755910850",
        keyword: "rental management solutions, property management companies in India, rental property management services, manage property in India, property management services for NRI, Property Management in Pune, Bengaluru, Hyderabad, Gurgaon, Noida, Chennai, Housewise, Professional property management services India",
        description: "Rental Property Management Company in India | Housewise | Hyderabad | Pune | Bengaluru | Gurgaon | Noida call : +91-77 55 91 0850"
    }

    static puneMetaData = {
        name: "Pune",
        pageUrl: "https://www.housewise.in/property-management-services-in-Pune",
        title: "Property Management Company in Pune | Housewise | 77 55 91 0850",
        keyword: "rental agencies in pune, property maintenance services in pune, best property management companies in pune, property management services in pune, property management pune, nri property management services pune, top property management companies in pune, professional nri property management services in pune, property management pune, rental property management in pune, rental agreement in pune, real estate investment in pune, power of attorney pune, property tax pune, property listing services in pune, licence and leave agreement in pune, property management Waked, Hinjewadi, Pimpri, Chinchwad, Aundh",
        description: "Are you looking for property management services in Pune? We are an experienced team of IIT and BITS graduates who take full charge of complete society formalities, renting the property, background verification, compliances and tenant management."
    }

    static bangloreMetaData = {
        name: "Bangalore",
        pageUrl: "https://www.housewise.in/property-management-services-in-Bangalore",
        title: "Property Management Company in Bangalore | Housewise | 77 55 91 0850",
        keyword: "rental agencies in bangalore, property maintenance services in bangalore, best property management companies in bangalore, property management services in bangalore, property management bangalore, nri property management services bangalore, top property management companies in bangalore, professional nri property management services in bangalore, property management bangalore, rental property management in bangalore, rental agreement in bangalore, real estate investment in bangalore, power of attorney bangalore, property tax bangalore, property listing services in bangalore, licence and leave agreement in bangalore, property management Bellandur, Bannerghatta Road, Whitefield, Koramangala, KR Puram, Marathahalli",
        description: "Are you looking for property management services in Bangalore? We are an experienced team of IIT and BITS graduates who take full charge of complete society formalities, renting the property, background verification, compliances and tenant management."
    }

    static hyderabadMetaData = {
        name: "Hyderabad",
        pageUrl: "https://www.housewise.in/property-management-services-in-Hydrabad",
        title: "Property Management Company in Hyderabad | Housewise | 77 55 91 0850",
        keyword: "rental agencies in hyderabad, property maintenance services in hyderabad, best property management companies in hyderabd, property management services in hyderabad, property management hyderabad, nri property management services hyderabad, top property management companies in hyderabad, professional nri property management services in hyderabad, property management hyderabad, rental property management in hyderabad, rental agreement in hyderabad, real estate investment in hyderabad, power of attorney hyderabad, property tax hyderabad, property listing services in hyderabad, licence and leave agreement in hyderabad, property management Kondapur , Gachibowli, Jubilee Hills, Ramantapur, Ameerpet",
        description: "Are you looking for property management services in Hyderabad? We are an experienced team of IIT and BITS graduates who take full charge of complete society formalities, renting the property, background verification, compliances and tenant management."
    }

    static gurugraonMetaData = {
        name: "Gurgaon",
        pageUrl: "https://www.housewise.in/property-management-services-in-Gurugram",
        title: "Property Management Company in Gurgaon | Housewise | 77 55 91 0850",
        keyword: "rental agencies in gurugram, property maintenance services in gurugram, best property management companies in gurugram, property management services in gurugram, property management gurugram, nri property management services chennai, top property management companies in gurugram, professional nri property management services in gurugram, property management gurugram, rental property management in gurugram, rental agreement in gurugram, real estate investment in gurugram, power of attorney gurugram, property tax gurugram, property listing services in gurugram, licence and leave agreement in gurugram, property management Sohna Road, Sec- 43 Gurgaon, NH-8 Gurgaon, Golf Course Extension Road, Dwarka Expressway, DLF City",
        description: "Are you looking for property management services in Gurugram? We are an experienced team of IIT and BITS graduates who take full charge of complete society formalities, renting the property, background verification, compliances and tenant management."
    }

    static chennaiMetaData = {
        name: "Chennai",
        pageUrl: "https://www.housewise.in/property-management-services-in-Chennai",
        title: "Property Management Company in Chennai | Housewise | 77 55 91 0850",
        keyword: "rental agencies in chennai, property maintenance services in chennai, best property management companies in chennai, property management services in chennai, property management chennai, nri property management services chennai, top property management companies in chennai, professional nri property management services in chennai, property management chennai, rental property management in chennai, rental agreement in chennai, real estate investment in chennai, power of attorney chennai, property tax chennai, property listing services in chennai, licence and leave agreement in chennai, property management Adyar, Thiruvanmiyur, Velachery, Numgambakkam, T nagar, Besant nagar, ECR",
        description: "Are you looking for property management services in Chennai? We are an experienced team of IIT and BITS graduates who take full charge of complete society formalities, renting the property, background verification, compliances and tenant management."
    }

    static delhiMetaData = {
        name: "Delhi",
        pageUrl: "https://www.housewise.in/property-management-services-in-Delhi",
        title: "Property Management Company in Delhi | Housewise | 77 55 91 0850",
        keyword: "rental agencies in delhi, property maintenance services in delhi, best property management companies in delhi, property management services in delhi, property management delhi, nri property management services delhi, top property management companies in delhi, professional nri property management services in delhi, property management delhi, rental property management in delhi, rental agreement in delhi, real estate investment in delhi, power of attorney delhi, property tax delhi, property listing services in delhi, licence and leave agreement in delhi, property management greater kailash, R.K Puram, Friends Colony, Hauz khas, Defence Colony, Badarpur, Janakpuri",
        description: "Are you looking for property management services in Delhi? We are an experienced team of IIT and BITS graduates who take full charge of complete society formalities, renting the property, background verification, compliances and tenant management."
    }

    static mumbaiMetaData = {
        name: "Mumbai",
        pageUrl: "https://www.housewise.in/property-management-services-in-Mumbai",
        title: "Property Management Company in Mumbai | Housewise | 77 55 91 0850",
        keyword: "rental agencies in mumbai, property maintenance services in mumbai, best property management companies in mumbai, property management services in mumbai, property management mumbai, nri property management services chennai, top property management companies in mumbai, professional nri property management services in mumbai, property management mumbai, rental property management in mumbai, rental agreement in mumbai, real estate investment in mumbai, power of attorney mumbai, property tax mumbai, property listing services in mumbai, property management Thane, Ghatkopar, Parel, Chembur, Andheri, Bandra, Dahisar, Mulund, Powai, Dombivli, Borivali",
        description: "Are you looking for property management services in Mumbai? We are an experienced team of IIT and BITS graduates who take full charge of complete society formalities, renting the property, background verification, compliances and tenant management."
    }

    static blogPageMetaData = {
        name: "Blog Page",
        pageUrl: "https://www.housewise.in/blog",
        title: "Our Blogs- Housewise",
        keyword: "property management companies for nri, property management services in India, property management in Bangalore, rental property management companies in Chennai, residential property management for nri, property management cost for nri, rental management solutions in India, property management companies in India, rental property management services in India, manage property in India, property management services for NRI, rental Property Management in Pune, Bengaluru, Hyderabad, Gurgaon, Noida, Chennai, Housewise, rental management companies in India, home property management in India, professional property management in India, residential property management in India ",
        description: "Deciding to use a rental property management company to care for your property is one of the most important decisions you can make as a property-owner. Follow our Housewise Property Management Blogs to keep up to date related to rental property services."
    }

    static rentalManagementMetaData = {
        name: "Rental Management",
        pageUrl: "https://www.housewise.in/rental-property-management",
        title: "Rental Property Management in India | Housewise",
        keyword: "property management companies in India, rental property management services, nri property management services, Property Management in Pune, Bengaluru, Hyderabad, Gurgaon, Noida, Chennai, Housewise, Professional property management services India, top property management companies in pune, lease rental agreement in pune",
        description: "Housewise is a leading rental Property management company in India which has been providing its clients with hassle-free rental services for many years ago in Bangalore, Pune, Chennai, Hyderabad, Mumbai, Delhi, Gurugram."
    }

    static endToEndServicesMetaData = {
        name: "End To End Services ",
        pageUrl: "https://www.housewise.in/property-maintenance-services",
        title: "Property Rental Management Company | Property Management in India",
        keyword: "property management company in India, rental property management services in pune, nri property management services india, Property Management services in Pune, Bengaluru, Hyderabad, Gurgaon, Noida, Chennai, Housewise, Professional property management services India, property maintenance services in pune, power of attorney pune",
        description: "Looking for a Rental Property Maintenance Services in India? Housewise in one of the best Rental Property Maintenance in Hyderabad, Pune, Mumbai, Chennai, Noida, Pune, Delhi, and Bangalore."
    }

    static propertyManagementMetaData = {
        name: "Property Management",
        pageUrl: "https://www.housewise.in/rental-property-management",
        title: "Property Management Services Company in Bangalore, Pune, Mumbai – Housewise",
        keyword: "property rental management companies in India, property rental management services india, nri property management services Hyderabad, Property rental Management services in Pune, Bengaluru, Hyderabad, Gurgaon, Noida, Chennai, Housewise, Professional property rental management services India",
        description: "Check out key advantages of using the services of NRI Rental Property Management Services in India? We offers Property Management Services for owners looking for guaranteed rent."
    }


}