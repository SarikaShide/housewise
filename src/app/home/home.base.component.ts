import { Observable } from 'rxjs';
import { Component, OnInit, AfterViewInit, OnDestroy, HostListener, ViewChild, ElementRef } from '@angular/core';
import * as $ from 'jquery';
import * as AOS from 'aos';
import { Router, ActivatedRoute } from '@angular/router';
import * as waypoint from 'waypoint';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from 'src/app/service/data.service';
import { ToastrService } from 'ngx-toastr';
import { Title, DomSanitizer } from '@angular/platform-browser';
import { HousewiseService } from '../service/housewise.service';
import { SeoService } from '../service/seo.service';
import { CONTEXT } from '@angular/core/src/render3/interfaces/view';
import { SecurityContext } from '@angular/compiler/src/core';
// import { SearchCountryField, TooltipLabel, CountryISO } from 'ngx-intl-tel-input';


export class HomeBaseComponent implements OnInit {


    @ViewChild('homeSection') homeSection: ElementRef;
    @ViewChild('servicesSection') servicesSection: ElementRef;
    @ViewChild('howItWorksSection') howItWorksSection: ElementRef;
    @ViewChild('teamSection') teamSection: ElementRef;
    @ViewChild('contactUsSection') contactUsSection: ElementRef;
    @ViewChild('countersView') countersView: ElementRef;

    slideConfig = {
        "slidesToShow": 4,
        "slidesToScroll": 1,
        "dots": true,
        "infinite": true,
        "autoplay": true,
        "autoplaySpeed": 2500,
        "mobileFirst": true,
        "responsive": [
          {
            "breakpoint": 300,
            "settings": {
              "slidesToShow": 1
            }
          },
          {
            "breakpoint": 500,
            "settings": {
              "slidesToShow": 2
            }
          },
          {
            "breakpoint": 1000,
            "settings": {
              "slidesToShow": 3
            }
          },
          {
            "breakpoint": 1200,
            "settings": {
              "slidesToShow": 4
            }
          }
        ]
      };

    isLoading = false;

    contactForm: FormGroup;

    isSubmitted = false;
    validForm = true;
    id: any;
    menuname: any;
    cityName = 'INDIA';
    solutionInCityName = "India"
    country_code = 91;
    contactNumber;
    submitForm;

    intervalFirst;
    intervalSecond;
    counterInterval;
    cityCountryCounterInterval;

    totalProperties = 510;
    totalClient = 1020;
    totalSqft = this.totalProperties * 1300;
    totalCities = 7;
    totalCountris = 21;

    propertyCounter = 0;
    sqftCounter = 1;
    clientCounter = 0;
    citiesCounter = 0;
    countriesCounter = 0;
    _this = this;
    captcha;
    isRecaptchaValid = false;
    loadingList: boolean = false;
    newsFeedData: any;
    dataError: boolean = false;

    constructor(
        public route: ActivatedRoute,
        public router: Router,
        public fb: FormBuilder,
        public housewiseService: HousewiseService,
        public dataService: DataService,
        public toastr: ToastrService,
        public titleService: Title,
        public meta: SeoService,
        public sanitizer: DomSanitizer
        ) {
            this.getCounterValues();
            this.getNewsFeeds();
    }

    ngOnInit() {     

    }


    ngAfterViewInit() {
        const testScript1 = document.getElementById('homeScript');
        if (testScript1) {
            testScript1.remove();
        }
        const testScript = document.createElement('script');
        testScript.setAttribute('id', 'homeScript');
        testScript.setAttribute('src', './assets/js/home-script.js');
        document.body.appendChild(testScript);
    }

    getCounterValues() {
        this.housewiseService.getCounterDetails().subscribe((result) => {
            
            for (const key in result) {
                if (key === 'Success') {
                    let data = result.Success[0];
                    this.totalProperties = data.properties;
                    this.totalClient = data.happy_clients;
                    this.totalSqft = data.square_feet;
                    this.totalCities = data.cities_covered;
                    this.totalCountris = data.countries;
                }
                if (key === 'Error') {
                    console.log(result.Error);
                }
            }
        }, (err) => {
            console.log(err);
        });
    }

    // country_code: ['', [Validators.required, Validators.pattern('\+[0-9]{2}')]],

    initForm() {

        this.contactForm = this.fb.group({
            name: ['', [Validators.required, Validators.pattern(/^[a-zA-Z\s]*$/)]],
            country_code: ['', [Validators.required]],
            contact_no: ['', [Validators.required, Validators.pattern('[0-9]{10}')]],
            email: ['', [Validators.required, Validators.pattern('^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$')]],
            subject: ['', Validators.required],
            message: ['', Validators.required],
            captcha: ['']
        });

    }

    getNewsFeeds() {
        this.loadingList = true;
        this.housewiseService.getNewsFeedsList().subscribe((result) => {
            // console.log(result);
            this.loadingList = false;
            for (const key in result) {
              if (key === 'Success') {
                this.newsFeedData = result.Success;
               // console.log(this.newsFeedData);
              } else if (key === "Error") {
                this.dataError = true;
                return;
              }
            }
          }, (err) => {
            this.loadingList = false;
            this.dataError = true;
            console.log(err);
          });
    }

    onSubmit(form: any) {

        //console.log(form);
        this.isSubmitted = true;
        if (form.invalid) {
            this.validForm = false;
            return;
        }
        // console.log(this.contactForm.value);
        if (this.isRecaptchaValid === false) {
            this.toastr.error("Please verify captcha", "Error:");
            return;
        }

        this.validForm = true;

        this.submitForm = {
            name: form.value.name,
            contact_no: form.value.country_code + "-" + form.value.contact_no,
            email: form.value.email,
            subject: form.value.subject,
            message: form.value.message,
            captcha: this.captcha,
        }
        this.isLoading = true;
        this.housewiseService.contactSubmit(this.submitForm).subscribe(res => {
            this.isLoading = false;
            this.isSubmitted = false;
            //alert('submitted successfully');
            for (let key in res) {
                if (key === "Success") {
                    this.toastr.success("Contact request submitted", "Success:");

                    this.contactForm.reset();
                    //  console.log(res.Success);
                } else if (key === "Error") {
                    this.toastr.error("Failed to send contact request", "Error:")
                    //  console.log(res.Error);
                }
            }
        }, e => {
            console.log(e);
            this.toastr.error("Failed to send contact request", "Error:");
        });

    }



    get control(): any {
        //console.log(this.contactForm.controls);
        return this.contactForm.controls;
    }

    loadReadmore(val: Number) {
        // alert(val);
        let serviceName;
        if (val == 1) {
            serviceName = 'rental-property-management';
            //this.router.navigate(['/rental-property-management' ]);
        } else if (val == 2) {
            serviceName = 'property-maintenance-services';
            //this.router.navigate(['/property-maintenance-services' ]);
        } else if (val == 3) {
            serviceName = 'property-management-services';
            //this.router.navigate(['/property-management-services' ]);
        }
        this.router.navigate(['/services', serviceName]);
    }

    gotoNewsFeedDetails(newsFeed) {
     
        localStorage.setItem('newsFeedData', JSON.stringify(newsFeed));

        let newsFeedUrl = newsFeed.title.replace( /[^a-zA-Z0-9]/g,' ');
        newsFeedUrl = newsFeedUrl.replace(/\s+/g, '-').toLowerCase();
       // console.log(newsFeedUrl);
        
        if(newsFeedUrl === 'housewise-offers-house-owners-the-option-to-check-the-tenant-s-credit-history') {
            newsFeedUrl = 'housewise-offers-owners-to-check-the-tenants-credit-history';
        } else if (newsFeedUrl === 'property-management-startup-housewise-selected-for-the-exclusive-google-digital-accelerator-programme-') {
            newsFeedUrl = 'housewise-property-management-startup-in-pune';            
        } else if (newsFeedUrl === '-housewise-title-sponsor-for-tact-4-ops-quiz-at-symbiosis-centre-for-management-and-human-resource-development-') {
            newsFeedUrl = 'housewise-title-sponsor-in-quiz-at-symbiosis-centre'
        }

        this.router.navigate(['news-feed', newsFeedUrl]);
    }


    scrollTo(id: any) {
        let contactoffsetTop = document.getElementById('contact').offsetTop;
        // alert(id);
        this.menuname = id;
        try {
            const element =
                id == 'Services' ? 'services' :
                    id == 'How It Works' ? 'how' :
                        id == 'Team' ? 'founders' :
                            id == 'Contact Us' ? 'contact' : '';
            const time =
                id == 'Services' ? 500 :
                    id == 'How It Works' ? 1000 :
                        id == 'Team' ? 1500 :
                            id == 'Contact Us' ? 2000 : '';


            $("html, body").animate({
                scrollTop: $("#" + element).offset().top
            }, time);
        } catch (e) {
            console.log(e);
        }

    }

    scrollToContactUs(id: any) {
        try {
            $("html, body").animate({
                scrollTop: $("#contact").offset().top + 150
            }, 2000);
        } catch (e) {
            console.log(e);
        }
    }

    // TODO: Cross browsing
    gotoTop() {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }

    changeCityName(cityName) {


        this.cityName = cityName;
        this.gotoTop();
    }


    changeWords() {
        let words = [
            'HESITANT',
            'WORRIED'
        ], i = 0;
        clearInterval(this.intervalFirst);
        clearInterval(this.intervalSecond);
        this.intervalFirst = setInterval(function () {
            $('#changingword').fadeOut(function () {
                $(this).html(words[i = (i + 1) % words.length]).fadeIn();
            });
        }, 4500);

        let words2 = [
            'RENTING',
            'MANAGING'
        ], i2 = 0;
        this.intervalSecond = setInterval(function () {
            $('#change').fadeOut(function () {
                $(this).html(words2[i2 = (i2 + 1) % words2.length]).fadeIn();
            });
        }, 4500);
    }

    ngOnDestroy(): void {
        clearInterval(this.intervalFirst);
        clearInterval(this.intervalSecond);
        clearInterval(this.counterInterval);
        clearInterval(this.cityCountryCounterInterval);
    }



    @HostListener('window:scroll', ['$event'])
    @HostListener('window:resize', ['$event'])
    doSomething(event) {
        // console.debug("Scroll Event", document.body.scrollTop);
        // see András Szepesházi's comment below
        var currentPosition = window.pageYOffset;
        if (currentPosition >= this.homeSection.nativeElement.offsetTop && currentPosition <= (this.homeSection.nativeElement.offsetHeight + this.homeSection.nativeElement.offsetTop - 70)) {
            this.dataService.currentMenuChanged('Home');
        } else if (currentPosition >= this.servicesSection.nativeElement.offsetTop - 70 && currentPosition <= (this.servicesSection.nativeElement.offsetHeight + this.servicesSection.nativeElement.offsetTop)) {
            this.dataService.currentMenuChanged('Services');

        } else if (currentPosition >= this.howItWorksSection.nativeElement.offsetTop && currentPosition <= (this.howItWorksSection.nativeElement.offsetHeight + this.howItWorksSection.nativeElement.offsetTop - 100)) {
            this.dataService.currentMenuChanged('How It Works');
        } else if (currentPosition >= this.teamSection.nativeElement.offsetTop - 70 && currentPosition <= (this.teamSection.nativeElement.offsetHeight + this.teamSection.nativeElement.offsetTop - 100)) {
            this.dataService.currentMenuChanged('Team');
        } else if (currentPosition >= this.contactUsSection.nativeElement.offsetTop -150 && currentPosition <= (this.contactUsSection.nativeElement.offsetHeight + this.contactUsSection.nativeElement.offsetTop)) {
            this.dataService.currentMenuChanged('Contact Us');
        }

    }


    increamentCount() {
        var _this = this;
        clearInterval(this.counterInterval);
        clearInterval(this.cityCountryCounterInterval);
        this.counterInterval = setInterval(function () {
            //console.log(_this.totalProperties + '\t' + _this.propertyCounter);
            if (_this.totalProperties > _this.propertyCounter) {
                _this.propertyCounter = _this.propertyCounter + 2;
            } else {
                _this.propertyCounter = _this.totalProperties;
            }

            if (_this.totalSqft > _this.sqftCounter) {
                _this.sqftCounter = _this.sqftCounter + 1800;
            } else {
                _this.sqftCounter = _this.totalSqft;
            }

            if (_this.totalClient > _this.clientCounter) {
                _this.clientCounter = _this.clientCounter + 8;
            } else {
                _this.clientCounter = _this.totalClient;
            }

            if (_this.totalCities > _this.citiesCounter) {
                _this.citiesCounter = _this.citiesCounter + 8
            } else {
                _this.citiesCounter = _this.totalCities;
            }

        }, 10);

        this.cityCountryCounterInterval = setInterval(function () {
            // if (_this.totalCities > _this.citiesCounter) {
            //     _this.citiesCounter++;
            // }

            if (_this.totalCountris > _this.countriesCounter) {
                _this.countriesCounter++;
            }

        }, 70);

    }

    resolved(captchaResponse: string) {
        if (captchaResponse === null || captchaResponse === undefined || captchaResponse === '') {
            // this.toastr.error("Captcha verification failed", "Error:")
            return;
        } else {
            this.isRecaptchaValid = true;
        }
        console.log(`Resolved captcha with response: ${captchaResponse}`);
        this.captcha = captchaResponse;
    }

    getSafeHTML(value: {}) {
        // If value convert to JSON and escape / to prevent script tag in JSON
        const json = value ? JSON.stringify(value, null, 2).replace(/\//g, '\\/') : '';
        const html = `${json}`;
        return this.sanitizer.bypassSecurityTrustHtml(html);
    }

}

