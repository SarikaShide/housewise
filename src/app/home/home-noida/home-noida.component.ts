import { HomeBaseComponent } from './../home.base.component';
import { HomeComponent } from './../home/home.component';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { HousewiseService } from 'src/app/service/housewise.service';
import { DataService } from 'src/app/service/data.service';
import { ToastrService } from 'ngx-toastr';
import { Title, DomSanitizer } from '@angular/platform-browser';
import { SeoService } from 'src/app/service/seo.service';
import { seoData } from '../seo.data';
import { metaData } from '../meta.data';
import { seoFooterData } from '../seoFooter.data';
import { filter } from 'rxjs/operators';
import { constants } from '../../constants';


declare var gtag;

@Component({
  selector: 'app-home-chennai',
  templateUrl: '../home/home.component.html',
  styleUrls: ['../home/home.component.css']
})
export class HomeNoidaComponent extends HomeBaseComponent implements OnInit {

  defaultAddress = constants.noidaAddress;

  // get the SEO data 
  schema = ''
  htmlText = seoFooterData.noidaSEOFooterData.htmlText;  
  cityName = "NOIDA"
  solutionInCityName = "India - Noida"


  constructor(public route: ActivatedRoute,
    public router: Router,
    public fb: FormBuilder,
    public housewiseService: HousewiseService,
    public dataService: DataService,
    public toastr: ToastrService,
    public sanitizer: DomSanitizer,
    public titleService: Title,
    public meta: SeoService) {
    super(route, router, fb, housewiseService, dataService, toastr, titleService, meta, sanitizer);
    route.params.subscribe(val => {
      this.ngOnInit();
    });

    
  }

  ngOnInit() {

    const navEndEvents =  this.router.events.pipe(filter(event => event instanceof NavigationEnd),);

      navEndEvents.subscribe((event: NavigationEnd) => {
        gtag('config', constants.GAID, { 
          'page_path': event.urlAfterRedirects
        }); 
      });

    this.initForm();

    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    //console.log(this.id);
    if (this.id === '1') {
      this.scrollTo('Contact Us');
    }

    this.dataService.currentMenuChanged('Home');
    this.dataService.currentCityChange('/property-management-services-in-Noida')

    this.changeWords();

    this.dataService.changeCuurentHomeMenuObservable.subscribe((currentMenu) => {
      if (currentMenu === 'Services') {
        this.increamentCount();
      }
    });
  }

}
