
export class seoData {
    static homeSEO = {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "Housewise",
        "url": "https://www.housewise.in",
        "logo": "https://www.housewise.in/images/logohw.png",
        "foundingDate": "2015",
        "founders": [
            {
                "@type": "Person",
                "name": "Pryank Agrawal"
            },
            {
                "@type": "Person",
                "name": "Manojeet Chowdhury"
            }],
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "91 Springboard Sky Loft, Creaticity Mall, Opposite Golf Course, Off, Airport Rd, Shastrinagar, Yerawada, Pune",
            "addressLocality": "Yerewada",
            "addressRegion": "Pune",
            "postalCode": "411006",
            "addressCountry": "India"
        },
        "contactPoint": {
            "@type": "ContactPoint",
            "telephone": " +91-7755910850 ",
            "contactType": "Sales",
            "contactOption": "Paid",
            "areaServed": "India"
        },
        "sameAs": [
            " https://www.facebook.com/housewiseindia ",
            " https://twitter.com/Housewise_in ",
            "https://www.linkedin.com/company/company/housewise",
            "https://www.crunchbase.com/organization/housewise"
        ]
    }



    static puneSEO = {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "Housewise Rental Property Management Services Company in India Pune Bengaluru Hyderabad Gurugram Noida Chennai Mumbai",
        "url": "https://www.housewise.in/",
        "telephone": "+91 7755910850",
        "email": "sales@housewise.in",
        "logo": "https://www.housewise.in/images/logohw.png",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "91 Springboard Sky Loft, Creaticity Mall, Opposite Golf Course, Off, Airport Rd, Shastrinagar,",
            "addressLocality": "Yerawada",
            "addressRegion": "Pune , Maharashtra",
            "postalCode": "411006"
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "4.8",
            "author": "Housewise",
            "reviewCount": "70"
        },
        "sameAs": ["https://plus.google.com/112523773421322782542",
            "https://www.facebook.com/housewiseindia/",
            "https://twitter.com/housewiseindia",
            "https://www.linkedin.com/company/housewise/"]


    }


    static bangloreSEO = {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "Housewise provides property management services in Pune, Bangalore, Mumbai, Hyderabad, Chennai, Gurgaon",
        "url": "https://www.housewise.in/property-management-services-in-Bangalore.jsp",
        "telephone": "+91 7755910850",
        "email": "sales@housewise.in",
        "logo": "https://www.housewise.in/images/logohw.png",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Spacelance Office Solutions Pvt.Ltd.",
            "addressLocality": "39, 2nd Floor, NGEF Lane Indiranagar Stage1",
            "addressRegion": "Bengaluru",
            "postalCode": "560038"
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "4.7",
            "author": "Housewise",
            "reviewCount": "1284"
        },
        "sameAs": ["https://www.facebook.com/housewiseindia",
            "https://twitter.com/Housewise_in",
            "https://www.linkedin.com/company/company/housewise"]
    }


    static hyderabadSEO = {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "Housewise provides property management services in Pune, Bangalore, Mumbai, Hyderabad, Chennai, Gurgaon",
        "url": "https://www.housewise.in/property-management-services-in-Hyderabad.jsp",
        "telephone": "+91 7755910850",
        "email": "sales@housewise.in",
        "logo": "https://www.housewise.in/images/logohw.png",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Unit D, T-Square Building, 4th Floor Kavuri Hills, ",
            "addressLocality": "Madhapur",
            "addressRegion": "Hyderabad, Telangana ",
            "postalCode": "500033"
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "4.8",
            "author": "Housewise",
            "reviewCount": "1374"
        },
        "sameAs": ["https://www.facebook.com/housewiseindia",
            "https://twitter.com/Housewise_in",
            "https://www.linkedin.com/company/company/housewise"]
    }


    static gurugramSEO = {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "Housewise provides property management services in Pune, Bangalore, Mumbai, Hyderabad, Chennai, Gurgaon",
        "url": "https://www.housewise.in/property-management-services-in-Gurgaon.jsp",
        "telephone": "+91 7755910850",
        "email": "sales@housewise.in",
        "logo": "https://www.housewise.in/images/logohw.png",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Unit No.258, Spaze iTech Park, Sector 49",
            "addressLocality": "Sohna Road",
            "addressRegion": "Gurugram",
            "postalCode": "122001"
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "4.9",
            "author": "Housewise",
            "reviewCount": "850"
        },
        "sameAs": ["https://www.facebook.com/housewiseindia",
            "https://twitter.com/Housewise_in",
            "https://www.linkedin.com/company/company/housewise"]
    }


    static chennaiSEO = {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "Housewise provides property management services in Pune, Bangalore, Mumbai, Hyderabad, Chennai, Gurgaon",
        "url": "https://www.housewise.in/property-management-services-in-Chennai.jsp",
        "telephone": "+91 7755910850",
        "email": "sales@housewise.in",
        "logo": "https://www.housewise.in/images/logohw.png",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "715-A, 7th Floor, Spencer Plaza, Suite No.499",
            "addressLocality": "Mount Road, Anna Salai, Chennai",
            "addressRegion": "Chennai",
            "postalCode": "600 002"
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "4.5",
            "author": "Housewise",
            "reviewCount": "1850"
        },
        "sameAs": ["https://www.facebook.com/housewiseindia",
            "https://twitter.com/Housewise_in",
            "https://www.linkedin.com/company/company/housewise"]
    }


    static delhiSEO = {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "Housewise provides property management services in Pune, Bangalore, Mumbai, Hyderabad, Chennai, Gurgaon, Delhi",
        "url": "https://www.housewise.in/property-management-services-in-Delhi.jsp",
        "telephone": "+91 7755910850",
        "email": "sales@housewise.in",
        "logo": "https://www.housewise.in/images/logohw.png",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Unit No.258, Spaze iTech Park, Sector 49",
            "addressLocality": "Sohna Road",
            "addressRegion": "Gurugram",
            "postalCode": "122001"
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "4.75",
            "author": "Housewise",
            "reviewCount": "885"
        },
        "sameAs": ["https://www.facebook.com/housewiseindia",
            "https://twitter.com/Housewise_in",
            "https://www.linkedin.com/company/company/housewise"]
    }


    static mumbaiSEO = {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "Housewise provides property management services in Pune, Bangalore, Mumbai, Hyderabad, Chennai, Gurgaon, Delhi",
        "url": "https://www.housewise.in/property-management-services-in-Mumbai.jsp",
        "telephone": "+91 7755910850",
        "email": "sales@housewise.in",
        "logo": "https://www.housewise.in/images/logohw.png",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "B-2/2, 8th Floor, Times Square, Bima Nagar, Western Express Highway",
            "addressLocality": "Andheri East",
            "addressRegion": "Mumbai",
            "postalCode": "400069"
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "4.55",
            "author": "Housewise",
            "reviewCount": "1775"
        },
        "sameAs": ["https://www.facebook.com/housewiseindia",
            "https://twitter.com/Housewise_in",
            "https://www.linkedin.com/company/company/housewise"]
    }

}

