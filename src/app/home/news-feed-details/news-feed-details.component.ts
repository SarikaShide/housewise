import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-news-feed-details',
  templateUrl: './news-feed-details.component.html',
  styleUrls: ['./news-feed-details.component.css']
})
export class NewsFeedDetailsComponent implements OnInit, OnDestroy {
  newsFeed: any;

  constructor(private route:ActivatedRoute) { 
    this.newsFeed = JSON.parse(localStorage.getItem('newsFeedData'));
    //console.log(this.newsFeed);
  }

  ngOnInit() {
    
  }

  ngOnDestroy() {
    localStorage.removeItem('newsFeedData');
  }

}
