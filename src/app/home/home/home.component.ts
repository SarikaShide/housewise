import { HomeBaseComponent } from './../home.base.component';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { HousewiseService } from 'src/app/service/housewise.service';
import { DataService } from 'src/app/service/data.service';
import { ToastrService } from 'ngx-toastr';
import { Title, DomSanitizer } from '@angular/platform-browser';
import { SeoService } from 'src/app/service/seo.service';
import { seoData } from '../seo.data';
import { metaData } from '../meta.data';
import { seoFooterData } from '../seoFooter.data';
import { filter } from 'rxjs/operators';
import { constants } from '../../constants';


declare var gtag;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent extends HomeBaseComponent implements OnInit {

  defaultAddress = constants.puneAddress;

  // get the SEO data 
  schema = seoData.homeSEO;
  htmlText = seoFooterData.homeSEOFooterData.htmlText;
  cityName = 'INDIA';
  solutionInCityName = "India - Pune, Bengaluru, Hyderabad, Gurugram, Noida, Chennai, Mumbai";

  constructor(public route: ActivatedRoute,
    public router: Router,
    public fb: FormBuilder,
    public housewiseService: HousewiseService,
    public dataService: DataService,
    public toastr: ToastrService,
    public titleService: Title,
    public sanitizer: DomSanitizer,
    public meta: SeoService) {
    super(route, router, fb, housewiseService, dataService, toastr, titleService, meta, sanitizer);
    this.meta.updateTitle(metaData.homeMetaData.title);
    this.meta.updateMetaInfo(metaData.homeMetaData.description, metaData.homeMetaData.title, metaData.homeMetaData.keyword);
    route.params.subscribe(val => {
      this.ngOnInit();
    });

    
  }

  ngOnInit() {

    const navEndEvents = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

    navEndEvents.subscribe((event: NavigationEnd) => {
      gtag('config', constants.GAID, {
        'page_path': event.urlAfterRedirects
      });
    });

    //console.log(this.schema);
    this.initForm();

    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    //console.log(this.id);
    if (this.id === '1') {
      this.scrollToContactUs('Contact Us');
    } else if (this.id === '2') {
      this.scrollTo('Services');
    }
    this.changeWords();

    this.dataService.changeCuurentHomeMenuObservable.subscribe((currentMenu) => {
      if (currentMenu === 'Services') {
        this.increamentCount();
      }
    });
  }

  ngAfterViewInit() {
    const testScript1 = document.getElementById('SchemaTag');
    if (testScript1) {
      testScript1.remove();
    }
    const testScript = document.createElement('script');
    testScript.setAttribute('type', 'application/ld+json');
    testScript.innerHTML = JSON.stringify(this.schema);
    document.body.appendChild(testScript);

    const testScript2 = document.getElementById('homeScript');
    if (testScript2) {
      testScript2.remove();
    }
    const testScript3 = document.createElement('script');
    testScript3.setAttribute('id', 'homeScript');
    testScript3.setAttribute('src', './assets/js/home-script.js');
    document.body.appendChild(testScript3);
  }
}

