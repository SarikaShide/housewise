export class constants {

    //Housewise Test GAID
    static GAID = 'G-XWM7HMBL4K'; 
    
    //Housewise Live GAID (housewise.in)
    // static GAID = 'UA-66146868-1'; 
    
    static tenant: String = "Tenant";
    static owner: String = "Owner";
    //static newUserDate = "2019-10-02";

    static puneAddress = "91 Springboard Sky Loft, Creaticity Mall,Opposite Golf Course, Off, Airport Rd, Shastrinagar, Yerawada, Pune, Maharashtra 411006";
    static mumbaiAddress = "Orbit Plaza, c-154 Mascots,103/104 1st Floor,New Prabhadevi Marg, Mumbai, Maharashtra - 400025";
    static bangloreAddress = "Spacelance Office Solutions Pvt.Ltd. #39,2nd Floor,NGEF Lane, Indiranagar, Stage1 Bangalore-560038";
    static delhiAddress = "91 Springboard Sky Loft, Creaticity Mall, Opposite Golf Course, Off, Airport Rd, Shastrinagar, Yerawada, Pune,Maharashtra 411006";
    static noidaAddress = "Unit No. 258, Spaze iTech Park, Sector 49, Sohna Road,Gurugram 122001";
    static gurugramAddress = "Unit No. 258, Spaze iTech Park, Sector 49, Sohna Road,Gurugram 122001";
    static chennaiAddress = "715-A, 7th Floor, Spencer Plaza, Suite No.499 Mount Road, Anna Salai, Chennai - 600 002";
    static hyderabadAddress = "Unit -D, T Square Building, 4th floor,Kavuri Hills, Madhapur,Hyderabad - 500033";

    static setUserData(data) {
        localStorage.setItem("HWUserData", data);
    }

    static getUserData() {
        return localStorage.getItem("HWUserData");
    }

    static getNoImage () {
        return './assets/images/no_photo_available.png';   
    }

}