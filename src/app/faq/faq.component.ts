import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { constants } from '../constants';


declare var gtag;

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  constructor(private router:Router) { 
   
  }

  ngOnInit() {

     const navEndEvents =  this.router.events.pipe(filter(event => event instanceof NavigationEnd),);

      navEndEvents.subscribe((event: NavigationEnd) => {
        gtag('config', constants.GAID, { 
          'page_path': event.urlAfterRedirects
        }); 
      });

    $('html, body').animate({ scrollTop: 0 }, 500);
    this.toggleFaq();
  }

  // TODO: Cross browsing
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  toggleFaq() {
    /*----------------------------------------------------*/
		/*	Toggle
		/*----------------------------------------------------*/

    $(".toggle-container").hide();

    $('.trigger, .trigger.opened').on('click', function (a) {
      $(this).toggleClass('active');
      a.preventDefault();
    });

    $(".trigger").on('click', function () {
      $(this).next(".toggle-container").slideToggle(300);
    });

    $(".trigger.opened").addClass("active").next(".toggle-container").show();
  }

}
