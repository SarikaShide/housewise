import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { constants } from './../../constants';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  userRole = constants.owner.toUpperCase();

  guestUser = [
    { name: 'Home', url: ['/'] },
    { name: 'Services', url: ['/'] },
    { name: 'How It Works', url: ['/'] },
    { name: 'About us', url: ['/about-us'] },
    { name: 'Team', url: ['/'] },
    { name: 'Contact Us', url: ['/'] }
  ]
  User = [
    { name: 'Dashboard', url: ['/user/owner-dashboard'] },

    { name: 'Submit New Property', url: ['/user/submit-property'] }
  ]
  // { name: 'My Properties', url: ['/user/listing-page'] },

  tenant = [
    { name: 'Home', url: ['/user/tenant-dashboard'] },
    { name: 'View Properties', url: ['/user/tenant-property-list'] },
    { name: 'Shortlisted Properties', url: ['/user/shortlisted-property'] },
    { name: 'Interested Properties', url: ['/user/interested-property'] }
  ]


  constructor() {

  }

  getUserRole() {
    return localStorage.getItem('userRole')
  }

  getHeader(usertype: string) {
    if (usertype.toUpperCase() === 'guest'.toUpperCase()) {
      return this.guestUser
    } else if (usertype.toUpperCase() === 'owner'.toUpperCase()) {
      return this.User
    } else {
      return this.tenant;
    }
  }


}
