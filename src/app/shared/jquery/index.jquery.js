<script type="text/javascript" src="./assets/js/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="./assets/js/chosen.min.js"></script>
<script type="text/javascript" src="./assets/js/magnific-popup.min.js"></script>
<script type="text/javascript" src="./assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="./assets/js/rangeSlider.js"></script>
<script type="text/javascript" src="./assets/js/sticky-kit.min.js"></script>
<script type="text/javascript" src="./assets/js/slick.min.js"></script>
<script type="text/javascript" src="./assets/js/mmenu.min.js"></script>
<script type="text/javascript" src="./assets/js/tooltips.min.js"></script>
<script type="text/javascript" src="./assets/js/masonry.min.js"></script>
<script type="text/javascript" src="./assets/js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="./assets/js/custom.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="./assets/js/aos.js"></script>
<script>

  AOS.init();

</script>


<script type="text/javascript">
	$(document).ready(function () {
    $(document).on("scroll", onScroll);
    
    //smoothscroll 

    $('a[href="index.php.parallax"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        
        $('a').each(function () {
            $(this).removeClass('current');
        })
        $(this).addClass('current');
      
        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });

    $('a[href="index.php#services"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        
        $('a').each(function () {
            $(this).removeClass('current');
        })
        $(this).addClass('current');
      
        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });

        $('a[href="index.php#how"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        
        $('a').each(function () {
            $(this).removeClass('current');
        })
        $(this).addClass('current');
      
        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });


        $('a[href="index.php#contact"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        
        $('a').each(function () {
            $(this).removeClass('current');
        })
        $(this).addClass('current');
      
        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });
});


function onScroll(event){
		var scrollPosition = $(document).scrollTop();
		$('nav a').each(function () {
			var currentLink = $(this);
			var refElement = $(currentLink.attr("href"));
			if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
				$('nav ul li a').removeClass("current");
				currentLink.addClass("current");
			}else{
				currentLink.removeClass("current");
			}
		});
	}


///scroll on click
// var sections = $('section')
//   , nav = $('nav')
//   , nav_height = nav.outerHeight();

// $(window).on('scroll', function () {
//   var cur_pos = $(this).scrollTop();
  
//   sections.each(function() {
//     var top = $(this).offset().top - nav_height,
//         bottom = top + $(this).outerHeight();
    
//     if (cur_pos >= top && cur_pos <= bottom) {
//       nav.find('a').removeClass('current');
//       sections.removeClass('current');
      
//       $(this).addClass('current');
//       nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('current');
//     }
//   });
// });

// nav.find('a').on('click', function () {
//   var $el = $(this)
//     , id = $el.attr('href');
  
//   $('html, body').animate({
//     scrollTop: $(id).offset().top - nav_height
//   }, 1000);
  
//   return false;
// });


/*
Using jquery waypoints to change active on scroll
*/
//index 
 $('.parallax').waypoint(function() {

  $("nav ul li a").removeClass("current");
  $("nav ul li #home").addClass("current");
  
}, { offset: 101 });

//services 
 $('#services').waypoint(function() {

  $("nav ul li a").removeClass("current");
  $("nav ul li #serv").addClass("current");
  
}, { offset: 101 });

//how
 $('#how').waypoint(function() {

  $("nav ul li a").removeClass("current");
  $("nav ul li #how-work").addClass("current");
  
}, { offset: 201 });

//Contact
 $('#contact').waypoint(function() {

  $("nav ul li a").removeClass("current");
  $("nav ul li #cont").addClass("current");
  
}, { offset: 301 });


</script>
<script>
{/* (function(){
      var words = [
	      'HESITANT',
          'WORRIED'
		  
          
          ], i = 0;
      setInterval(function(){
          $('#changingword').fadeOut(function(){
              $(this).html(words[i=(i+1)%words.length]).fadeIn();
          });
      }, 3500);
        
  })();
  
  (function(){
      var words = [
	      'RENTING',
          ' MANAGING'
		  
          
          ], i = 0;
      setInterval(function(){
          $('#change').fadeOut(function(){
              $(this).html(words[i=(i+1)%words.length]).fadeIn();
          });
      }, 3500);
        
  })(); */}
</script>

<script>
var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myIndex++;
  if (myIndex > x.length) {myIndex = 1}    
  x[myIndex-1].style.display = "block";  
  setTimeout(carousel, 4000);    
}

  AOS.init();

</script>
</script>
<!--<script>
	//pune 
$( "span.pune" ).hover(
  function() {
	  $("span.pune").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span>91 Springboard Sky Loft, Creaticity Mall, Opposite Golf Course, Off, Airport Rd, Shastrinagar, Yerawada, Pune, Maharashtra 411006</span>" ) );
  }, function() {
	  $("span.pune").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
  }
);

//Bengaluru
$( "span.Bengaluru" ).hover(
  function() {
	   $("span.Bengaluru").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span>Spacelance Office Solutions Pvt.Ltd. #39,2nd Floor,NGEF Lane, Indiranagar, Stage1 Bangalore-560038 </span>" ) );
  }, function() {
	  	  $("span.Bengaluru").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
  }
);

//Hyderabad
$( "span.Hyderabad" ).hover(
  function() {
	   $("span.Hyderabad").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span>Unit -D, T Square Building, 4th floor,<br> Kavuri Hills, Madhapur,Hyderabad - 500033</span>" ) );
  }, function() {
	    $("span.Hyderabad").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
  }
);

//Gurugram 
$( "span.Gurugram" ).hover(
  function() {
	   $("span.Gurugram").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span>Unit No. 258, Spaze iTech Park, Sector 49, Sohna Road,Gurugram 122001</span>" ) );
  }, function() {
	   $("span.Gurugram").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
  }
);

//Noida 
$( "span.Noida" ).hover(
  function() {
	    $("span.Noida").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span>Unit No. 258, Spaze iTech Park, Sector 49, Sohna Road,Gurugram 122001</span>" ) );
  }, function() {
	   $("span.Noida").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
  }
);

// Chennai 
$( "span.Chennai" ).hover(
  function() {
	    $("span.Chennai").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span>715-A, 7th Floor, Spencer Plaza, Suite No.499 Mount Road, Anna Salai, Chennai - 600 002</span>" ) );
  }, function() {
	  $("span.Chennai").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
  }
);

//Delhi 
$( "span.Delhi" ).hover(
  function() {
	   $("span.Delhi").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span>91 Springboard Sky Loft, Creaticity Mall, Opposite Golf Course,Off, Airport Rd, Shastrinagar, Yerawada, Pune,Maharashtra 411006</span>" ) );
  }, function() {
	  	  $("span.Delhi").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
  }
);

//Mumbai
$( "span.Mumbai" ).hover(
  function() {
	   $("span.Mumbai").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span>Orbit Plaza, c-154 Mascots,103/104 1st Floor,New Prabhadevi Marg, Mumbai, Maharashtra - 400025</span>" ) );
  }, function() {
	  	  $("span.Mumbai").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
  }
);
</script>-->
<script>
   $("#news-slider13").owlCarousel({
        navigation : false,
        pagination : true,
        items : 3,
        itemsDesktop:[1199,3],
        itemsDesktopSmall:[980,2],
        itemsMobile : [600,1],
        navigationText : ["",""]
    });
</script>
<script>
 	//pune 
$( "span.pune" ).hover(
  function() {
  	 $("span.pune").css("color", "#8dc63f");
  	 $("div.punediv").show();
     }, function() {
  	$("span.pune").css("color", ""); 
  	$("div.punediv").show(); 
  }
);

//Bengaluru
$( "span.Bengaluru" ).hover(
  function() {
	   $("span.Bengaluru").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span><center><b>Spacelance Office Solutions Pvt.Ltd. #39,2nd Floor,NGEF Lane, Indiranagar, Stage1 Bangalore-560038</b> </center></span>" ) );
    $("div.punediv").hide();
  }, function() {
  $("span.Bengaluru").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
    $("div.punediv").show();
  }
);

//Hyderabad
$( "span.Hyderabad" ).hover(
  function() {
	   $("span.Hyderabad").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span><center><b>Unit -D, T Square Building, 4th floor,Kavuri Hills, Madhapur,Hyderabad - 500033</b></center></span>" ) );
    $("div.punediv").hide();
  }, function() {
	  	    $("span.Hyderabad").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
    $("div.punediv").show();
  }
);

//Gurugram 
$( "span.Gurugram" ).hover(
  function() {
	    $("span.Gurugram").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<center><span><b>Unit No. 258, Spaze iTech Park, Sector 49, Sohna Road,Gurugram 122001</b></center></span>" ) );
    $("div.punediv").hide();
  }, function() {
	   $("span.Gurugram").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
    $("div.punediv").show();
  }
);

//Noida 
$( "span.Noida" ).hover(
  function() {
	   $("span.Noida").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span><center><b>Unit No. 258, Spaze iTech Park, Sector 49, Sohna Road,Gurugram 122001</b></center></span>" ) );
    $("div.punediv").hide();
  }, function() {
	   $("span.Noida").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
    $("div.punediv").show();
  }
);

// Chennai 
$( "span.Chennai" ).hover(
  function() {
	    $("span.Chennai").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span><center><b>	715-A, 7th Floor, Spencer Plaza, Suite No.499 Mount Road, Anna Salai, Chennai - 600 002</b></center></span>" ) );
    $("div.punediv").hide();
  }, function() {
	   	  $("span.Chennai").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
    $("div.punediv").show();
  }
);

//Delhi 
$( "span.Delhi" ).hover(
  function() {
	    $("span.Delhi").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span><center><b>91 Springboard Sky Loft, Creaticity Mall, Opposite Golf Course, Off, Airport Rd, Shastrinagar, Yerawada, Pune,Maharashtra 411006</b></center></span>" ) );
    $("div.punediv").hide();
  }, function() {
		  	  $("span.Delhi").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
    $("div.punediv").show();
  }
);

//Mumbai
$( "span.Mumbai" ).hover(
  function() {
	   $("span.Mumbai").css("color", "#8dc63f");
    $( "div.address" ).append( $( "<span><center><b>Orbit Plaza, c-154 Mascots,103/104 1st Floor,New Prabhadevi Marg, Mumbai, Maharashtra - 400025</b></center></span>" ) );
    $("div.punediv").hide();
  }, function() {
	  	  	  $("span.Mumbai").css("color", "");
    $( "div.address" ).find( "span:last" ).remove();
    $("div.punediv").show();
  }
);
</script>