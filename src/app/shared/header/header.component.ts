import { constants } from './../../constants';
import { DataService } from './../../service/data.service';
import { Component, OnInit, ViewChild, ElementRef, HostListener, Directive, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeaderService } from '../services/header.service'
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { reduce } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @ViewChild('collapse') collapse: ElementRef;

  Menu: any[];
  usermenu: boolean = false;
  usermenuphone: boolean = false;
  userMenuSticky: boolean = false;
  phoneNavToggle: boolean = false;
  showUser = false;
  userName: string;
  userData: any;
  thisPointer;
  selectedHeaderName = 'Home';
  lastMenuPath = '/';



  constructor(private router: Router, private header: HeaderService, private dataService: DataService,
    private activatedRoute: ActivatedRoute, private eRef: ElementRef) {
    this.Menu = this.header.getHeader('guest');
    this.router.events.subscribe(res => {
      let currentUrl = this.router.url;
      const last = currentUrl.split('/').pop();
      //console.log(this.last);
      if (last === 'shortlisted-property') {
        this.selectedHeaderName = 'Shortlisted Properties';
      } else if (last === 'interested-property') {
        this.selectedHeaderName = 'Interested Properties';
      } else if (last === 'tenant-property-list') {
        this.selectedHeaderName = 'View Properties';
      } else if (last === 'tenant-dashboard') {
        this.selectedHeaderName = 'Home';
      } else if (last === 'submit-property') {
        this.selectedHeaderName = 'Submit Property';
      } else if (last === 'listing-page') {
        this.selectedHeaderName = 'My Properties';
      } else if (last === 'owner-dashboard') {
        this.selectedHeaderName = 'Dashboard';
      } else if (last === 'about-us') {
        this.selectedHeaderName = 'About us';
      } else if (last === 'login' || last === 'signup') {
        this.selectedHeaderName = '';
      }
      this.getMenu();

    });

    this.thisPointer = this;

  }


  ngOnInit() {
    if (constants.getUserData()) {
      this.showUser = true;
    } else {
      this.showUser = false;
    }

    this.dataService.logInChange.subscribe((message) => {
      this.getMenu();
      this.usermenu = false;
      this.usermenuphone = false;
      setTimeout(() => {
        this.setUpStickyMenu();
      }, 500);
    });

    this.dataService.changeCuurentHomeMenuObservable.subscribe((currentMenu) => {
      this.selectedHeaderName = currentMenu;
    });

    this.dataService.changeCurrentCityObservable.subscribe((currentCity) => {
      this.lastMenuPath = currentCity
    })
  }


  toggleuser() {

    this.usermenu = !this.usermenu;
   // console.log(this.usermenu)
    if (this.usermenu === true) {
      this.userMenuSticky = false;
    }
  }

  toggleUserSticky() {

    this.userMenuSticky = !this.userMenuSticky;
    //  console.log(this.userMenuSticky);
    if (this.userMenuSticky === true) {
      this.usermenu = false
      this.usermenuphone = false;
      ;
    }

  }

  toggleUserMenuPhone() {
    this.usermenuphone = !this.usermenuphone
  }

  closeUserMenu() {
    // console.log("pavan");
    this.usermenu = false;
  }

  closeUserStickyMenu() {
    //  console.log("pavan");
    this.userMenuSticky = false;
  }

  closeUserMenuPhone() {
    this.usermenuphone = false;
  }

  closeNav() {
    // alert('clicked');
    const a = this.collapse.nativeElement as HTMLElement;
    a.className = 'collapse navbar-collapse mobile-navbar navbar-fixed-top';
    // this.collapse.nativeElement.collapse('hide');
  }

  closePhoneNav(event) {


    this.phoneNavToggle = false;
    this.usermenuphone = false
  }


  togglePhoneMenu() {
    this.phoneNavToggle = !this.phoneNavToggle
  }

  scrollTo(id: any) {
    this.userData = JSON.parse(constants.getUserData());
    if (!this.userData) {
      setTimeout(() => {
        try {
          const element =
            id == 'Services' ? 'services' :
              id == 'How It Works' ? 'how' :
                id == 'Team' ? 'founders' :
                  id == 'Contact Us' ? 'contact' : '';
          const time =
            id == 'Services' ? 500 :
              id == 'How It Works' ? 1000 :
                id == 'Team' ? 1500 :
                  id == 'Contact Us' ? 2000 : '';

          this.selectedHeaderName = id;

          $("html, body").animate({
            scrollTop: $("#" + element).offset().top
          }, time);

        } catch (e) {
          console.log(e);
        }
        if (id === 'Home' || id === '') {
          this.selectedHeaderName = 'Home';
          window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
          });
        }
      }, 50);
    }



  }

  onLogout() {
    localStorage.removeItem('userRole');
    localStorage.removeItem('token');
    localStorage.removeItem('HWUserData');
    localStorage.removeItem('cityId');
    localStorage.removeItem('editPropertyData');
    this.userMenuSticky = false;
    this.userMenuSticky = false;
    this.usermenuphone = false;
    this.router.navigate(['/']);
    setTimeout(() => {
      this.getMenu();
      this.setUpStickyMenu();
    }, 1000);
  }

  getMenu() {
    try {
      this.userData = JSON.parse(constants.getUserData());
      if (this.userData) {
        if (this.userData.user_type.toUpperCase() === constants.owner.toUpperCase()) {
          this.Menu = this.header.getHeader('owner'.toUpperCase());
          this.showUser = true;
        } else if (this.userData.user_type.toUpperCase() === constants.tenant.toUpperCase()) {
          this.Menu = this.header.getHeader('tenant'.toUpperCase());
          this.showUser = true;
        } else {
          this.Menu = this.header.getHeader('guest'.toUpperCase());
          this.showUser = false;
        }
        if (this.userData.first_name) {
          this.userName = this.userData['first_name'];
        }
      } else {
        this.Menu = this.header.getHeader('guest');
        this.showUser = false;
      }
    }
    catch (e) {
      this.Menu = this.header.getHeader('guest');
      this.showUser = false;
    }
  }





  setUpStickyMenu() {
    // var x = document.getElementsByClassName("cloned");

    // for (let index = 0; index < x.length; index++) {
    //   const element = x[index];
    //   element.remove();
    // }


    //$("#header").not("#header-container #header .cloned").clone(true).addClass('cloned unsticky').insertAfter("#header");
    $("#navigation").clone(true).addClass('cloned unsticky').insertAfter("#navigation.style-2");

    // Logo for header style 2
    $("#logo .sticky-logo").clone(true).prependTo("#navigation.cloned ul#responsive");


    // sticky header script
    var headerOffset = 50 // height on which the sticky header will shows
    let _this = this;
    $(window).scroll(function () {
      _this.userMenuSticky = false;
      _this.usermenu = false;
      _this.usermenuphone = false;
      if ($(window).scrollTop() >= headerOffset) {
        $("#header.cloned").addClass('sticky').removeClass("unsticky");
        $("#nav_header").addClass('phone-sticky-header');
      } else {
        $("#header.cloned").addClass('unsticky').removeClass("sticky");
        $("#nav_header").removeClass('phone-sticky-header');
      }
    });
  }

  changeMenu(item) {

    this.userData = JSON.parse(constants.getUserData());
    if (!this.userData) {
      if (item.name) {
        // console.log(item.name);
        if (item.name == 'About us') {
          this.router.navigate(item.url);
        } else if (item.name == 'Home') {
          this.router.navigate(item.url);
          this.lastMenuPath = '/';
        } else {
          this.router.navigate([this.lastMenuPath]);
        }
      }
    } else {
      this.router.navigate(item.url);
    }

    this.selectedMenu(item.name)
    this.phoneNavToggle = false;
  }

  selectedMenu(menuName) {
    this.selectedHeaderName = menuName;
    //  console.log(this.selectedHeaderName);
  }

  getUserInitial() {
    return this.userName.charAt(0);
  }


}
