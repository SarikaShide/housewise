import { Component, OnInit, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { HeaderService } from 'src/app/shared/services/header.service';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/service/data.service';
import { HousewiseService } from './service/housewise.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  loginForm: FormGroup;
  invalid = false;
  isSubmitted = false;
  param;
  isLoading = false;

  constructor(private header: HeaderService,
    private formBuilder: FormBuilder, private housewiseService: HousewiseService, private router: Router,
    private toaster: ToastrService, 
    private data: DataService) {

  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group(
      {
        username: ['', [Validators.required, Validators.pattern(/^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/)]]
      }
    );
  }



  get control(): any {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.isSubmitted = true;

    if (this.loginForm.invalid) {
      this.invalid = true;
      return;
    }

    this.invalid = false;
    this.param = {
      user_email_id: this.loginForm.value.username
    };
    this.param = JSON.stringify(this.param);
    this.isLoading = true;
    this.housewiseService.forgotPassword(this.param).subscribe((res: any) => {
      this.isLoading = false;
      for (const key in res)
        if (key === "Success") {
          this.toaster.success('Password sent to email please check email', 'Success:');
          document.getElementById('closeModal').click();
        } else if (key === 'Error') {
          this.toaster.error(res.Error, 'Error:');
        }
    }, (e) => {
      this.isLoading = false;
    });
  }

}
