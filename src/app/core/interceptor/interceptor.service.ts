import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { constants } from 'src/app/constants';

@Injectable({providedIn: 'root'})
export class Interceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const headersConfig = {
      'Content-Type': 'application/json',
      'x-api-key': 'Sagar@12',
    };
    const userData = JSON.parse(constants.getUserData());

    if (userData && userData.Authorization) {
      headersConfig['Authorization'] = `Bearer ${userData.Authorization}`;
    }

    const request = req.clone({ setHeaders: headersConfig });
    return next.handle(req);
  }
}