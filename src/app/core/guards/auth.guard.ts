import { constants } from './../../constants';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }

  public verifyLoggedInAsTenant(): boolean {
    let status = false;
    let userData = JSON.parse(constants.getUserData());
    if (userData && (userData.user_type.toUpperCase() === constants.tenant.toUpperCase() || userData.user_type.toUpperCase() === constants.owner.toUpperCase())) {
      status = true;
    } else {
      status = false;
      // this.router.navigate(['/home']);
    }
    return status;
  }

}
