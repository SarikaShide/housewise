import { constants } from './../../constants';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TenantGuard implements CanActivate {

  constructor(private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.verifyLoggedInAsTenant();
  }

  public verifyLoggedInAsTenant(): boolean {
    let status = false;
    let userData = JSON.parse(constants.getUserData());
    if (userData && userData.user_type.toUpperCase() === constants.tenant.toUpperCase()) {
      status = true;
    } else if (userData && userData.user_type.toUpperCase() === constants.owner.toUpperCase()) {
      this.router.navigate(['/user/owner-dashboard']);
    } else {
      status = false;
    }
    return status;
  }
}
