import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { constants } from 'src/app/constants';

@Injectable({
  providedIn: 'root'
})
export class GuestGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.verifyNoOneLoggenIn();
  }


  verifyNoOneLoggenIn(): boolean {
    let status = false;
    try{
      let userData = JSON.parse(constants.getUserData());
      //  console.log(userData);
      if (userData && userData != null) {
        this.router.navigate(['user']);
        status = false;
      } else {
        status = true;
      }
    }catch(exp){
      //console.log(exp);
      return status;
    }
        
    return status;
  }
}
