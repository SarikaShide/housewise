import { constants } from './../../constants';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OwnerGuard implements CanActivate {
  constructor(private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.verifyLoggedInAsOwner();
  }

  public verifyLoggedInAsOwner(): boolean {
    let status = false;
    let userData = JSON.parse(constants.getUserData());

    if (userData && userData.user_type.toUpperCase() === constants.owner.toUpperCase()) {
      status = true;
      //  console.log('owner');
    } else if (userData && userData.user_type.toUpperCase() === constants.tenant.toUpperCase()) {
      this.router.navigate(['/user/tenant-dashboard']);
      status = false;
    } else {
      status = false;
    }
    return status;
  }

}
