import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { constants } from '../constants';


declare var gtag;

@Component({
  selector: 'app-terms-condition',
  templateUrl: './terms-condition.component.html',
  styleUrls: ['./terms-condition.component.css']
})
export class TermsConditionComponent implements OnInit,AfterViewInit {
  
 

  constructor(private router:Router) {
    
  }

  ngOnInit() {
    const navEndEvents =  this.router.events.pipe(filter(event => event instanceof NavigationEnd),);

      navEndEvents.subscribe((event: NavigationEnd) => {
        gtag('config', constants.GAID, { 
          'page_path': event.urlAfterRedirects
        }); 
      });
  }

  ngAfterViewInit() 
  {

    const scroll1 = document.getElementById('scroll');
    if (scroll1) {
      scroll1.remove();
    }
      const scrollanimation=document.createElement('animation');
      scrollanimation.setAttribute('id','scroll');
      scrollanimation.setAttribute('src','./assets/js/scroll-animation.js');
      document.body.appendChild(scrollanimation);
 
  }

// TODO: Cross browsing
gotoTop() {
  window.scroll({ 
    top: 0, 
    left: 0, 
    behavior: 'smooth' 
  });
}

}
