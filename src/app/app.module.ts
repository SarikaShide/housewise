import { HomeChennaiComponent } from './home/home-chennai/home-chennai.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown-angular7';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { Interceptor } from './core/interceptor/interceptor.service';
import { OwlModule } from 'ngx-owl-carousel';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ReadmoreComponent } from './home/readmore/readmore.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { BlogsComponent } from './blogs/blogs.component';
import { FaqComponent } from './faq/faq.component';
import { BlogDetailsComponent } from './blogs/blog-details/blog-details.component';
import { NewsFeedDetailsComponent } from './home/news-feed-details/news-feed-details.component';
import { SlickModule } from 'ngx-slick';
import { ToastrModule } from 'ngx-toastr'
import { MatDatepickerModule } from '@angular/material/datepicker';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS, OwlDateTimeIntl } from 'ng-pick-datetime';
import { HashLocationStrategy, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { MatChipsModule } from '@angular/material/chips';
import { ClickOutsideModule } from 'ng4-click-outside';

import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';

import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
  LinkedinLoginProvider,
} from "angular-6-social-login";

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataService } from './service/data.service';
import { MatNativeDateModule } from '@angular/material/core';
import { HomePuneComponent } from './home/home-pune/home-pune.component';
import { HomeHydrabadComponent } from './home/home-hydrabad/home-hydrabad.component';
import { HomeMumbaiComponent } from './home/home-mumbai/home-mumbai.component';
import { HomeDelhiComponent } from './home/home-delhi/home-delhi.component';
import { HomeNoidaComponent } from './home/home-noida/home-noida.component';
import { HomeGurugramComponent } from './home/home-gurugram/home-gurugram.component';
import { HomeBengaluruComponent } from './home/home-bengaluru/home-bengaluru.component';
import { RentalPropertyComponent } from './home/readmore/rental-property/rental-property.component';
import { EndToEndServicesComponent } from './home/readmore/end-to-end-services/end-to-end-services.component';
import { PropertyManagementComponent } from './home/readmore/property-management/property-management.component';


import { NgxJsonLdModule } from '@ngx-lite/json-ld';

// import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';

//facebook APP ID sarik = 370951080448056

//facebook APP ID kumar = 1130848100454838

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('1130848100454838')
        //2910902555618886 2910902555618886
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("375951878238-sfhdtu12pb682kfisk49s9av37p18ns8.apps.googleusercontent.com")
      },

    ]
  );
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    AboutUsComponent,
    ReadmoreComponent,
    TermsConditionComponent,
    PrivacyPolicyComponent,
    BlogsComponent,
    FaqComponent,
    BlogDetailsComponent,
    NewsFeedDetailsComponent,
    HomePuneComponent,
    HomeHydrabadComponent,
    HomeMumbaiComponent,
    HomeDelhiComponent,
    HomeChennaiComponent,
    HomeNoidaComponent,
    HomeGurugramComponent,
    HomeBengaluruComponent,
    RentalPropertyComponent,
    EndToEndServicesComponent,
    PropertyManagementComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientModule,
    FormsModule,
    SocialLoginModule,
    OwlModule,
    ReactiveFormsModule,
    SlickModule.forRoot(),
    OwlDateTimeModule,
    NgMultiSelectDropDownModule.forRoot(),
    OwlNativeDateTimeModule,
    MatChipsModule,
    ToastrModule.forRoot({
      timeOut: 4000,
      positionClass: 'toast-top-center',
    }),
    RecaptchaModule,
    RecaptchaFormsModule,
    ClickOutsideModule,
    // For google SEO Schema JsonLD
    NgxJsonLdModule

  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true },
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    DataService,
    MatDatepickerModule,
    { provide: LocationStrategy, useClass: PathLocationStrategy }
  ],


  bootstrap: [AppComponent],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
